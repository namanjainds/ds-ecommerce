﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Infrastructure;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs;
using Nop.Plugin.Api.DTOs.OrderItems;
using Nop.Plugin.Api.DTOs.Orders;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.OrdersParameters;
using Nop.Plugin.Api.Services;
using Nop.Plugin.Api.Validators;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using Microsoft.AspNetCore.Mvc;

namespace Nop.Plugin.Api.Controllers
{
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using DTOs.Errors;
    using JSON.Serializers;
    using Newtonsoft.Json;
    using Nop.Core.Domain.Payments;
    using Microsoft.AspNetCore.Http;
    using Nop.Core.Http.Extensions;
    using Nop.Plugin.Api.Models.CustomersParameters;
    using Nop.Plugin.Api.Enums;
    using Nop.Plugin.Api.DataStructures;
    using Nop.Core.Domain.Common;

    //[ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class OrdersController : BaseApiController
    {
        private readonly IOrderApiService _orderApiService;
        private readonly IProductService _productService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IOrderService _orderService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IShippingService _shippingService;
        private readonly IDTOHelper _dtoHelper;        
        private readonly IProductAttributeConverter _productAttributeConverter;
        private readonly IStoreContext _storeContext;
        private readonly IFactory<Order> _factory;
        private readonly IWorkContext _workContext;
        private readonly PaymentSettings _paymentSettings;
        private readonly ICustomerApiService _customerApiService;
        private readonly ICategoryService _categoryService;
        private readonly IPictureService _pictureService;
        private readonly IDiscountService _discountService;

        // We resolve the order settings this way because of the tests.
        // The auto mocking does not support concreate types as dependencies. It supports only interfaces.
        private OrderSettings _orderSettings;

        private OrderSettings OrderSettings => _orderSettings ?? (_orderSettings = EngineContext.Current.Resolve<OrderSettings>());

        public OrdersController(IOrderApiService orderApiService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IProductService productService,
            IFactory<Order> factory,
            IOrderProcessingService orderProcessingService,
            IOrderService orderService,
            IShoppingCartService shoppingCartService,
            IGenericAttributeService genericAttributeService,
            IStoreContext storeContext,
            IShippingService shippingService,
            IPictureService pictureService,
            IDTOHelper dtoHelper, ICustomerApiService customerApiService,
            IProductAttributeConverter productAttributeConverter,
            IWorkContext workContext, PaymentSettings paymentSettings, ICategoryService categoryService)
            : base(jsonFieldsSerializer, aclService, customerService, storeMappingService,
                 storeService, discountService, customerActivityService, localizationService,pictureService)
        {
            _orderApiService = orderApiService;
            _factory = factory;
            _orderProcessingService = orderProcessingService;
            _orderService = orderService;
            _shoppingCartService = shoppingCartService;
            _genericAttributeService = genericAttributeService;
            _storeContext = storeContext;
            _shippingService = shippingService;
            _dtoHelper = dtoHelper;
            _productService = productService;
            _productAttributeConverter = productAttributeConverter;
            _workContext = workContext;
            _paymentSettings = paymentSettings;
            _customerApiService = customerApiService;
            _categoryService = categoryService;
            _pictureService = pictureService;
            _discountService = discountService;
        }

        /// <summary>
        /// Receive a list of all Orders
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/orders")]
        [ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetOrders(OrdersParametersModel parameters)
        {
            if (parameters.Page < Configurations.DefaultPageValue)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid page parameter");
            }

            if (parameters.Limit < Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid limit parameter");
            }

            var storeId = _storeContext.CurrentStore.Id;

            var totalOrders = _orderApiService.GetOrders(parameters.Ids, parameters.CreatedAtMin,
                parameters.CreatedAtMax,
                parameters.Limit, parameters.Page, parameters.SinceId,
                parameters.Status, parameters.PaymentStatus, parameters.ShippingStatus,
                parameters.CustomerId, storeId);
            var orders = new ApiList<Order>(totalOrders, parameters.Page - 1, parameters.Limit).ToList();
            IList<OrderDto> ordersAsDtos = orders.Select(x => _dtoHelper.PrepareOrderDTO(x)).ToList();

            var ordersRootObject = new OrdersRootObject()
            {
                Orders = ordersAsDtos
            };

            var json = JsonFieldsSerializer.Serialize(ordersRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// Receive a count of all Orders
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/orders/count")]
        [ProducesResponseType(typeof(OrdersCountRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetOrdersCount(OrdersCountParametersModel parameters)
        {
            var storeId = _storeContext.CurrentStore.Id;

            var ordersCount = _orderApiService.GetOrdersCount(parameters.CreatedAtMin, parameters.CreatedAtMax, parameters.Status,
                                                              parameters.PaymentStatus, parameters.ShippingStatus, parameters.CustomerId, storeId);

            var ordersCountRootObject = new OrdersCountRootObject()
            {
                Count = ordersCount
            };

            return Ok(ordersCountRootObject);
        }

        /// <summary>
        /// Retrieve order by spcified id
        /// </summary>
        ///   /// <param name="id">Id of the order</param>
        /// <param name="fields">Fields from the order you want your json to contain</param>
        /// <response code="200">OK</response>
        /// <response code="404">Not Found</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/orders/{id}")]
        [ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetOrderById(int id, string fields = "")
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            var order = _orderApiService.GetOrderById(id);

            if (order == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }

            var ordersRootObject = new OrdersRootObject();

            var orderDto = _dtoHelper.PrepareOrderDTO(order);
            ordersRootObject.Orders.Add(orderDto);

            var json = JsonFieldsSerializer.Serialize(ordersRootObject, fields);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// Retrieve all orders for customer
        /// </summary>
        /// <param name="customerId">Id of the customer whoes orders you want to get</param>
        /// <response code="200">OK</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/orders/customer/{customer_id}")]
        [ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetOrdersByCustomerId(int customerId)
        {
            IList<OrderDto> ordersForCustomer = _orderApiService.GetOrdersByCustomerId(customerId).Select(x => _dtoHelper.PrepareOrderDTO(x)).ToList();

            var ordersRootObject = new OrdersRootObject()
            {
                Orders = ordersForCustomer
            };

            return Ok(ordersRootObject);
        }

        [HttpPost]
        [Route("/api/v1/orderDetail")]
        public IActionResult GetOrderDetailByOrderId(OrderDetailRequestModel orderDetail)
        {
            if (orderDetail.Order_Id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }
             
            var order = _orderApiService.GetOrderById(orderDetail.Order_Id);

            if (order == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }

            var ordersRootObject = new List<OrderDetailResponseModel>();
            string chargeJson = _genericAttributeService.GetAttribute<string>(order, NopOrderDefaults.StripeChargeAttribute);
            Stripe.Charge charge = null;
            charge = !string.IsNullOrEmpty(chargeJson) ? Stripe.Charge.FromJson(_genericAttributeService.GetAttribute<string>(order, NopOrderDefaults.StripeChargeAttribute)) : null;
            
            var orderDto = new OrderDetailResponseModel()
            {
                Id = order.Id,
                OrderId = $"ch{order.Id.ToString()}_{(order.CustomOrderNumber + 1000).ToString()}",
                Amount = order.OrderSubtotalInclTax,
                TotalAmount = order.OrderTotal,
                Discount = order.OrderDiscount,
                DiscountType = order.DiscountUsageHistory.Any() ? order.DiscountUsageHistory?.FirstOrDefault()?.Discount?.Name?.ToString() : null,
                VoucherCode = order.DiscountUsageHistory.Any() ? order.DiscountUsageHistory?.FirstOrDefault()?.Discount?.CouponCode : null,
                OrderStatus = ApiOrderStatus.Delivered.ToString(),
                OrderPaid = 1,
                PaymentStatus = 1,
                OrderComment = order.OrderNotes.Any() ? order.OrderNotes.FirstOrDefault()?.Note : null,
                CreatedAt = order.CreatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                BillingAddressName = order.BillingAddress?.FirstName,
                BillingAddressNumber = order.BillingAddress?.PhoneNumber,
                BillingFlatNumber = order.BillingAddress != null ? _genericAttributeService.GetAttribute<string>(order.BillingAddress, NopCustomerDefaults.FlatNumberAttribute) : null,
                BillingAddress1 = order.BillingAddress != null ? order.BillingAddress?.Address1 : null,
                BillingAddress2 = order.BillingAddress?.Address2,
                BillingAddressSuburb = order.BillingAddress?.City,
                BillingAddressState = string.IsNullOrEmpty(order.BillingAddress?.StateProvince?.Name) ? order.BillingAddress?.County : order.BillingAddress?.StateProvince?.Name,
                BillingAddressCountry = _genericAttributeService.GetAttribute<string>(order.BillingAddress, NopCustomerDefaults.CountryNameAttribute),
                BillingAddressPostCode = order.BillingAddress?.ZipPostalCode,
                ShippingAddressName = order.ShippingAddress?.FirstName ?? order.BillingAddress?.FirstName,
                ShippingAddressNumber = order.ShippingAddress?.PhoneNumber ?? order.BillingAddress?.PhoneNumber,
                ShippingAddressFlatNumber = string.IsNullOrEmpty(order.ShippingAddress?.Address1) ? (order.BillingAddress != null ? _genericAttributeService.GetAttribute<string>(order.BillingAddress, NopCustomerDefaults.FlatNumberAttribute) : null) : _genericAttributeService.GetAttribute<string>(order.ShippingAddress, NopCustomerDefaults.FlatNumberAttribute),
                ShippingAddress1 = string.IsNullOrEmpty(order.ShippingAddress?.Address1) ? (order.BillingAddress != null ? order.BillingAddress?.Address1 : null) : order.ShippingAddress?.Address1,
                ShippingAddress2 = order.ShippingAddress?.Address2 ?? order.BillingAddress?.Address2,
                ShippingAddressSuburb = order.ShippingAddress?.City ?? order.BillingAddress?.City,
                ShippingAddressState = string.IsNullOrEmpty(order.ShippingAddress?.StateProvince?.Name) ? (string.IsNullOrEmpty(order.ShippingAddress?.County) ? (string.IsNullOrEmpty(order.BillingAddress?.StateProvince?.Name) ? order.BillingAddress?.County : order.BillingAddress?.StateProvince?.Name) : order.ShippingAddress?.County) : order.ShippingAddress?.StateProvince?.Name,
                ShippingAddressCountry = order.ShippingAddress !=null ? _genericAttributeService.GetAttribute<string>(order.BillingAddress, NopCustomerDefaults.CountryNameAttribute) : _genericAttributeService.GetAttribute<string>(order.ShippingAddress, NopCustomerDefaults.CountryNameAttribute),
                ShippingAddressPostCode = order.ShippingAddress?.ZipPostalCode ?? order.BillingAddress?.ZipPostalCode,
                Transaction = charge != null ? new List<Transaction>() { new Transaction()
                {
                    Id = _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Order", NopOrderDefaults.StripeChargeAttribute, chargeJson).Id,
                    UserId = order.CustomerId,
                    RefundId = charge.Refunds.Any() ? charge.Refunds.FirstOrDefault().Id : null,
                    RefundResponse = charge.Refunded ? charge.Refunds.FirstOrDefault().Reason : null,
                    TransactionId = charge.Id,
                    Amount = charge.Amount,
                    Type = 1,
                    PaymentMethod = charge.PaymentMethodId,
                    PaymentReponse = chargeJson,
                    PaymentStatus = charge.Status,
                    CreatedAt = order.CreatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                    UpdatedAt = order.CreatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                    Pivot = new Pivot()
                    {
                        OrderId = order.Id,
                        TransactionId = _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Order", NopOrderDefaults.StripeChargeAttribute, _genericAttributeService.GetAttribute<string>(order, NopOrderDefaults.StripeChargeAttribute)).Id
                    }
                } } : new List<Transaction>(),
                Products = order.OrderItems.Any() ? order.OrderItems.Select(item => new OrderDetailProductModel()
                {
                    Id = item.Id,
                    OrderId = order.Id,
                    ProductId = item.ProductId,
                    ProductName = item.Product.Name,
                    ProductCategoryName = item.Product.ProductCategories.Any() ? item.Product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId ==0 ? item.Product.ProductCategories.FirstOrDefault().Category.Name : _categoryService.GetCategoryById(item.Product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId ?? 0)?.Name : null,
                    ProductSubCategoryName = item.Product.ProductCategories.Any() ? item.Product.ProductCategories.FirstOrDefault()?.Category.Name : null,
                    Description = item.Product.ShortDescription,
                    UnitPrice = item.UnitPriceInclTax,
                    Quantity = item.Quantity,
                    TotalPrice = item.PriceInclTax,
                    ColorId = 0,
                    SizeId = 0,
                    WeightId = 0,
                    CreatedAt = order.CreatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                    UpdatedAt = DateTime.MinValue.ToString("yyyy/MM/dd HH:mm:ss"),
                    Colors = new Common(),
                    Weight = new Common(),
                    Size = new Common(),
                    AnimalProduct = new OrderDetailProductDetailModel()
                    {
                        Id = item.ProductId,
                        ProductCategoryId = item.Product.ProductCategories.Any() ? item.Product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId ==0 ? item.Product.ProductCategories.FirstOrDefault().CategoryId : item.Product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId : null,
                        ProductSubCategoryId = item.Product.ProductCategories.Any() ? item.Product.ProductCategories.FirstOrDefault()?.CategoryId : null,
                        Title = item.Product.Name,
                        Quantity = item.Quantity,
                        Price = item.Product.Price,
                        PrimaryImage = _pictureService.GetPicturesByProductId(item.ProductId).Any() ? _pictureService.GetPictureUrl(_pictureService.GetPicturesByProductId(item.ProductId).FirstOrDefault(), 75, true) : null,
                        Description = item.Product.FullDescription,
                        IsFeatured = item.Product.ProductCategories.Any() ? item.Product.ProductCategories.FirstOrDefault().IsFeaturedProduct ? 1 : 0 : 0,
                        Status = item.Product.Deleted ? 0 : 1,
                        CreatedAt = item.Product.CreatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                        UpdatedAt = item.Product.UpdatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss")
                    }
                }).ToList() : new List<OrderDetailProductModel>()
            };
            
            ordersRootObject.Add(orderDto);
            var response = new ResponseModel<List<OrderDetailResponseModel>>()
            {
                Message = "successfully get Data",
                Data = ordersRootObject
            };
            var json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore});

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/v1/orderHistory")]
        public IActionResult GetOrderHistory(OrderHistoryRequestModel orderHistory)
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Split("Bearer", 2)[1]).EntityId;
            var customer = _customerApiService.GetCustomerEntityById(customerId);
            var totalOrders = _orderApiService.GetOrders(customerId: customerId, page: orderHistory.Page);
            var ordersForCustomer = new ApiList<Order>(totalOrders, orderHistory.Page - 1, 10).ToList().
                Select(order => new OrderHistoryModel()
                {
                    Id = order.Id,
                    OrderId = $"ch{order.Id.ToString()}_{(order.CustomOrderNumber+1000).ToString()}",
                    TotalAmount = order.OrderTotal,
                    OrderStatus = ApiOrderStatus.Delivered.ToString(),
                    CreatedAt = order.CreatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                    Products = order.OrderItems.Select(item => new OrderHistoryProductModel()
                    {
                        Id = item.Id,
                        OrderId = order.Id,
                        OrderStatus = ApiOrderStatus.Delivered.ToString(),
                        ProductId = item.ProductId,
                        ProductName = item.Product.Name,
                        ProductCategoryName = item.Product.ProductCategories.Any() ? item.Product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId == 0 ? item.Product.ProductCategories.FirstOrDefault().Category.Name : _categoryService.GetCategoryById(item.Product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId ?? 0)?.Name : null,                        
                        ProductSubCategoryName = item.Product.ProductCategories.Any() ? item.Product.ProductCategories.FirstOrDefault()?.Category.Name : null,
                        Description = item.Product.ShortDescription,
                        UnitPrice = item.UnitPriceInclTax,
                        Quantity = item.Quantity,
                        TotalPrice = item.PriceInclTax,                        
                        ColorId = 0,
                        SizeId = 0,
                        WeightId = 0,
                        CreatedAt = order.CreatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                        UpdatedAt = DateTime.MinValue.ToString("yyyy/MM/dd HH:mm:ss")
                    }).ToList()
                }).ToList().OrderByDescending(y=> y.Id).ToList();


               //var test = _orderApiService.GetOrders(customerId: customerId, page: orderHistory.Page).Select(x=>_dtoHelper.PrepareOrderDTO(x)).ToList();

            var ordersRootObject = new OrderHistoryResponseModel()
            {
                OrderHistoryData = ordersForCustomer,
                RecordPerPage = 10,
                TotalRecords = totalOrders.ToList().Count
            };

            var response = new ResponseModel<OrderHistoryResponseModel>()
            {
                Message = "Successfully get Data",
                Data = ordersRootObject
            };
            var json = JsonConvert.SerializeObject(response);
            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/orders")]
        [ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult CreateOrder([ModelBinder(typeof(JsonModelBinder<OrderDto>))] Delta<OrderDto> orderDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            if (orderDelta.Dto.CustomerId == null)
            {
                return Error();
            }

            // We doesn't have to check for value because this is done by the order validator.
            var customer = CustomerService.GetCustomerById(orderDelta.Dto.CustomerId.Value);
            
            if (customer == null)
            {
                return Error(HttpStatusCode.NotFound, "customer", "not found");
            }

            var shippingRequired = false;

            if (orderDelta.Dto.OrderItems != null)
            {
                var shouldReturnError = AddOrderItemsToCart(orderDelta.Dto.OrderItems, customer, orderDelta.Dto.StoreId ?? _storeContext.CurrentStore.Id);
                if (shouldReturnError)
                {
                    return Error(HttpStatusCode.BadRequest);
                }

                shippingRequired = IsShippingAddressRequired(orderDelta.Dto.OrderItems);
            }

            if (shippingRequired)
            {
                var isValid = true;

                isValid &= SetShippingOption(orderDelta.Dto.ShippingRateComputationMethodSystemName,
                                            orderDelta.Dto.ShippingMethod,
                                            orderDelta.Dto.StoreId ?? _storeContext.CurrentStore.Id,
                                            customer, 
                                            BuildShoppingCartItemsFromOrderItemDtos(orderDelta.Dto.OrderItems.ToList(), 
                                                                                    customer.Id, 
                                                                                    orderDelta.Dto.StoreId ?? _storeContext.CurrentStore.Id));

                if (!isValid)
                {
                    return Error(HttpStatusCode.BadRequest);
                }
            }

            var newOrder = _factory.Initialize();
            orderDelta.Merge(newOrder);

            customer.BillingAddress = newOrder.BillingAddress;
            customer.ShippingAddress = newOrder.ShippingAddress;

            // If the customer has something in the cart it will be added too. Should we clear the cart first? 
            newOrder.Customer = customer;

            // The default value will be the currentStore.id, but if it isn't passed in the json we need to set it by hand.
            if (!orderDelta.Dto.StoreId.HasValue)
            {
                newOrder.StoreId = _storeContext.CurrentStore.Id;
            }
            
            var placeOrderResult = PlaceOrder(newOrder, customer);

            if (!placeOrderResult.Success)
            {
                foreach (var error in placeOrderResult.Errors)
                {
                    ModelState.AddModelError("order placement", error);
                }

                return Error(HttpStatusCode.BadRequest);
            }

            CustomerActivityService.InsertActivity("AddNewOrder",
                 LocalizationService.GetResource("ActivityLog.AddNewOrder"), newOrder);

            var ordersRootObject = new OrdersRootObject();

            var placedOrderDto = _dtoHelper.PrepareOrderDTO(placeOrderResult.PlacedOrder);

            ordersRootObject.Orders.Add(placedOrderDto);

            var json = JsonFieldsSerializer.Serialize(ordersRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/v1/placeOrder")]
        public IActionResult CheckoutOrder(PlaceOrderRequestModel placeOrder)
        {
            var response = new ResponseModel<PlaceOrderResponseModel>()
            {
                Message = "Order placed successfully",
                Data = new PlaceOrderResponseModel()
            };
            try
            { 
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Split("Bearer", 2)[1]).EntityId;
            var customer = _customerApiService.GetCustomerEntityById(customerId);

            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                response = new ResponseModel<PlaceOrderResponseModel>()
                {
                    Status = false,
                    Message = ModelState.Values.Where(x => x.Errors.Count > 0).FirstOrDefault().Errors.FirstOrDefault().ErrorMessage,
                    Data = new PlaceOrderResponseModel()
                };

            }
            else
            {
                if (customer == null)
                {
                    response = new ResponseModel<PlaceOrderResponseModel>()
                    {
                        Status = false,
                        Message = "Customer not found",
                        Data = new PlaceOrderResponseModel()
                    };
                }
                else
                {
                    if (placeOrder.Voucher_code_applied == 1)
                    {
                        string discountcouponcode = placeOrder.Voucher_code_value;
                        //trim
                        if (discountcouponcode != null)
                            discountcouponcode = discountcouponcode.Trim();

                        //cart
                        var cart = _shoppingCartService.GetShoppingCart(customer, ShoppingCartType.ShoppingCart, _storeContext.CurrentStore.Id);

                        //parse and save checkout attributes
                        //ParseAndSaveCheckoutAttributes(cart, form);

                        if (!string.IsNullOrWhiteSpace(discountcouponcode))
                        {
                            //we find even hidden records here. this way we can display a user-friendly message if it's expired
                            var discounts = _discountService.GetAllDiscountsForCaching(couponCode: discountcouponcode, showHidden: true)
                                .Where(d => d.RequiresCouponCode)
                                .ToList();
                            if (discounts.Any())
                            {
                                var userErrors = new List<string>();
                                var anyValidDiscount = discounts.Any(discount =>
                                {
                                    var validationResult = _discountService.ValidateDiscount(discount, _workContext.CurrentCustomer, new[] { discountcouponcode });
                                    userErrors.AddRange(validationResult.Errors);

                                    return validationResult.IsValid;
                                });

                                if (anyValidDiscount)
                                {
                                    //valid
                                    _customerApiService.ApplyDiscountCouponCode(customer, discountcouponcode);
                                }
                            }
                        }

                    }

                    //var shippingRequired = false;

                    //if (orderDelta.Dto.OrderItems != null)
                    //{
                    //    var shouldReturnError = AddOrderItemsToCart(orderDelta.Dto.OrderItems, customer, _storeContext.CurrentStore.Id);
                    //    if (shouldReturnError)
                    //    {
                    //        return Error(HttpStatusCode.BadRequest);
                    //    }

                    //    shippingRequired = IsShippingAddressRequired(orderDelta.Dto.OrderItems);
                    //}

                    //if (shippingRequired)
                    //{
                    //    var isValid = true;

                    //    isValid &= SetShippingOption(orderDelta.Dto.ShippingRateComputationMethodSystemName,
                    //                                orderDelta.Dto.ShippingMethod,
                    //                                _storeContext.CurrentStore.Id,
                    //                                customer,
                    //                                BuildShoppingCartItemsFromOrderItemDtos(orderDelta.Dto.OrderItems.ToList(),
                    //                                                                        customer.Id,
                    //                                                                        _storeContext.CurrentStore.Id));

                    //    if (!isValid)
                    //    {
                    //        return Error(HttpStatusCode.BadRequest);
                    //    }
                    //}

                    var newOrder = _factory.Initialize();
                    //orderDelta.Merge(newOrder);
                    if (placeOrder.Billing_Address_Id != null)
                        customer.BillingAddress = customer.Addresses.Where(add => add.Id == placeOrder.Billing_Address_Id).FirstOrDefault();

                    if (customer.BillingAddress == null)
                        customer.BillingAddress = customer.Addresses.FirstOrDefault();

                    if (placeOrder.Shipping_Address_Id != null)
                        customer.ShippingAddress = customer.Addresses.Where(add => add.Id == placeOrder.Shipping_Address_Id).FirstOrDefault();

                    if (customer.ShippingAddress == null)
                        customer.ShippingAddress = customer.BillingAddress;

                    _customerApiService.UpdateCustomer(customer);
                    // If the customer has something in the cart it will be added too. Should we clear the cart first? 
                    newOrder.Customer = customer;

                    // The default value will be the currentStore.id, but if it isn't passed in the json we need to set it by hand.
                    newOrder.StoreId = customer.RegisteredInStoreId;

                    var placeOrderResult = PlaceOrder(customer, placeOrder.StripeToken, placeOrder.Order_Comment);

                    if (!placeOrderResult.Success)
                    {
                        foreach (var error in placeOrderResult.Errors)
                        {
                            ModelState.AddModelError("order placement", error);
                        }

                        response = new ResponseModel<PlaceOrderResponseModel>()
                        {
                            Status = false,
                            Message = placeOrderResult.Errors.FirstOrDefault(),
                            Data = new PlaceOrderResponseModel()
                        };
                    }
                    else
                    {
                        CustomerActivityService.InsertActivity("AddNewOrder",
                         LocalizationService.GetResource("ActivityLog.AddNewOrder"), newOrder);

                        var ordersRootObject = new PlaceOrderResponseModel()
                        {
                            Id = placeOrderResult.PlacedOrder.Id,
                            Order_Id = $"ch{placeOrderResult.PlacedOrder.Id.ToString()}_{(placeOrderResult.PlacedOrder.CustomOrderNumber + 1000).ToString()}"
                        };

                        //var placedOrderDto = _dtoHelper.PrepareOrderDTO(placeOrderResult.PlacedOrder);

                        response = new ResponseModel<PlaceOrderResponseModel>()
                        {
                            Message = "Order placed successfully",
                            Data = ordersRootObject
                        };
                    }
                }
            }
            }
            catch (Exception ex) 
            {
                response = new ResponseModel<PlaceOrderResponseModel>()
                {
                    Data = new PlaceOrderResponseModel(),
                    Message = "There is an error while placing order " + ex.InnerException != null ?  ex.InnerException.Message : ex.Message,
                    Status = false
                };
            }
            var json = JsonConvert.SerializeObject(response);
            return new RawJsonActionResult(json);
        }

        [HttpDelete]
        [Route("/api/orders/{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult DeleteOrder(int id)
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }
            
            var orderToDelete = _orderApiService.GetOrderById(id);

            if (orderToDelete == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }

            _orderProcessingService.DeleteOrder(orderToDelete);

            //activity log
            CustomerActivityService.InsertActivity("DeleteOrder", LocalizationService.GetResource("ActivityLog.DeleteOrder"), orderToDelete);

            return new RawJsonActionResult("{}");
        }

        [HttpPut]
        [Route("/api/orders/{id}")]
        [ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult UpdateOrder([ModelBinder(typeof(JsonModelBinder<OrderDto>))] Delta<OrderDto> orderDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            var currentOrder = _orderApiService.GetOrderById(orderDelta.Dto.Id);

            if (currentOrder == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }

            var customer = currentOrder.Customer;

            var shippingRequired = currentOrder.OrderItems.Any(item => !item.Product.IsFreeShipping);

            if (shippingRequired)
            {
                var isValid = true;

                if (!string.IsNullOrEmpty(orderDelta.Dto.ShippingRateComputationMethodSystemName) ||
                    !string.IsNullOrEmpty(orderDelta.Dto.ShippingMethod))
                {
                    var storeId = orderDelta.Dto.StoreId ?? _storeContext.CurrentStore.Id;

                    isValid &= SetShippingOption(orderDelta.Dto.ShippingRateComputationMethodSystemName ?? currentOrder.ShippingRateComputationMethodSystemName,
                        orderDelta.Dto.ShippingMethod, 
                        storeId,
                        customer, BuildShoppingCartItemsFromOrderItems(currentOrder.OrderItems.ToList(), customer.Id, storeId));
                }

                if (isValid)
                {
                    currentOrder.ShippingMethod = orderDelta.Dto.ShippingMethod;
                }
                else
                {
                    return Error(HttpStatusCode.BadRequest);
                }
            }

            orderDelta.Merge(currentOrder);
            
            customer.BillingAddress = currentOrder.BillingAddress;
            customer.ShippingAddress = currentOrder.ShippingAddress;

            _orderService.UpdateOrder(currentOrder);

            CustomerActivityService.InsertActivity("UpdateOrder",
                 LocalizationService.GetResource("ActivityLog.UpdateOrder"), currentOrder);

            var ordersRootObject = new OrdersRootObject();

            var placedOrderDto = _dtoHelper.PrepareOrderDTO(currentOrder);
            placedOrderDto.ShippingMethod = orderDelta.Dto.ShippingMethod;

            ordersRootObject.Orders.Add(placedOrderDto);

            var json = JsonFieldsSerializer.Serialize(ordersRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("api/v1/applyVoucher")]
        public IActionResult ApplyVoucher(VoucherRequestModel voucher)
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Split("Bearer", 2)[1]).EntityId;
            var customer = _customerApiService.GetCustomerEntityById(customerId);

            string discountcouponcode = "";
            var response = new ResponseModel<VoucherResponseModel>();
            
            //trim
            if (voucher.Voucher_Code != null)
               discountcouponcode = voucher.Voucher_Code.Trim();

            ////cart
            //var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, _storeContext.CurrentStore.Id);

            ////parse and save checkout attributes
            //ParseAndSaveCheckoutAttributes(cart, form);

            //var model = new ShoppingCartModel();
            if (!string.IsNullOrWhiteSpace(discountcouponcode))
            {
                //we find even hidden records here. this way we can display a user-friendly message if it's expired
                var discounts = _discountService.GetAllDiscounts(couponCode: discountcouponcode, showHidden: true)
                    .Where(d => d.RequiresCouponCode)
                    .ToList();
                if (discounts.Any())
                {
                        response = new ResponseModel<VoucherResponseModel>()
                        {
                            Data = new VoucherResponseModel
                            {
                                VoucherCode = new List<VoucherCouponModel>() { new VoucherCouponModel()
                                {
                                    Id = discounts.FirstOrDefault().Id,
                                    Title = discounts.FirstOrDefault().Name,
                                    VoucherCode = discounts.FirstOrDefault().CouponCode,
                                    Type = discounts.FirstOrDefault().DiscountAmount > 0 ? "Fixed Amount" : "Percentage",
                                    Value = discounts.FirstOrDefault().DiscountAmount > 0 ? Convert.ToInt32(discounts.FirstOrDefault().DiscountAmount) : Convert.ToInt32(discounts.FirstOrDefault().DiscountPercentage),
                                    FromDate = discounts.FirstOrDefault().StartDateUtc?.ToString("yyyy/MM/dd HH:mm:ss"),
                                    ToDate = discounts.FirstOrDefault().EndDateUtc?.ToString("yyyy/MM/dd HH:mm:ss"),
                                } }
                            }
                        };
                        //valid
                        //_customerService.ApplyDiscountCouponCode(_workContext.CurrentCustomer, discountcouponcode);
                        //model.DiscountBox.Messages.Add(_localizationService.GetResource("ShoppingCart.DiscountCouponCode.Applied"));
                        //model.DiscountBox.IsApplied = true;
                }
                else
                    response = new ResponseModel<VoucherResponseModel>()
                    {
                        Message = "Wrong Voucher Code",
                        Data = new VoucherResponseModel(),
                        Status = false
                    };  
                //discount cannot be found
                //model.DiscountBox.Messages.Add(_localizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount"));
            }
            else
                response = new ResponseModel<VoucherResponseModel>()
                {
                    Message = "Wrong Voucher Code",
                    Data = new VoucherResponseModel(),
                    Status = false
                };
            //empty coupon code
            //model.DiscountBox.Messages.Add(_localizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount"));

            var json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });

            return new RawJsonActionResult(json);
        }

        private bool SetShippingOption(string shippingRateComputationMethodSystemName, string shippingOptionName, int storeId, Customer customer, List<ShoppingCartItem> shoppingCartItems)
        {
            var isValid = true;

            if (string.IsNullOrEmpty(shippingRateComputationMethodSystemName))
            {
                isValid = false;

                ModelState.AddModelError("shipping_rate_computation_method_system_name",
                    "Please provide shipping_rate_computation_method_system_name");
            }
            else if (string.IsNullOrEmpty(shippingOptionName))
            {
                isValid = false;

                ModelState.AddModelError("shipping_option_name", "Please provide shipping_option_name");
            }
            else
            {
                var shippingOptionResponse = _shippingService.GetShippingOptions(shoppingCartItems, customer.ShippingAddress, customer,
                        shippingRateComputationMethodSystemName, storeId);

                if (shippingOptionResponse.Success)
                {
                    var shippingOptions = shippingOptionResponse.ShippingOptions.ToList();

                    var shippingOption = shippingOptions
                        .Find(so => !string.IsNullOrEmpty(so.Name) && so.Name.Equals(shippingOptionName, StringComparison.InvariantCultureIgnoreCase));
                    
                    _genericAttributeService.SaveAttribute(customer,
                        NopCustomerDefaults.SelectedShippingOptionAttribute,
                        shippingOption, storeId);
                }
                else
                {
                    isValid = false;

                    foreach (var errorMessage in shippingOptionResponse.Errors)
                    {
                        ModelState.AddModelError("shipping_option", errorMessage);
                    }
                }
            }

            return isValid;
        }

        private List<ShoppingCartItem> BuildShoppingCartItemsFromOrderItems(List<OrderItem> orderItems, int customerId, int storeId)
        {
            var shoppingCartItems = new List<ShoppingCartItem>();

            foreach (var orderItem in orderItems)
            {
                shoppingCartItems.Add(new ShoppingCartItem()
                {
                    ProductId = orderItem.ProductId,
                    CustomerId = customerId,
                    Quantity = orderItem.Quantity,
                    RentalStartDateUtc = orderItem.RentalStartDateUtc,
                    RentalEndDateUtc = orderItem.RentalEndDateUtc,
                    StoreId = storeId,
                    Product = orderItem.Product,
                    ShoppingCartType = ShoppingCartType.ShoppingCart
                });
            }

            return shoppingCartItems;
        }

        private List<ShoppingCartItem> BuildShoppingCartItemsFromOrderItemDtos(List<OrderItemDto> orderItemDtos, int customerId, int storeId)
        {
            var shoppingCartItems = new List<ShoppingCartItem>();

            foreach (var orderItem in orderItemDtos)
            {
                if (orderItem.ProductId != null)
                {
                    shoppingCartItems.Add(new ShoppingCartItem()
                    {
                        ProductId = orderItem.ProductId.Value, // required field
                        CustomerId = customerId,
                        Quantity = orderItem.Quantity ?? 1,
                        RentalStartDateUtc = orderItem.RentalStartDateUtc,
                        RentalEndDateUtc = orderItem.RentalEndDateUtc,
                        StoreId = storeId,
                        Product = _productService.GetProductById(orderItem.ProductId.Value),
                        ShoppingCartType = ShoppingCartType.ShoppingCart
                    });
                }
            }

            return shoppingCartItems;
        }

        private PlaceOrderResult PlaceOrder(Order newOrder, Customer customer)
        {
            var processPaymentRequest = new ProcessPaymentRequest
            {
                StoreId = newOrder.StoreId,
                CustomerId = customer.Id,
                PaymentMethodSystemName = newOrder.PaymentMethodSystemName
            };

            var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest);

            return placeOrderResult;
        }

        private PlaceOrderResult PlaceOrder(Customer customer, string stripeToken, string orderComment)
        {
            var processPaymentRequest = new ProcessPaymentRequest();
            GenerateOrderGuid(processPaymentRequest);
            processPaymentRequest.StoreId = _storeContext.CurrentStore.Id;
            processPaymentRequest.CustomerId = customer.Id;
            _genericAttributeService.SaveAttribute<string>(customer,
                NopCustomerDefaults.SelectedPaymentMethodAttribute, "Payments.CheckMoneyOrder", _storeContext.CurrentStore.Id);
            processPaymentRequest.PaymentMethodSystemName = _genericAttributeService.GetAttribute<string>(customer,
                NopCustomerDefaults.SelectedPaymentMethodAttribute, _storeContext.CurrentStore.Id);

            var placeOrderResult = _orderApiService.PlaceOrder(processPaymentRequest, stripeToken, orderComment);

            return placeOrderResult;
        }
        /// <summary>
        /// Generate an order GUID
        /// </summary>
        /// <param name="processPaymentRequest">Process payment request</param>
        protected virtual void GenerateOrderGuid(ProcessPaymentRequest processPaymentRequest)
        {
            if (processPaymentRequest == null)
                return;

            //we should use the same GUID for multiple payment attempts
            //this way a payment gateway can prevent security issues such as credit card brute-force attacks
            //in order to avoid any possible limitations by payment gateway we reset GUID periodically
            var previousPaymentRequest = HttpContext.Session.Get<ProcessPaymentRequest>("OrderPaymentInfo");
            if (_paymentSettings.RegenerateOrderGuidInterval > 0 &&
                previousPaymentRequest != null &&
                previousPaymentRequest.OrderGuidGeneratedOnUtc.HasValue)
            {
                var interval = DateTime.UtcNow - previousPaymentRequest.OrderGuidGeneratedOnUtc.Value;
                if (interval.TotalSeconds < _paymentSettings.RegenerateOrderGuidInterval)
                {
                    processPaymentRequest.OrderGuid = previousPaymentRequest.OrderGuid;
                    processPaymentRequest.OrderGuidGeneratedOnUtc = previousPaymentRequest.OrderGuidGeneratedOnUtc;
                }
            }

            if (processPaymentRequest.OrderGuid == Guid.Empty)
            {
                processPaymentRequest.OrderGuid = Guid.NewGuid();
                processPaymentRequest.OrderGuidGeneratedOnUtc = DateTime.UtcNow;
            }
        }
        private bool IsShippingAddressRequired(ICollection<OrderItemDto> orderItems)
        {
            var shippingAddressRequired = false;

            foreach (var orderItem in orderItems)
            {
                if (orderItem.ProductId != null)
                {
                    var product = _productService.GetProductById(orderItem.ProductId.Value);

                    shippingAddressRequired |= product.IsShipEnabled;
                }
            }

            return shippingAddressRequired;
        }

        private bool AddOrderItemsToCart(ICollection<OrderItemDto> orderItems, Customer customer, int storeId)
        {
            var shouldReturnError = false;

            foreach (var orderItem in orderItems)
            {
                if (orderItem.ProductId != null)
                {
                    var product = _productService.GetProductById(orderItem.ProductId.Value);

                    if (!product.IsRental)
                    {
                        orderItem.RentalStartDateUtc = null;
                        orderItem.RentalEndDateUtc = null;
                    }

                    var attributesXml = _productAttributeConverter.ConvertToXml(orderItem.Attributes.ToList(), product.Id);                

                    var errors = _shoppingCartService.AddToCart(customer, product,
                        ShoppingCartType.ShoppingCart, storeId,attributesXml,
                        0M, orderItem.RentalStartDateUtc, orderItem.RentalEndDateUtc,
                        orderItem.Quantity ?? 1);

                    if (errors.Count > 0)
                    {
                        foreach (var error in errors)
                        {
                            ModelState.AddModelError("order", error);
                        }

                        shouldReturnError = true;
                    }
                }
            }

            return shouldReturnError;
        }
     }
}