﻿using Nop.Core;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.DTOs.Stores;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Stores;
using System.Collections.Generic;
using System.Net;
using Nop.Plugin.Api.Helpers;

namespace Nop.Plugin.Api.Controllers
{
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Mvc;
    using DTOs.Errors;
    using JSON.Serializers;
    using Newtonsoft.Json;
    using Nop.Plugin.Api.Models.CustomersParameters;
    using Nop.Services.Catalog;
    using System.Linq;
    using Nop.Plugin.Api.Services;
    using Nop.Plugin.Api.Models.OrdersParameters;
    using Nop.Services.Common;
    using Nop.Core.Domain.Customers;
    using Nop.Core.Domain.Orders;
    using System;
    using Nop.Services.Messages;
    using Nop.Services.Topics;

    //[ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class StoreController : BaseApiController
    {
        private readonly IStoreContext _storeContext;
        private readonly IDTOHelper _dtoHelper;
        private readonly ICategoryService _categoryService;
        private readonly IOrderApiService _orderApiService;
        private readonly IPictureService _pictureService;
        private readonly IProductPictureService _productPictureService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICustomerApiService _customerApiService;
        private readonly IShoppingCartItemApiService _shoppingCartItemApiService;
        private readonly IWorkContext _workContext;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly ILocalizationService _localizationService;
        private readonly ITopicService _topicService;

        public StoreController(IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService, IWorkflowMessageService workflowMessageService,
            IDiscountService discountService, IWorkContext workContext,
            ICustomerActivityService customerActivityService, IShoppingCartItemApiService shoppingCartItemApiService,
            ILocalizationService localizationService, ICustomerApiService customerApiService,
            IPictureService pictureService, IGenericAttributeService genericAttributeService,
            IStoreContext storeContext, IProductPictureService productPictureService, ITopicService topicService,
            IDTOHelper dtoHelper, ICategoryService categoryService, IOrderApiService orderApiService)
            : base(jsonFieldsSerializer,
                  aclService,
                  customerService,
                  storeMappingService,
                  storeService,
                  discountService,
                  customerActivityService,
                  localizationService,
                  pictureService)
        {
            _storeContext = storeContext;
            _dtoHelper = dtoHelper;
            _categoryService = categoryService;
            _orderApiService = orderApiService;
            _pictureService = pictureService;
            _productPictureService = productPictureService;
            _genericAttributeService = genericAttributeService;
            _customerApiService = customerApiService;
            _shoppingCartItemApiService = shoppingCartItemApiService;
            _workContext = workContext;
            _customerActivityService = customerActivityService;
            _workflowMessageService = workflowMessageService;
            _localizationService = localizationService;
            _topicService = topicService;
        }

        /// <summary>
        /// Retrieve category by spcified id
        /// </summary>
        /// <param name="fields">Fields from the category you want your json to contain</param>
        /// <response code="200">OK</response>
        /// <response code="404">Not Found</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/current_store")]
        [ProducesResponseType(typeof(StoresRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetCurrentStore(string fields = "")
        {
            var store = _storeContext.CurrentStore;

            if (store == null)
            {
                return Error(HttpStatusCode.NotFound, "store", "store not found");
            }

            var storeDto = _dtoHelper.PrepareStoreDTO(store);

            var storesRootObject = new StoresRootObject();

            storesRootObject.Stores.Add(storeDto);

            var json = JsonFieldsSerializer.Serialize(storesRootObject, fields);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// Retrieve all stores
        /// </summary>
        /// <param name="fields">Fields from the store you want your json to contain</param>
        /// <response code="200">OK</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/stores")]
        [ProducesResponseType(typeof(StoresRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetAllStores(string fields = "")
        {
            var allStores = StoreService.GetAllStores();

            IList<StoreDto> storesAsDto = new List<StoreDto>();

            foreach (var store in allStores)
            {
                var storeDto = _dtoHelper.PrepareStoreDTO(store);

                storesAsDto.Add(storeDto);
            }

            var storesRootObject = new StoresRootObject()
            {
                Stores = storesAsDto
            };

            var json = JsonFieldsSerializer.Serialize(storesRootObject, fields);

            return new RawJsonActionResult(json);
        }

        [HttpGet]
        [Route("/api/v1/getAllData")]
        public IActionResult GetAllData()
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = headers ? _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Split("Bearer", 2)[1]).EntityId : 0;
            var customer = _customerApiService.GetCustomerEntityById(customerId);            

            if (customer == null)
                customer = _workContext.CurrentCustomer;

            var response = new ResponseModel<StoreDataResponseModel>()
            {
                Data = new StoreDataResponseModel(),                
            };

            try
            {
                response = new ResponseModel<StoreDataResponseModel>()
                {
                    Data = new StoreDataResponseModel()
                    {
                        Categories = _categoryService.GetAllCategoriesByParentCategoryId(0).Select(category => new StoreCategoryListingModel()
                        {
                            Id = category.Id,
                            Category = category.Name,
                            ParentId = category.ParentCategoryId,
                            PrimaryImage = _pictureService.GetPictureById(category.PictureId) != null ? _pictureService.GetPictureUrl(_pictureService.GetPictureById(category.PictureId), 75, true) : null,
                            Status = category.Deleted ? 0 : 1,
                            CreatedAt = category.CreatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                            UpdatedAt = category.UpdatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                            SubCategory = _categoryService.GetAllCategoriesByParentCategoryId(category.Id).Select(subcategory => new StoreSubCategoryListingModel()
                            {
                                Id = subcategory.Id,
                                Category = subcategory.Name,
                                ParentId = subcategory.ParentCategoryId,
                                PrimaryImage = _pictureService.GetPictureById(subcategory.PictureId) != null ? _pictureService.GetPictureUrl(_pictureService.GetPictureById(subcategory.PictureId), 75, true) : null,
                                Status = subcategory.Deleted ? 0 : 1,
                                CreatedAt = subcategory.CreatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                                UpdatedAt = subcategory.UpdatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                            }).ToList()
                        }).ToList(),
                        BestSellerProduct = _orderApiService.BestSellersReport().Select(product => new StoreProductListingModel()
                        {
                            Id = product.Id,
                            ProductCategoryId = product.ProductCategories.Any() ? product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId == 0 ? product.ProductCategories.FirstOrDefault().CategoryId : product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId : null,
                            ProductSubCategoryId = product.ProductCategories.Any() ? product.ProductCategories.FirstOrDefault()?.CategoryId : null,
                            Title = product.Name,
                            CategoryName = product.ProductCategories.Any() ? product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId == 0 ? product.ProductCategories.FirstOrDefault().Category.Name : _categoryService.GetCategoryById(product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId ?? 0)?.Name : null,
                            SubCategoryName = product.ProductCategories.Any() ? product.ProductCategories.FirstOrDefault()?.Category.Name : null,
                            Quantity = product.StockQuantity,
                            Price = product.Price,
                            AvgRating = Convert.ToInt32(product.ProductReviews.Any() ? product.ProductReviews.Average(review => review.Rating) : 0),
                            WishlistAdded = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Any() ? _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(x => x.ShoppingCartTypeId == (int)ShoppingCartType.Wishlist && x.ProductId == product.Id).Any() ? 1 : 0 : 0,
                            Description = product.ShortDescription,
                            PrimaryImage = _pictureService.GetPicturesByProductId(product.Id).Any() ? _pictureService.GetPictureUrl(_pictureService.GetPicturesByProductId(product.Id).FirstOrDefault(), 0, true) : null
                        }).ToList(),
                        FeaturedProduct = _orderApiService.GetAllProductsFeatured().Select(product => new StoreProductListingModel()
                        {
                            Id = product.Id,
                            ProductCategoryId = product.ProductCategories.Any() ? product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId == 0 ? product.ProductCategories.FirstOrDefault().CategoryId : product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId : null,
                            ProductSubCategoryId = product.ProductCategories.Any() ? product.ProductCategories.FirstOrDefault()?.CategoryId : null,
                            Title = product.Name,
                            CategoryName = product.ProductCategories.Any() ? product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId == 0 ? product.ProductCategories.FirstOrDefault().Category.Name : _categoryService.GetCategoryById(product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId ?? 0)?.Name : null,
                            SubCategoryName = product.ProductCategories.Any() ? product.ProductCategories.FirstOrDefault()?.Category.Name : null,
                            Quantity = product.StockQuantity,
                            Price = product.Price,
                            AvgRating = Convert.ToInt32(product.ProductReviews.Any() ? product.ProductReviews.Average(review => review.Rating) : 0),
                            WishlistAdded = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Any() ? _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(x => x.ShoppingCartTypeId == (int)ShoppingCartType.Wishlist && x.ProductId == product.Id).Any() ? 1 : 0 : 0,
                            Description = product.ShortDescription,
                            PrimaryImage = _pictureService.GetPicturesByProductId(product.Id).Any() ? _pictureService.GetPictureUrl(_pictureService.GetPicturesByProductId(product.Id).FirstOrDefault(), 0, true) : null
                        }).ToList(),
                        Slider = _orderApiService.BestSellersReport().Select(product => new StoreImageSlider()
                        {
                            Id = _pictureService.GetPicturesByProductId(product.Id).FirstOrDefault().Id,
                            Title = _pictureService.GetPicturesByProductId(product.Id).FirstOrDefault().TitleAttribute,
                            Description = product.ShortDescription,
                            Image = _pictureService.GetPicturesByProductId(product.Id).Any() ? _pictureService.GetPictureUrl(_pictureService.GetPicturesByProductId(product.Id).FirstOrDefault(), 0, true) : null
                        }).ToList(),
                        CartCount = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Any() ? _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(x => x.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).ToList().Count.ToString() : "0"
                    },
                    Message = "Data Found"
                };
            }
            catch
            {
                response = new ResponseModel<StoreDataResponseModel>()
                {
                    Message = "Error while getting details",
                    Data = new StoreDataResponseModel() { CartCount = "0", BestSellerProduct = new List<StoreProductListingModel>(), Categories = new List<StoreCategoryListingModel>(), FeaturedProduct = new List<StoreProductListingModel>(), Slider = new List<StoreImageSlider>() },
                    Status = false,
                };
            }
            var json = JsonConvert.SerializeObject(response);
            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/v1/contactUs")]
        public IActionResult ContactUs(ContactUsRequestModel contactUs)
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = headers ? _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Split("Bearer", 2)[1]).EntityId : 0;
            var customer = _customerApiService.GetCustomerEntityById(customerId);

            if (customer == null)
                customer = _workContext.CurrentCustomer;

            var response = new ResponseModel<Common>()
            {
                Message = "Data saved successfully",
                Data = new Common()
            };
            try
            {
                if (ModelState.IsValid)
                {
                    var subject = contactUs.Title;
                    var body = Core.Html.HtmlHelper.FormatText(contactUs.Message, false, true, false, false, false, false);

                    _workflowMessageService.SendContactUsMessage(_workContext.WorkingLanguage.Id,
                        contactUs.Email.Trim(), contactUs.Name, subject, body);

                    //activity log
                    _customerActivityService.InsertActivity("PublicStore.ContactUs",
                        _localizationService.GetResource("ActivityLog.PublicStore.ContactUs"));

                    response = new ResponseModel<Common>()
                    {
                        Message = "Data saved successfully",
                        Data = new Common()
                    };

                }
                else
                {
                    response = new ResponseModel<Common>()
                    {
                        Message = ModelState.Values.Where(x => x.Errors.Count > 0).FirstOrDefault().Errors.FirstOrDefault().ErrorMessage,
                        Data = new Common()
                    };
                }
            }
            catch
            {
                response = new ResponseModel<Common>()
                {
                    Message = "Some Error has occured while saving Contact Us details",
                    Data = new Common()
                };
            }
            
            var json = JsonConvert.SerializeObject(response);
            return new RawJsonActionResult(json);
        }

        [HttpGet]
        [Route("/api/v1/getPrivacyPolicy")]
        public IActionResult GetPrivacyPolicy()
        {
            var topic = _topicService.GetTopicBySystemName("PrivacyInfo");
            var response = new ResponseModel<PrivacyResponseModel>()
            {
                Data = new PrivacyResponseModel()
                {
                    Id = topic.Id,
                    Title = topic.Title,
                    Description = topic.Body,
                    StatusLabel = "<label class='label label-success'>Active</label>",
                    Status= "Active",
                    CreatedAt = "2020-01-01",
                    CreatedBy = "DS e-commerce Admin"
                }
            };
            var json = JsonConvert.SerializeObject(response);
            return new RawJsonActionResult(json);
        }
    }
}
