﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Discounts;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.Images;
using Nop.Plugin.Api.DTOs.Products;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.ProductsParameters;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Plugin.Api.Helpers;

namespace Nop.Plugin.Api.Controllers
{
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Mvc;
    using DTOs.Errors;
    using JSON.Serializers;
    using Nop.Plugin.Api.Models.CustomersParameters;
    using Newtonsoft.Json;
    using Nop.Core;
    using Nop.Core.Domain.Customers;
    using Nop.Services.Messages;
    using Nop.Services.Events;
    using Nop.Core.Domain.Localization;
    using Nop.Services.Common;
    using Nop.Core.Domain.Orders;
    using NUglify.Helpers;
    using Nop.Plugin.Api.DataStructures;

    //[ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ProductsController : BaseApiController
    {
        private readonly IProductApiService _productApiService;
        private readonly IProductService _productService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IFactory<Product> _factory;
        private readonly IProductTagService _productTagService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IDTOHelper _dtoHelper;
        private readonly ICategoryService _categoryService;
        private readonly IPictureService _pictureService;
        private readonly IStoreContext _storeContext;
        private readonly IShoppingCartItemApiService _shoppingCartItemApiService;
        private readonly ILocalizationService _localizationService;
        private readonly CatalogSettings _catalogSettings;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly LocalizationSettings _localizationSettings;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICustomerApiService _customerApiService;
        private readonly ILogger _logger;

        public ProductsController(IProductApiService productApiService,
                                  IJsonFieldsSerializer jsonFieldsSerializer,
                                  IProductService productService,
                                  IUrlRecordService urlRecordService,
                                  ICustomerActivityService customerActivityService,
                                  ILocalizationService localizationService,
                                  IFactory<Product> factory,
                                  IAclService aclService,
                                  IStoreMappingService storeMappingService,
                                  IStoreService storeService,
                                  ICustomerService customerService,
                                  IDiscountService discountService,
                                  IPictureService pictureService,
                                  IManufacturerService manufacturerService,
                                  IProductTagService productTagService,
                                  IProductAttributeService productAttributeService,
                                  IDTOHelper dtoHelper,
                                  ICategoryService categoryService,
                                  IStoreContext storeContext, ILogger logger,
                                  IShoppingCartItemApiService shoppingCartItemApiService,
                                  IWorkContext workContext, CatalogSettings catalogSettings, ICustomerApiService customerApiService,
                                  IWorkflowMessageService workflowMessageService, IEventPublisher eventPublisher,
                                  LocalizationSettings localizationSettings, IGenericAttributeService genericAttributeService
                                  ) : base(jsonFieldsSerializer, aclService, customerService, storeMappingService, storeService, discountService, customerActivityService, localizationService, pictureService)
        {
            _productApiService = productApiService;
            _factory = factory;
            _manufacturerService = manufacturerService;
            _productTagService = productTagService;
            _urlRecordService = urlRecordService;
            _productService = productService;
            _productAttributeService = productAttributeService;
            _dtoHelper = dtoHelper;
            _categoryService = categoryService;
            _pictureService = pictureService;
            _storeContext = storeContext;
            _shoppingCartItemApiService = shoppingCartItemApiService;
            _localizationService = localizationService;
            _catalogSettings = catalogSettings;
            _workflowMessageService = workflowMessageService;
            _eventPublisher = eventPublisher;
            _customerActivityService = customerActivityService;
            _localizationSettings = localizationSettings;
            _genericAttributeService = genericAttributeService;
            _customerApiService = customerApiService;
            _logger = logger;
        }

        /// <summary>
        /// Receive a list of all products
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/products")]
        [ProducesResponseType(typeof(ProductsRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetProducts(ProductsParametersModel parameters)
        {
            if (parameters.Limit < Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "limit", "invalid limit parameter");
            }

            if (parameters.Page < Configurations.DefaultPageValue)
            {
                return Error(HttpStatusCode.BadRequest, "page", "invalid page parameter");
            }

            var allProducts = _productApiService.GetProducts(parameters.Ids, parameters.CreatedAtMin, parameters.CreatedAtMax, parameters.UpdatedAtMin,
                                                                        parameters.UpdatedAtMax, parameters.Limit, parameters.Page, parameters.SinceId, parameters.CategoryId,
                                                                        parameters.VendorName, parameters.PublishedStatus)
                                                .Where(p => StoreMappingService.Authorize(p));
            
            IList<ProductDto> productsAsDtos = allProducts.Select(product => _dtoHelper.PrepareProductDTO(product)).ToList();

            var productsRootObject = new ProductsRootObjectDto()
            {
                Products = productsAsDtos
            };

            var json = JsonFieldsSerializer.Serialize(productsRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/v1/getlisting")]
        public IActionResult GetListing(ProductListingRequestModel parameters)
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = headers ? _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Split("Bearer", 2)[1]).EntityId: 0;
            var customerDto = _customerApiService.GetCustomerById(customerId);
            var customer = _customerApiService.GetCustomerEntityById(customerId);

            var response = new ResponseModel<ProductsListingResponseModel>();

            try
            {
                if (ModelState.IsValid)
                {
                    if (parameters.Page < Configurations.DefaultPageValue)
                    {
                        response = new ResponseModel<ProductsListingResponseModel>()
                        {
                            Message = "Invalid Page Parameter",
                            Data = new ProductsListingResponseModel(),
                            Status = false
                        };
                        //return Error(HttpStatusCode.BadRequest, "page", "invalid page parameter");
                    }
                    else
                    {
                        var totalProducts = _productApiService.GetProducts(parameters.Title, parameters.MinPrice, parameters.MaxPrice, 10, parameters.Page, parameters.Category, parameters.SubCategory, parameters.Sort, parameters.Is_Featured, parameters.Is_BestSeller);

                        var allProducts = new ApiList<Product>(totalProducts, parameters.Page - 1, 10).ToList().Any() ? new ApiList<Product>(totalProducts, parameters.Page - 1, 10).ToList() : new List<Product>();
                        IList<ProductListingModel> productsAsDtos = allProducts.Count == 0 ? new List<ProductListingModel>() : allProducts.Select(product => new ProductListingModel()
                        {
                            Id = product.Id,
                            ProductCategoryId = product.ProductCategories.Any() ? product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId == 0 ? product.ProductCategories.FirstOrDefault().CategoryId : product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId : null,
                            ProductSubCategoryId = product.ProductCategories.Any() ? product.ProductCategories.FirstOrDefault()?.CategoryId : null,
                            Title = product.Name,
                            CategoryName = product.ProductCategories.Any() ? product.ProductCategories.FirstOrDefault().Category.ParentCategoryId == 0 ? product.ProductCategories.FirstOrDefault().Category.Name : _categoryService.GetCategoryById(product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId ?? 0)?.Name : null,
                            SubCategoryName = product.ProductCategories.Any() ? product.ProductCategories.FirstOrDefault()?.Category.Name : null,
                            Quantity = product.StockQuantity,
                            Price = product.Price,
                            AvgRating = Convert.ToInt32(product.ProductReviews.Any() ? product.ProductReviews.Average(review => review.Rating) : 0),
                            WishlistAdded = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Any() ? _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(x => x.ShoppingCartTypeId == (int)ShoppingCartType.Wishlist && x.ProductId == product.Id).Any() ? 1 : 0 : 0,
                            Description = product.ShortDescription,
                            PrimaryImage = _pictureService.GetPicturesByProductId(product.Id).Any() ? _pictureService.GetPictureUrl(_pictureService.GetPicturesByProductId(product.Id).FirstOrDefault(), 75, true) : null
                        }).OrderBy(ord => ord.Id).ToList();

                        var allParentCategories = _categoryService.GetAllCategoriesByParentCategoryId(0).Select(category => new CategoryListingModel()
                        {
                            Id = category.Id,
                            Category = category.Name,
                            ParentId = category.ParentCategoryId,
                            Status = category.Deleted ? 0 : 1,
                            CreatedAt = category.CreatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                            UpdatedAt = category.UpdatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                            SubCategory = _categoryService.GetAllCategoriesByParentCategoryId(category.Id).Select(subcategory => new SubCategoryListingModel()
                            {
                                Id = subcategory.Id,
                                Category = subcategory.Name,
                                ParentId = subcategory.ParentCategoryId,
                                Status = subcategory.Deleted ? 0 : 1,
                                CreatedAt = subcategory.CreatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                                UpdatedAt = subcategory.UpdatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                            }).ToList()
                        }).ToList();

                        var parentCategories = allParentCategories.Select(x => x.Id).ToList();
                        var allSubCategories = _categoryService.GetAllCategories().Where(subCategory => !parentCategories.Contains(subCategory.Id)).Select(
                            sub => new CategoryListingModel()
                            {
                                Id = sub.Id,
                                Category = sub.Name,
                                ParentId = sub.ParentCategoryId,
                                Status = sub.Deleted ? 0 : 1,
                                CreatedAt = sub.CreatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                                UpdatedAt = sub.UpdatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                                DeletedAt = DateTime.MinValue.ToString("yyyy/MM/dd HH:mm:ss")
                            }).ToList();

                        var productsRootObject = new ProductsListingResponseModel()
                        {
                            Products = productsAsDtos,
                            TotalRecords = totalProducts.ToList().Count,
                            RecordsPerPage = 10,
                            MaxPrice = productsAsDtos.Any() ? productsAsDtos.Max(x => x.Price ?? 0) : 0,
                            MinPrice = productsAsDtos.Any() ? productsAsDtos.Min(x => x.Price ?? 0) : 0,
                            Categories = allParentCategories,
                            SubCategories = allSubCategories,
                            CartCount = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Any() ? _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(x => x.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).ToList()?.Count ?? 0 : 0,
                        };

                        response = new ResponseModel<ProductsListingResponseModel>()
                        {
                            Message = "Data Found",
                            Data = productsRootObject
                        };
                    }
                }
                else
                {
                    response = new ResponseModel<ProductsListingResponseModel>()
                    {
                        Data = new ProductsListingResponseModel(),
                        Message = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).FirstOrDefault().Select(e => e.ErrorMessage).FirstOrDefault(),
                        Status = false
                    };
                }
            }
            catch
            {
                response = new ResponseModel<ProductsListingResponseModel>()
                {
                    Status = false,
                    Data = new ProductsListingResponseModel(),
                    Message = "There is some error in getting product listing"
                };
            }
            var json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });//JsonFieldsSerializer.Serialize(productsRootObject, "");

            return new RawJsonActionResult(json);
        }
        /// <summary>
        /// Receive a count of all products
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/products/count")]
        [ProducesResponseType(typeof(ProductsCountRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetProductsCount(ProductsCountParametersModel parameters)
        {
            var allProductsCount = _productApiService.GetProductsCount(parameters.CreatedAtMin, parameters.CreatedAtMax, parameters.UpdatedAtMin,
                                                                       parameters.UpdatedAtMax, parameters.PublishedStatus, parameters.VendorName,
                                                                       parameters.CategoryId);

            var productsCountRootObject = new ProductsCountRootObject()
            {
                Count = allProductsCount
            };

            return Ok(productsCountRootObject);
        }

        /// <summary>
        /// Retrieve product by spcified id
        /// </summary>
        /// <param name="id">Id of the product</param>
        /// <param name="fields">Fields from the product you want your json to contain</param>
        /// <response code="200">OK</response>
        /// <response code="404">Not Found</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/products/{id}")]
        [ProducesResponseType(typeof(ProductsRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetProductById(int id, string fields = "")
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            var product = _productApiService.GetProductById(id);

            if (product == null)
            {
                return Error(HttpStatusCode.NotFound, "product", "not found");
            }

            var productDto = _dtoHelper.PrepareProductDTO(product);

            var productsRootObject = new ProductsRootObjectDto();

            productsRootObject.Products.Add(productDto);

            var json = JsonFieldsSerializer.Serialize(productsRootObject, fields);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/v1/getDetail")]
        public IActionResult GetProductDetailById(ProductDetailRequestModel productDetail)
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = headers ? _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Split("Bearer", 2)[1]).EntityId : 0;

            var response = new ResponseModel<ProductsDetailResponseModel>()
            {
                Message = "Data Found",
                Data = new ProductsDetailResponseModel()
            };
            try
            {
                if (ModelState.IsValid)
                {
                    if (productDetail.Product_Id <= 0)
                    {
                        response = new ResponseModel<ProductsDetailResponseModel>()
                        {
                            Message = "Product Id is not valid",
                            Data = new ProductsDetailResponseModel()
                        };
                    }
                    else
                    {
                        var product = _productApiService.GetProductById(productDetail.Product_Id);

                        if (product == null)
                        {
                            response = new ResponseModel<ProductsDetailResponseModel>()
                            {
                                Message = "Product is not found",
                                Data = new ProductsDetailResponseModel()
                            };
                        }
                        else
                        {
                            var productAttributeValues = new List<ProductAttributeModel>();
                            var productDto = new ProductDetailModel()
                            {
                                Id = product.Id,
                                ProductCategoryId = product.ProductCategories.Any() ? product.ProductCategories.FirstOrDefault()?.Category?.ParentCategoryId == 0 ? product.ProductCategories.FirstOrDefault()?.CategoryId : product.ProductCategories.FirstOrDefault()?.Category?.ParentCategoryId : null,
                                ProductCategory = product.ProductCategories.Any() ? product.ProductCategories.FirstOrDefault()?.Category?.ParentCategoryId == 0 ? product.ProductCategories.FirstOrDefault()?.Category.Name : _categoryService.GetCategoryById(product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId ?? 0)?.Name : null,
                                ProductSubCategoryId = product.ProductCategories.Any() ? product.ProductCategories.FirstOrDefault()?.CategoryId : null,
                                ProductSubCategory = product.ProductCategories.Any() ? product.ProductCategories.FirstOrDefault()?.Category.Name : null,
                                Title = product.Name,
                                Description = product.FullDescription,
                                StockQuantity = product.StockQuantity,
                                Quantity = 0,
                                Price = product.Price,
                                PrimaryImage = _pictureService.GetPicturesByProductId(product.Id).Any() ? _pictureService.GetPictureUrl(_pictureService.GetPicturesByProductId(product.Id).FirstOrDefault(), 75, true) : null,
                                ProductImages = _pictureService.GetPicturesByProductId(product.Id).Any() ? _pictureService.GetPicturesByProductId(product.Id).Select(pict => new ProductImages()
                                {
                                    Image = _pictureService.GetPictureUrl(pict)
                                }).ToList() : new List<ProductImages>()
                            };

                            foreach (var map in product.ProductAttributeMappings.ToList())
                                productAttributeValues.AddRange(map.ProductAttributeValues.Select(x => new ProductAttributeModel()
                                {
                                    Id = map.Id,
                                    ProductId = product.Id,
                                    ProductAttributeId = map.ProductAttributeId,
                                    Value = map.ProductAttribute.Name,
                                    AttributeTypeValue = x.Name,
                                    CreatedAt = product.CreatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                                    UpdatedAt = product.UpdatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss")
                                }).ToList());

                            productDto.ProductAttribute = productAttributeValues;

                            var productDetailResponse = new ProductsDetailResponseModel()
                            {
                                ProductDetail = productDto,
                                SimilarProducts = _productService.GetAssociatedProducts(product.Id, _storeContext.CurrentStore.Id).Any() ? _productService.GetAssociatedProducts(product.Id, _storeContext.CurrentStore.Id).Select(assosciate => new SimilarProductDetailModel()
                                {
                                    Id = assosciate.Id,
                                    ProductCategoryId = assosciate.ProductCategories.Any() ? assosciate.ProductCategories.FirstOrDefault()?.Category?.ParentCategoryId == 0 ? assosciate.ProductCategories.FirstOrDefault()?.CategoryId : assosciate.ProductCategories.FirstOrDefault().Category?.ParentCategoryId : null,
                                    ProductCategory = assosciate.ProductCategories.Any() ? assosciate.ProductCategories.FirstOrDefault()?.Category?.ParentCategoryId == 0 ? assosciate.ProductCategories.FirstOrDefault()?.Category.Name : _categoryService.GetCategoryById(assosciate.ProductCategories.FirstOrDefault()?.Category?.ParentCategoryId ?? 0).Name : null,
                                    ProductSubCategoryId = assosciate.ProductCategories.Any() ? assosciate.ProductCategories.FirstOrDefault()?.CategoryId : null,
                                    ProductSubCategory = assosciate.ProductCategories.Any() ? assosciate.ProductCategories.FirstOrDefault()?.Category.Name : null,
                                    Title = assosciate.Name,
                                    Description = assosciate.FullDescription,
                                    Quantity = assosciate.StockQuantity,
                                    Price = assosciate.Price,
                                    PrimaryImage = _pictureService.GetPicturesByProductId(assosciate.Id).Any() ? _pictureService.GetPictureUrl(_pictureService.GetPicturesByProductId(assosciate.Id).FirstOrDefault(), 75, true) : null,
                                    AvgRating = Convert.ToInt32(assosciate.ProductReviews.Any() ? assosciate.ProductReviews.Average(review => review.Rating) : 0),
                                    WishlistAdded = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Any() ? _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(x => x.ShoppingCartTypeId == (int)ShoppingCartType.Wishlist && x.ProductId == assosciate.Id).Any() ? 1 : 0 : 0
                                }).ToList() : new List<SimilarProductDetailModel>(),
                                CartCount = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Any() ? _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(x => x.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).ToList()?.Count : 0,
                                AllReviews = product.ProductReviews.Select(review => new ProductReviewModel()
                                {
                                    Id = review.Id,
                                    Title = review.Title,
                                    Description = review.ReviewText,
                                    Star = review.Rating,
                                    CustomerId = review.CustomerId,
                                    ProductId = product.Id,
                                    CreatedAt = product.CreatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss")
                                }).ToList(),
                                AvgRating = Convert.ToInt32(product.ProductReviews.Any() ? product.ProductReviews.Average(review => review.Rating) : 0),
                                WishlistAdded = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(x => x.ShoppingCartTypeId == (int)ShoppingCartType.Wishlist && x.ProductId == product.Id).Any() ? 1 : 0,
                                Colors = new List<Common>(),
                                Weight = new List<Common>(),
                                Size = new List<Common>()
                            };
                            response = new ResponseModel<ProductsDetailResponseModel>()
                            {
                                Message = "Data Found",
                                Data = productDetailResponse
                            };
                        }
                    }
                }
                else
                {
                    response = new ResponseModel<ProductsDetailResponseModel>()
                    {
                        Message = ModelState.Values.Where(x => x.Errors.Count > 0).FirstOrDefault().Errors.FirstOrDefault().ErrorMessage,
                        Data = new ProductsDetailResponseModel(),
                        Status = false
                    };
                }
            }
            catch
            {
                response = new ResponseModel<ProductsDetailResponseModel>()
                {
                    Status = false,
                    Data = new ProductsDetailResponseModel(),
                    Message  = "There is some error in getting product detail"
                };
            }
            var json = JsonConvert.SerializeObject(response);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/products")]
        [ProducesResponseType(typeof(ProductsRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult CreateProduct([ModelBinder(typeof(JsonModelBinder<ProductDto>))] Delta<ProductDto> productDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            // Inserting the new product
            var product = _factory.Initialize();
            productDelta.Merge(product);

            _productService.InsertProduct(product);

            UpdateProductPictures(product, productDelta.Dto.Images);

            UpdateProductTags(product, productDelta.Dto.Tags);

            UpdateProductManufacturers(product, productDelta.Dto.ManufacturerIds);

            UpdateAssociatedProducts(product, productDelta.Dto.AssociatedProductIds);

            //search engine name
            var seName = _urlRecordService.ValidateSeName(product, productDelta.Dto.SeName, product.Name, true);
            _urlRecordService.SaveSlug(product, seName, 0);

            UpdateAclRoles(product, productDelta.Dto.RoleIds);

            UpdateDiscountMappings(product, productDelta.Dto.DiscountIds);

            UpdateStoreMappings(product, productDelta.Dto.StoreIds);

            _productService.UpdateProduct(product);

            CustomerActivityService.InsertActivity("AddNewProduct",
                LocalizationService.GetResource("ActivityLog.AddNewProduct"), product);

            // Preparing the result dto of the new product
            var productDto = _dtoHelper.PrepareProductDTO(product);

            var productsRootObject = new ProductsRootObjectDto();

            productsRootObject.Products.Add(productDto);

            var json = JsonFieldsSerializer.Serialize(productsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpPut]
        [Route("/api/products/{id}")]
        [ProducesResponseType(typeof(ProductsRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        public IActionResult UpdateProduct([ModelBinder(typeof(JsonModelBinder<ProductDto>))] Delta<ProductDto> productDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            var product = _productApiService.GetProductById(productDelta.Dto.Id);

            if (product == null)
            {
                return Error(HttpStatusCode.NotFound, "product", "not found");
            }

            productDelta.Merge(product);

            product.UpdatedOnUtc = DateTime.UtcNow;
            _productService.UpdateProduct(product);

            UpdateProductAttributes(product, productDelta);

            UpdateProductPictures(product, productDelta.Dto.Images);

            UpdateProductTags(product, productDelta.Dto.Tags);

            UpdateProductManufacturers(product, productDelta.Dto.ManufacturerIds);

            UpdateAssociatedProducts(product, productDelta.Dto.AssociatedProductIds);

            // Update the SeName if specified
            if (productDelta.Dto.SeName != null)
            {
                var seName = _urlRecordService.ValidateSeName(product, productDelta.Dto.SeName, product.Name, true);
                _urlRecordService.SaveSlug(product, seName, 0);
            }

            UpdateDiscountMappings(product, productDelta.Dto.DiscountIds);

            UpdateStoreMappings(product, productDelta.Dto.StoreIds);

            UpdateAclRoles(product, productDelta.Dto.RoleIds);

            _productService.UpdateProduct(product);

            CustomerActivityService.InsertActivity("UpdateProduct",
               LocalizationService.GetResource("ActivityLog.UpdateProduct"), product);

            // Preparing the result dto of the new product
            var productDto = _dtoHelper.PrepareProductDTO(product);

            var productsRootObject = new ProductsRootObjectDto();

            productsRootObject.Products.Add(productDto);

            var json = JsonFieldsSerializer.Serialize(productsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpDelete]
        [Route("/api/products/{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult DeleteProduct(int id)
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            var product = _productApiService.GetProductById(id);

            if (product == null)
            {
                return Error(HttpStatusCode.NotFound, "product", "not found");
            }

            _productService.DeleteProduct(product);

            //activity log
            CustomerActivityService.InsertActivity("DeleteProduct",
                string.Format(LocalizationService.GetResource("ActivityLog.DeleteProduct"), product.Name), product);

            return new RawJsonActionResult("{}");
        }

        [HttpPost]
        [Route("api/v1/addReview")]
        public virtual IActionResult ProductReviewsAdd(ProductReviewRequestModel model)
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Split("Bearer", 2)[1]).EntityId;
            var customerDto = _customerApiService.GetCustomerById(customerId);
            var customer = _customerApiService.GetCustomerEntityById(customerId);
            var response = new ResponseModel<ProductReviewResponseModel>();
            var product = _productService.GetProductById(model.Product_Id);
            if (product == null || product.Deleted)
                return new RawJsonActionResult(JsonConvert.SerializeObject(new ResponseModel<ProductReviewResponseModel>()
                {
                    Message = "Product was not found",
                    Data = new ProductReviewResponseModel(),
                    Status = false,
                }));

            if (ModelState.IsValid)
            {
                //save review
                var rating = model.Star;
                if (rating < 1 || rating > 5)
                    rating = _catalogSettings.DefaultProductRatingValue;
                var isApproved = !_catalogSettings.ProductReviewsMustBeApproved;
                if (product.ProductReviews.Any(review => review.CustomerId == customerId))
                {
                    response = new ResponseModel<ProductReviewResponseModel>()
                    {
                        Data = new ProductReviewResponseModel(),
                        Message = "You already submitted the review",
                        Status = false
                    };
                }
                else
                {
                    var productReview = new ProductReview
                    {
                        ProductId = product.Id,
                        CustomerId = customerId,
                        Title = model.Title,
                        ReviewText = model.Description,
                        Rating = Convert.ToInt32(rating),
                        HelpfulYesTotal = 0,
                        HelpfulNoTotal = 0,
                        IsApproved = isApproved,
                        CreatedOnUtc = DateTime.UtcNow,
                        StoreId = _storeContext.CurrentStore.Id,
                    };

                    product.ProductReviews.Add(productReview);

                    //update product totals
                    _productService.UpdateProductReviewTotals(product);

                    //notify store owner
                    if (_catalogSettings.NotifyStoreOwnerAboutNewProductReviews)
                        _workflowMessageService.SendProductReviewNotificationMessage(productReview, _localizationSettings.DefaultAdminLanguageId);

                    //activity log
                    _customerActivityService.InsertActivity("PublicStore.AddProductReview",
                        string.Format(_localizationService.GetResource("ActivityLog.PublicStore.AddProductReview"), product.Name), product);

                    //raise event
                    if (productReview.IsApproved)
                        _eventPublisher.Publish(new ProductReviewApprovedEvent(productReview));

                    response = new ResponseModel<ProductReviewResponseModel>()
                    {
                        Data = new ProductReviewResponseModel(),
                        Message = "Review Submitted successfully"
                    };
                }
            }
            var json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() {NullValueHandling = NullValueHandling.Ignore });
            return new RawJsonActionResult(json);
        }

        private void UpdateProductPictures(Product entityToUpdate, List<ImageMappingDto> setPictures)
        {
            // If no pictures are specified means we don't have to update anything
            if (setPictures == null)
                return;

            // delete unused product pictures
            var unusedProductPictures = entityToUpdate.ProductPictures.Where(x => setPictures.All(y => y.Id != x.Id)).ToList();
            foreach (var unusedProductPicture in unusedProductPictures)
            {
                var picture = PictureService.GetPictureById(unusedProductPicture.PictureId);
                if (picture == null)
                    throw new ArgumentException("No picture found with the specified id");
                PictureService.DeletePicture(picture);
            }

            foreach (var imageDto in setPictures)
            {
                if (imageDto.Id > 0)
                {
                    // update existing product picture
                    var productPictureToUpdate = entityToUpdate.ProductPictures.FirstOrDefault(x => x.Id == imageDto.Id);
                    if (productPictureToUpdate != null && imageDto.Position > 0)
                    {
                        productPictureToUpdate.DisplayOrder = imageDto.Position;
                        _productService.UpdateProductPicture(productPictureToUpdate);
                    }
                }
                else
                {
                    // add new product picture
                    var newPicture = PictureService.InsertPicture(imageDto.Binary, imageDto.MimeType, string.Empty);
                    _productService.InsertProductPicture(new ProductPicture()
                    {
                        PictureId = newPicture.Id,
                        ProductId = entityToUpdate.Id,
                        DisplayOrder = imageDto.Position
                    });
                }
            }
        }

        private void UpdateProductAttributes(Product entityToUpdate, Delta<ProductDto> productDtoDelta)
        {
            // If no product attribute mappings are specified means we don't have to update anything
            if (productDtoDelta.Dto.ProductAttributeMappings == null)
                return;

            // delete unused product attribute mappings
            var toBeUpdatedIds = productDtoDelta.Dto.ProductAttributeMappings.Where(y => y.Id != 0).Select(x => x.Id);

            var unusedProductAttributeMappings = entityToUpdate.ProductAttributeMappings.Where(x => !toBeUpdatedIds.Contains(x.Id)).ToList();

            foreach (var unusedProductAttributeMapping in unusedProductAttributeMappings)
            {
                _productAttributeService.DeleteProductAttributeMapping(unusedProductAttributeMapping);
            }

            foreach (var productAttributeMappingDto in productDtoDelta.Dto.ProductAttributeMappings)
            {
                if (productAttributeMappingDto.Id > 0)
                {
                    // update existing product attribute mapping
                    var productAttributeMappingToUpdate = entityToUpdate.ProductAttributeMappings.FirstOrDefault(x => x.Id == productAttributeMappingDto.Id);
                    if (productAttributeMappingToUpdate != null)
                    {
                        productDtoDelta.Merge(productAttributeMappingDto,productAttributeMappingToUpdate,false);
                       
                        _productAttributeService.UpdateProductAttributeMapping(productAttributeMappingToUpdate);

                        UpdateProductAttributeValues(productAttributeMappingDto, productDtoDelta);
                    }
                }
                else
                {
                    var newProductAttributeMapping = new ProductAttributeMapping {ProductId = entityToUpdate.Id};

                    productDtoDelta.Merge(productAttributeMappingDto, newProductAttributeMapping);

                    // add new product attribute
                    _productAttributeService.InsertProductAttributeMapping(newProductAttributeMapping);
                }
            }
        }

        private void UpdateProductAttributeValues(ProductAttributeMappingDto productAttributeMappingDto, Delta<ProductDto> productDtoDelta)
        {
            // If no product attribute values are specified means we don't have to update anything
            if (productAttributeMappingDto.ProductAttributeValues == null)
                return;

            // delete unused product attribute values
            var toBeUpdatedIds = productAttributeMappingDto.ProductAttributeValues.Where(y => y.Id != 0).Select(x => x.Id);

            var unusedProductAttributeValues =
                _productAttributeService.GetProductAttributeValues(productAttributeMappingDto.Id).Where(x => !toBeUpdatedIds.Contains(x.Id)).ToList();

            foreach (var unusedProductAttributeValue in unusedProductAttributeValues)
            {
                _productAttributeService.DeleteProductAttributeValue(unusedProductAttributeValue);
            }

            foreach (var productAttributeValueDto in productAttributeMappingDto.ProductAttributeValues)
            {
                if (productAttributeValueDto.Id > 0)
                {
                    // update existing product attribute mapping
                    var productAttributeValueToUpdate =
                        _productAttributeService.GetProductAttributeValueById(productAttributeValueDto.Id);
                    if (productAttributeValueToUpdate != null)
                    {
                        productDtoDelta.Merge(productAttributeValueDto, productAttributeValueToUpdate, false);

                        _productAttributeService.UpdateProductAttributeValue(productAttributeValueToUpdate);
                    }
                }
                else
                {
                    var newProductAttributeValue = new ProductAttributeValue();
                    productDtoDelta.Merge(productAttributeValueDto, newProductAttributeValue);

                    newProductAttributeValue.ProductAttributeMappingId = productAttributeMappingDto.Id;
                    // add new product attribute value
                    _productAttributeService.InsertProductAttributeValue(newProductAttributeValue);
                }
            }
        }

        private void UpdateProductTags(Product product, IReadOnlyCollection<string> productTags)
        {
            if (productTags == null)
                return;

            if (product == null)
                throw new ArgumentNullException(nameof(product));

            //Copied from UpdateProductTags method of ProductTagService
            //product tags
            var existingProductTags = _productTagService.GetAllProductTagsByProductId(product.Id);
            var productTagsToRemove = new List<ProductTag>();
            foreach (var existingProductTag in existingProductTags)
            {
                var found = false;
                foreach (var newProductTag in productTags)
                {
                    if (!existingProductTag.Name.Equals(newProductTag, StringComparison.InvariantCultureIgnoreCase))
                        continue;

                    found = true;
                    break;
                }

                if (!found)
                {
                    productTagsToRemove.Add(existingProductTag);
                }
            }

            foreach (var productTag in productTagsToRemove)
            {
                //product.ProductTags.Remove(productTag);
                product.ProductProductTagMappings
                    .Remove(product.ProductProductTagMappings.FirstOrDefault(mapping => mapping.ProductTagId == productTag.Id));
                _productService.UpdateProduct(product);
            }

            foreach (var productTagName in productTags)
            {
                ProductTag productTag;
                var productTag2 = _productTagService.GetProductTagByName(productTagName);
                if (productTag2 == null)
                {
                    //add new product tag
                    productTag = new ProductTag
                    {
                        Name = productTagName
                    };
                    _productTagService.InsertProductTag(productTag);
                }
                else
                {
                    productTag = productTag2;
                }

                if (!_productService.ProductTagExists(product, productTag.Id))
                {
                    product.ProductProductTagMappings.Add(new ProductProductTagMapping { ProductTag = productTag });
                    _productService.UpdateProduct(product);
                }

                var seName = _urlRecordService.ValidateSeName(productTag, string.Empty, productTag.Name, true);
                _urlRecordService.SaveSlug(productTag, seName, 0);
            }
        }

        private void UpdateDiscountMappings(Product product, List<int> passedDiscountIds)
        {
            if (passedDiscountIds == null)
                return;

            var allDiscounts = DiscountService.GetAllDiscounts(DiscountType.AssignedToSkus, showHidden: true);

            foreach (var discount in allDiscounts)
            {
                if (passedDiscountIds.Contains(discount.Id))
                {
                    //new discount
                    if (product.AppliedDiscounts.Count(d => d.Id == discount.Id) == 0)
                        product.AppliedDiscounts.Add(discount);
                }
                else
                {
                    //remove discount
                    if (product.AppliedDiscounts.Count(d => d.Id == discount.Id) > 0)
                        product.AppliedDiscounts.Remove(discount);
                }
            }

            _productService.UpdateProduct(product);
            _productService.UpdateHasDiscountsApplied(product);
        }
        
        private void UpdateProductManufacturers(Product product, List<int> passedManufacturerIds)
        {
            // If no manufacturers specified then there is nothing to map 
            if (passedManufacturerIds == null)
                return;

            var unusedProductManufacturers = product.ProductManufacturers.Where(x => !passedManufacturerIds.Contains(x.ManufacturerId)).ToList();

            // remove all manufacturers that are not passed
            foreach (var unusedProductManufacturer in unusedProductManufacturers)
            {
                _manufacturerService.DeleteProductManufacturer(unusedProductManufacturer);
            }

            foreach (var passedManufacturerId in passedManufacturerIds)
            {
                // not part of existing manufacturers so we will create a new one
                if (product.ProductManufacturers.All(x => x.ManufacturerId != passedManufacturerId))
                {
                    // if manufacturer does not exist we simply ignore it, otherwise add it to the product
                    var manufacturer = _manufacturerService.GetManufacturerById(passedManufacturerId);
                    if (manufacturer != null)
                    {
                        _manufacturerService.InsertProductManufacturer(new ProductManufacturer()
                        { ProductId = product.Id, ManufacturerId = manufacturer.Id });
                    }
                }
            }
        }

        private void UpdateAssociatedProducts(Product product, List<int> passedAssociatedProductIds)
        {
            // If no associated products specified then there is nothing to map 
            if (passedAssociatedProductIds == null)
                return;

            var noLongerAssociatedProducts =
                _productService.GetAssociatedProducts(product.Id, showHidden: true)
                    .Where(p => !passedAssociatedProductIds.Contains(p.Id));

            // update all products that are no longer associated with our product
            foreach (var noLongerAssocuatedProduct in noLongerAssociatedProducts)
            {
                noLongerAssocuatedProduct.ParentGroupedProductId = 0;
                _productService.UpdateProduct(noLongerAssocuatedProduct);
            }

            var newAssociatedProducts = _productService.GetProductsByIds(passedAssociatedProductIds.ToArray());
            foreach (var newAssociatedProduct in newAssociatedProducts)
            {
                newAssociatedProduct.ParentGroupedProductId = product.Id;
                _productService.UpdateProduct(newAssociatedProduct);
            }
        }
    }
}