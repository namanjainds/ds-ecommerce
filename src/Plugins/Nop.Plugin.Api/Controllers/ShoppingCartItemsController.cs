﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.ShoppingCarts;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.ShoppingCartsParameters;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Plugin.Api.Helpers;
using Nop.Core;

namespace Nop.Plugin.Api.Controllers
{
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Mvc;
    using DTOs.Errors;
    using JSON.Serializers;
    using Nop.Plugin.Api.Models.CustomersParameters;
    using Newtonsoft.Json;
    using Nop.Services.Common;
    using Nop.Core.Domain.Customers;

    //[ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ShoppingCartItemsController : BaseApiController
    {
        private readonly IShoppingCartItemApiService _shoppingCartItemApiService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IProductService _productService;
        private readonly IFactory<ShoppingCartItem> _factory;
        private readonly IProductAttributeConverter _productAttributeConverter;
        private readonly IDTOHelper _dtoHelper;
        private readonly IStoreContext _storeContext;
        private readonly IPictureService _pictureService;
        private readonly ICategoryService _categoryService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICustomerApiService _customerApiService;

        public ShoppingCartItemsController(IShoppingCartItemApiService shoppingCartItemApiService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IShoppingCartService shoppingCartService,
            IProductService productService,
            IFactory<ShoppingCartItem> factory,
            IPictureService pictureService,
            IProductAttributeConverter productAttributeConverter,
            IDTOHelper dtoHelper,
            IStoreContext storeContext, ICustomerApiService customerApiService,
            ICategoryService categoryService, IGenericAttributeService genericAttributeService)
            : base(jsonFieldsSerializer,
                 aclService,
                 customerService,
                 storeMappingService,
                 storeService,
                 discountService,
                 customerActivityService,
                 localizationService,
                 pictureService)
        {
            _shoppingCartItemApiService = shoppingCartItemApiService;
            _shoppingCartService = shoppingCartService;
            _productService = productService;
            _factory = factory;
            _productAttributeConverter = productAttributeConverter;
            _dtoHelper = dtoHelper;
            _storeContext = storeContext;
            _pictureService = pictureService;
            _categoryService = categoryService;
            _genericAttributeService = genericAttributeService;
            _customerApiService = customerApiService;
        }

        /// <summary>
        /// Receive a list of all shopping cart items
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/shopping_cart_items")]
        [ProducesResponseType(typeof(ShoppingCartItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetShoppingCartItems(ShoppingCartItemsParametersModel parameters)
        {
            if (parameters.Limit < Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "limit", "invalid limit parameter");
            }

            if (parameters.Page < Configurations.DefaultPageValue)
            {
                return Error(HttpStatusCode.BadRequest, "page", "invalid page parameter");
            }

            IList<ShoppingCartItem> shoppingCartItems = _shoppingCartItemApiService.GetShoppingCartItems(customerId: null,
                                                                                                         createdAtMin: parameters.CreatedAtMin,
                                                                                                         createdAtMax: parameters.CreatedAtMax,
                                                                                                         updatedAtMin: parameters.UpdatedAtMin,
                                                                                                         updatedAtMax: parameters.UpdatedAtMax,
                                                                                                         limit: parameters.Limit,
                                                                                                         page: parameters.Page);

            var shoppingCartItemsDtos = shoppingCartItems.Select(shoppingCartItem =>
            {
                return _dtoHelper.PrepareShoppingCartItemDTO(shoppingCartItem);
            }).ToList();

            var shoppingCartsRootObject = new ShoppingCartItemsRootObject()
            {
                ShoppingCartItems = shoppingCartItemsDtos
            };

            var json = JsonFieldsSerializer.Serialize(shoppingCartsRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// Receive a list of all shopping cart items by customer id
        /// </summary>
        /// <param name="customerId">Id of the customer whoes shopping cart items you want to get</param>
        /// <param name="parameters"></param>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="404">Not Found</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/shopping_cart_items/{customerId}")]
        [ProducesResponseType(typeof(ShoppingCartItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetShoppingCartItemsByCustomerId(int customerId, ShoppingCartItemsForCustomerParametersModel parameters)
        {
            if (customerId <= Configurations.DefaultCustomerId)
            {
                return Error(HttpStatusCode.BadRequest, "customer_id", "invalid customer_id");
            }

            if (parameters.Limit < Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "limit", "invalid limit parameter");
            }

            if (parameters.Page < Configurations.DefaultPageValue)
            {
                return Error(HttpStatusCode.BadRequest, "page", "invalid page parameter");
            }

            IList<ShoppingCartItem> shoppingCartItems = _shoppingCartItemApiService.GetShoppingCartItems(customerId,
                                                                                                         parameters.CreatedAtMin,
                                                                                                         parameters.CreatedAtMax, parameters.UpdatedAtMin,
                                                                                                         parameters.UpdatedAtMax, parameters.Limit,
                                                                                                         parameters.Page);

            if (shoppingCartItems == null)
            {
                return Error(HttpStatusCode.NotFound, "shopping_cart_item", "not found");
            }

            var shoppingCartItemsDtos = shoppingCartItems
                .Select(shoppingCartItem => _dtoHelper.PrepareShoppingCartItemDTO(shoppingCartItem))
                .ToList();

            var shoppingCartsRootObject = new ShoppingCartItemsRootObject()
            {
                ShoppingCartItems = shoppingCartItemsDtos
            };

            var json = JsonFieldsSerializer.Serialize(shoppingCartsRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        [HttpGet]
        [Route("/api/v1/cartListing")]
        public IActionResult GetShoppingCartListByCustomerId()
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Split("Bearer", 2)[1]).EntityId;
            var customer = _customerApiService.GetCustomerEntityById(customerId);
            var response = new ResponseModel<List<ShoppingCartModel>>()
            {
                Message = "Successfully got cart listing",
                Data = new List<ShoppingCartModel>()
            };
            try
            {
                IList<ShoppingCartItem> shoppingCartItems = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(cart => cart.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).ToList();

                if (shoppingCartItems == null || shoppingCartItems.Count == 0)
                {
                    response = new ResponseModel<List<ShoppingCartModel>>()
                    {
                        Message = "No items found in the cart",
                        Status = false,
                        Data = new List<ShoppingCartModel>()
                    };                    
                }
                else
                { 
                var cartModels = shoppingCartItems.Select(item => new ShoppingCartModel()
                {
                    Id = item.Id,
                    ProductId = item.ProductId,
                    Title = item.Product?.Name,
                    AvailableQuantity = item.Product?.StockQuantity,
                    Price = item.Product?.Price,
                    Quantity = item.Quantity,
                    PrimaryImage = _pictureService.GetPicturesByProductId(item.ProductId).Any() ? _pictureService.GetPictureUrl(_pictureService.GetPicturesByProductId(item.ProductId)?.FirstOrDefault(), 75, true) : "",
                    CategoryId = item.Product.ProductCategories.Any() ? item.Product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId == 0 ? item.Product.ProductCategories.FirstOrDefault().CategoryId : item.Product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId : 0,
                    CategoryTitle = item.Product.ProductCategories.Any() ? item.Product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId == 0 ? item.Product.ProductCategories.FirstOrDefault().Category.Name : _categoryService.GetCategoryById(item.Product.ProductCategories.FirstOrDefault()?.Category?.ParentCategoryId ?? 0)?.Name : "",
                    SubCategoryId = item.Product.ProductCategories.Any() ? item.Product.ProductCategories.FirstOrDefault()?.CategoryId : 0,
                    SubCategoryTitle = item.Product.ProductCategories.Any() ? item.Product.ProductCategories.FirstOrDefault()?.Category?.Name : "",
                    Colors = new Common(),
                    Size = new Common(),
                    Weight = new Common()
                }).ToList();
                response = new ResponseModel<List<ShoppingCartModel>>()
                {
                    Message = "Successfully got cart listing",
                    Data = cartModels
                };
                //var shoppingCartItemsDtos = shoppingCartItems
                //    .Select(shoppingCartItem => _dtoHelper.PrepareShoppingCartItemDTO(shoppingCartItem))
                //    .ToList();

                //var shoppingCartsRootObject = new ShoppingCartItemsRootObject()
                //{
                //    ShoppingCartItems = shoppingCartItemsDtos
                //};
                }
            }
            catch
            {
                response = new ResponseModel<List<ShoppingCartModel>>()
                {
                    Message = "Error during cart listing for customer "+ customerId,
                    Data = new List<ShoppingCartModel>(),
                    Status = false
                };
            }
            

            var json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() {NullValueHandling = NullValueHandling.Ignore });//JsonFieldsSerializer.Serialize(shoppingCartsRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/shopping_cart_items")]
        [ProducesResponseType(typeof(ShoppingCartItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), 422)]
        public IActionResult CreateShoppingCartItem([ModelBinder(typeof(JsonModelBinder<ShoppingCartItemDto>))] Delta<ShoppingCartItemDto> shoppingCartItemDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            var newShoppingCartItem = _factory.Initialize();
            shoppingCartItemDelta.Merge(newShoppingCartItem);

            // We know that the product id and customer id will be provided because they are required by the validator.
            // TODO: validate
            var product = _productService.GetProductById(newShoppingCartItem.ProductId);

            if (product == null)
            {
                return Error(HttpStatusCode.NotFound, "product", "not found");
            }

            var customer = CustomerService.GetCustomerById(newShoppingCartItem.CustomerId);

            if (customer == null)
            {
                return Error(HttpStatusCode.NotFound, "customer", "not found");
            }

            var shoppingCartType = (ShoppingCartType)Enum.Parse(typeof(ShoppingCartType), shoppingCartItemDelta.Dto.ShoppingCartType);

            if (!product.IsRental)
            {
                newShoppingCartItem.RentalStartDateUtc = null;
                newShoppingCartItem.RentalEndDateUtc = null;
            }

            var attributesXml = _productAttributeConverter.ConvertToXml(shoppingCartItemDelta.Dto.Attributes, product.Id);

            var currentStoreId = _storeContext.CurrentStore.Id;

            var warnings = _shoppingCartService.AddToCart(customer, product, shoppingCartType, currentStoreId, attributesXml, 0M,
                                        newShoppingCartItem.RentalStartDateUtc, newShoppingCartItem.RentalEndDateUtc,
                                        shoppingCartItemDelta.Dto.Quantity ?? 1);

            if (warnings.Count > 0)
            {
                foreach (var warning in warnings)
                {
                    ModelState.AddModelError("shopping cart item", warning);
                }

                return Error(HttpStatusCode.BadRequest);
            }
            else {
                // the newly added shopping cart item should be the last one
                newShoppingCartItem = customer.ShoppingCartItems.LastOrDefault();
            }

            // Preparing the result dto of the new product category mapping
            var newShoppingCartItemDto = _dtoHelper.PrepareShoppingCartItemDTO(newShoppingCartItem);

            var shoppingCartsRootObject = new ShoppingCartItemsRootObject();

            shoppingCartsRootObject.ShoppingCartItems.Add(newShoppingCartItemDto);

            var json = JsonFieldsSerializer.Serialize(shoppingCartsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/v1/addOrUpdateToCart")]
        public IActionResult AddOrUpdateToCart(ShoppingCartRequestModel model)
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Split("Bearer", 2)[1]).EntityId;
            var customer = _customerApiService.GetCustomerEntityById(customerId);
            var response = new ResponseModel<ShoppingCartResponseModel>() {
                Message = "There is some error while adding or updating cart",
                Data = new ShoppingCartResponseModel()
            };

            // Here we display the errors if the validation has failed at some point.
            try
            {
                if (!ModelState.IsValid)
                {
                    response = new ResponseModel<ShoppingCartResponseModel>()
                    {
                        Message = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).FirstOrDefault().Select(e => e.ErrorMessage).FirstOrDefault(),
                        Data = new ShoppingCartResponseModel(),
                        Status = false
                    };
                    //return Error(errorMessage: "There was some error with request for customer " + customerId);
                }
                else
                {
                    if (model.Type.ToLower() == "add")
                    {
                        var newShoppingCartItem = _factory.Initialize();
                        // We know that the product id and customer id will be provided because they are required by the validator.
                        // TODO: validate
                        var product = _productService.GetProductById(!string.IsNullOrEmpty(model.Product_Id) ? Convert.ToInt32(model.Product_Id) : 0);

                        if (product == null)
                        {
                            return Error(HttpStatusCode.NotFound, "product", "not found");
                        }


                        if (customer == null)
                        {
                            return Error(HttpStatusCode.NotFound, "customer", "not found");
                        }

                        var shoppingCartType = (ShoppingCartType)Enum.Parse(typeof(ShoppingCartType), ShoppingCartType.ShoppingCart.ToString());

                        if (!product.IsRental)
                        {
                            newShoppingCartItem.RentalStartDateUtc = null;
                            newShoppingCartItem.RentalEndDateUtc = null;
                        }

                        var attributesXml = "";

                        var currentStoreId = _storeContext.CurrentStore.Id;

                        var warnings = _shoppingCartService.AddToCart(customer, product, shoppingCartType, currentStoreId, attributesXml, 0M,
                                                    newShoppingCartItem.RentalStartDateUtc, newShoppingCartItem.RentalEndDateUtc,
                                                    !string.IsNullOrEmpty(model.Quantity) ? Convert.ToInt32(model.Quantity) : 1);

                        if (warnings.Count > 0)
                        {
                            foreach (var warning in warnings)
                            {
                                ModelState.AddModelError("shopping cart item", warning);
                            }

                            return Error(HttpStatusCode.BadRequest);
                        }
                        var data = new ShoppingCartResponseModel()
                        {
                            CartCount = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(x => x.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).ToList().Count
                        };
                        response = new ResponseModel<ShoppingCartResponseModel>()
                        {
                            Message = "Added to Cart Successfully",
                            Data = data
                        };
                    }
                    //// Preparing the result dto of the new product category mapping
                    //var newShoppingCartItemDto = _dtoHelper.PrepareShoppingCartItemDTO(newShoppingCartItem);

                    //var shoppingCartsRootObject = new ShoppingCartItemsRootObject();

                    //shoppingCartsRootObject.ShoppingCartItems.Add(newShoppingCartItemDto);

                    // We kno that the id will be valid integer because the validation for this happens in the validator which is executed by the model binder.
                    else if (model.Type.ToLower() == "update")
                    {
                        var cartItem = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(shop => shop.ProductId ==
                        (!string.IsNullOrEmpty(model.Product_Id) ? Convert.ToInt32(model.Product_Id) : 0) && shop.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).FirstOrDefault();

                        var shoppingCartItemForUpdate = _shoppingCartItemApiService.GetShoppingCartItem(cartItem.Id);

                        if (shoppingCartItemForUpdate == null)
                        {
                            return Error(HttpStatusCode.NotFound, "shopping_cart_item", "not found for customer " + customerId);
                        }

                        if (!shoppingCartItemForUpdate.Product.IsRental)
                        {
                            shoppingCartItemForUpdate.RentalStartDateUtc = null;
                            shoppingCartItemForUpdate.RentalEndDateUtc = null;
                        }

                        shoppingCartItemForUpdate.AttributesXml = "";


                        // The update time is set in the service.
                        var warnings = _shoppingCartService.UpdateShoppingCartItem(shoppingCartItemForUpdate.Customer, shoppingCartItemForUpdate.Id,
                            shoppingCartItemForUpdate.AttributesXml, shoppingCartItemForUpdate.CustomerEnteredPrice,
                            shoppingCartItemForUpdate.RentalStartDateUtc, shoppingCartItemForUpdate.RentalEndDateUtc,
                            !string.IsNullOrEmpty(model.Quantity) ? Convert.ToInt32(model.Quantity) : 1);

                        if (warnings.Count > 0)
                        {
                            foreach (var warning in warnings)
                            {
                                ModelState.AddModelError("shopping cart item", warning);
                            }

                            return Error(HttpStatusCode.BadRequest);
                        }
                        var data = new ShoppingCartResponseModel()
                        {
                            CartCount = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(x => x.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).ToList().Count
                        };
                        response = new ResponseModel<ShoppingCartResponseModel>()
                        {
                            Message = "Updated to Cart Successfully",
                            Data = data
                        };
                    }
                }
            }
            catch
            {
                response = new ResponseModel<ShoppingCartResponseModel>()
                {
                    Message = "There is some error while adding or updating cart",
                    Status = false,
                    Data = new ShoppingCartResponseModel()
                };
            }
            var json = JsonConvert.SerializeObject(response); //JsonFieldsSerializer.Serialize(shoppingCartsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpPut]
        [Route("/api/shopping_cart_items/{id}")]
        [ProducesResponseType(typeof(ShoppingCartItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult UpdateShoppingCartItem([ModelBinder(typeof(JsonModelBinder<ShoppingCartItemDto>))] Delta<ShoppingCartItemDto> shoppingCartItemDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            // We kno that the id will be valid integer because the validation for this happens in the validator which is executed by the model binder.
            var shoppingCartItemForUpdate = _shoppingCartItemApiService.GetShoppingCartItem(shoppingCartItemDelta.Dto.Id);

            if (shoppingCartItemForUpdate == null)
            {
                return Error(HttpStatusCode.NotFound, "shopping_cart_item", "not found");
            }

            shoppingCartItemDelta.Merge(shoppingCartItemForUpdate);

            if (!shoppingCartItemForUpdate.Product.IsRental)
            {
                shoppingCartItemForUpdate.RentalStartDateUtc = null;
                shoppingCartItemForUpdate.RentalEndDateUtc = null;
            }

            if (shoppingCartItemDelta.Dto.Attributes != null)
            {
                shoppingCartItemForUpdate.AttributesXml = _productAttributeConverter.ConvertToXml(shoppingCartItemDelta.Dto.Attributes, shoppingCartItemForUpdate.Product.Id);
            }

            // The update time is set in the service.
            var warnings = _shoppingCartService.UpdateShoppingCartItem(shoppingCartItemForUpdate.Customer, shoppingCartItemForUpdate.Id,
                shoppingCartItemForUpdate.AttributesXml, shoppingCartItemForUpdate.CustomerEnteredPrice,
                shoppingCartItemForUpdate.RentalStartDateUtc, shoppingCartItemForUpdate.RentalEndDateUtc,
                shoppingCartItemForUpdate.Quantity);

            if (warnings.Count > 0)
            {
                foreach (var warning in warnings)
                {
                    ModelState.AddModelError("shopping cart item", warning);
                }

                return Error(HttpStatusCode.BadRequest);
            }
            else
            {
                shoppingCartItemForUpdate = _shoppingCartItemApiService.GetShoppingCartItem(shoppingCartItemForUpdate.Id);
            }

            // Preparing the result dto of the new product category mapping
            var newShoppingCartItemDto = _dtoHelper.PrepareShoppingCartItemDTO(shoppingCartItemForUpdate);

            var shoppingCartsRootObject = new ShoppingCartItemsRootObject();

            shoppingCartsRootObject.ShoppingCartItems.Add(newShoppingCartItemDto);

            var json = JsonFieldsSerializer.Serialize(shoppingCartsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpDelete]
        [Route("/api/shopping_cart_items/{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult DeleteShoppingCartItem(int id)
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            var shoppingCartItemForDelete = _shoppingCartItemApiService.GetShoppingCartItem(id);

            if (shoppingCartItemForDelete == null)
            {
                return Error(HttpStatusCode.NotFound, "shopping_cart_item", "not found");
            }

            _shoppingCartService.DeleteShoppingCartItem(shoppingCartItemForDelete);

            //activity log
            CustomerActivityService.InsertActivity("DeleteShoppingCartItem", LocalizationService.GetResource("ActivityLog.DeleteShoppingCartItem"), shoppingCartItemForDelete);

            return new RawJsonActionResult("{}");
        }

        [HttpPost]
        [Route("/api/v1/clearCart")]
        public IActionResult ClearShoppingCart()
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Split("Bearer", 2)[1]).EntityId;
            var customer = _customerApiService.GetCustomerEntityById(customerId);

            var cartItems = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(cart => cart.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).Select(item => item.Id).ToList();
            foreach (var id in cartItems)
            {
                var shoppingCartItemForDelete = _shoppingCartItemApiService.GetShoppingCartItem(id);

                if (shoppingCartItemForDelete == null)
                {
                    return Error(HttpStatusCode.NotFound, "shopping_cart_item", "not found");
                }

                _shoppingCartService.DeleteShoppingCartItem(shoppingCartItemForDelete);

                //activity log
                CustomerActivityService.InsertActivity("DeleteShoppingCartItem", LocalizationService.GetResource("ActivityLog.DeleteShoppingCartItem"), shoppingCartItemForDelete);
            }

            var response = new ResponseModel<ShoppingCartResponseModel>()
            {
                Message = "Cart Cleared Successfully",
                Data = new ShoppingCartResponseModel()
            };
            var json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/v1/removeProduct")]
        public IActionResult RemoveProductShoppingCart(ShoppingCartProductModel productModel)
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Split("Bearer", 2)[1]).EntityId;
            var customer = _customerApiService.GetCustomerEntityById(customerId);
            var response = new ResponseModel<ShoppingCartResponseModel>()
            {
                Message = "There is some error for Customer "+ customerId,
                Data = new ShoppingCartResponseModel(),
            };
            var cartItems = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(item => item.ProductId == productModel.Product_Id && item.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).Select(prod => prod.Id).ToList();
            if(cartItems.Count == 0)
            {
                response = new ResponseModel<ShoppingCartResponseModel>()
                {
                    Message = "Product was not found in the cart",
                    Data = new ShoppingCartResponseModel(),
                };
            }
            foreach (var id in cartItems)
            {
                var shoppingCartItemForDelete = _shoppingCartItemApiService.GetShoppingCartItem(id);

                if (shoppingCartItemForDelete == null)
                {
                    return Error(HttpStatusCode.NotFound, "shopping_cart_item", "not found");
                }

                _shoppingCartService.DeleteShoppingCartItem(shoppingCartItemForDelete);

                //activity log
                CustomerActivityService.InsertActivity("DeleteShoppingCartItem", LocalizationService.GetResource("ActivityLog.DeleteShoppingCartItem"), shoppingCartItemForDelete);
            }

            response = new ResponseModel<ShoppingCartResponseModel>()
            {
                Message = "Product has been successfully removed from cart",
                Data = new ShoppingCartResponseModel()
            };
            var json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/v1/addWishlist")]
        public IActionResult AddWishlist(ShoppingCartProductModel model)
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Split("Bearer", 2)[1]).EntityId;
            var customer = _customerApiService.GetCustomerEntityById(customerId);

            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            var wishlistCart = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(item => item.ShoppingCartTypeId == (int)ShoppingCartType.Wishlist && item.ProductId == model.Product_Id).FirstOrDefault();

            var response = new ResponseModel<WishlistCartResponseModel>();

            if (wishlistCart == null)
            {
                var newShoppingCartItem = _factory.Initialize();
                // We know that the product id and customer id will be provided because they are required by the validator.
                // TODO: validate
                var product = _productService.GetProductById(model.Product_Id);

                if (product == null)
                {
                    return Error(HttpStatusCode.NotFound, "product", "not found");
                }                

                if (customer == null)
                {
                    return Error(HttpStatusCode.NotFound, "customer", "not found");
                }

                var shoppingCartType = (ShoppingCartType)Enum.Parse(typeof(ShoppingCartType), ShoppingCartType.Wishlist.ToString());

                if (!product.IsRental)
                {
                    newShoppingCartItem.RentalStartDateUtc = null;
                    newShoppingCartItem.RentalEndDateUtc = null;
                }

                var attributesXml = "";

                var currentStoreId = _storeContext.CurrentStore.Id;

                var warnings = _shoppingCartService.AddToCart(customer, product, shoppingCartType, currentStoreId, attributesXml, 0M,
                                            newShoppingCartItem.RentalStartDateUtc, newShoppingCartItem.RentalEndDateUtc, 1);

                if (warnings.Count > 0)
                {
                    foreach (var warning in warnings)
                    {
                        ModelState.AddModelError("shopping cart item", warning);
                    }

                    return Error(HttpStatusCode.BadRequest);
                }
                var data = new WishlistProductModel()
                {
                    ProductId = model.Product_Id,
                    CustomerId = customer.Id,
                    Id = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(cart => cart.ProductId == model.Product_Id && cart.ShoppingCartTypeId == (int)ShoppingCartType.Wishlist).OrderByDescending(x => x.Id).FirstOrDefault().Id
                };
                response = new ResponseModel<WishlistCartResponseModel>()
                {
                    Message = "Added to Wishlist Successfully",
                    Data = new WishlistCartResponseModel() { Wishlist = data }
                };
            }

            // We kno that the id will be valid integer because the validation for this happens in the validator which is executed by the model binder.
            else if (wishlistCart != null)
            {
                var shoppingCartItemForDelete = _shoppingCartItemApiService.GetShoppingCartItem(wishlistCart.Id);

                if (shoppingCartItemForDelete == null)
                {
                    response = new ResponseModel<WishlistCartResponseModel>()
                    {
                        Message = "No item found in Wishlist",
                        Data = new WishlistCartResponseModel()
                    };
                }
                else
                {
                    _shoppingCartService.DeleteShoppingCartItem(shoppingCartItemForDelete);

                    //activity log
                    CustomerActivityService.InsertActivity("DeleteShoppingCartItem", LocalizationService.GetResource("ActivityLog.DeleteShoppingCartItem"), shoppingCartItemForDelete);

                    response = new ResponseModel<WishlistCartResponseModel>()
                    {
                        Message = "Removed from Wishlist Successfully",
                        Data = new WishlistCartResponseModel()
                    };
                }
            }

            var json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore }); //JsonFieldsSerializer.Serialize(shoppingCartsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpGet]
        [Route("/api/v1/getAllWishlist")]
        public IActionResult GetWishlistCartListByCustomerId()
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Split("Bearer", 2)[1]).EntityId;
            var customer = _customerApiService.GetCustomerEntityById(customerId);

            var response = new ResponseModel<WishlistProductResponseModel>()
            {
                Message = "All data",
                Data = new WishlistProductResponseModel()
                {
                    CartCount = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Any() ? _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(x => x.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).ToList().Count : 0,
                    AllWishlists = new List<WishlistProductModel>()
                }
            };

            IList<ShoppingCartItem> shoppingCartItems = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(cart => cart.ShoppingCartTypeId == (int)ShoppingCartType.Wishlist).ToList();

            if (shoppingCartItems == null || shoppingCartItems.Count == 0)
            {
                response = new ResponseModel<WishlistProductResponseModel>()
                {
                    Message = "No items in Wishlist",
                    Data = new WishlistProductResponseModel()
                    {
                        CartCount = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Any() ? _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(x => x.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).ToList().Count : 0,
                        AllWishlists = new List<WishlistProductModel>()
                    }
                };
            }
            else
            {
                var cartModels = shoppingCartItems.Select(item => new WishlistProductModel()
                {
                    Id = item.Id,
                    CustomerId = customerId,
                    ProductId = item.ProductId,
                    CreatedAt = item.CreatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                    AnimalProduct = new WishlistProductListingModel()
                    {
                        Id = item.ProductId,
                        ProductCategoryId = item.Product.ProductCategories.Any() ? item.Product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId == 0 ? item.Product.ProductCategories.FirstOrDefault().CategoryId : item.Product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId : null,
                        ProductSubCategoryId = item.Product.ProductCategories.Any() ? item.Product.ProductCategories.FirstOrDefault()?.CategoryId : null,
                        Title = item.Product.Name,
                        Quantity = item.Quantity,
                        Price = item.Product.Price,
                        PrimaryImage = _pictureService.GetPicturesByProductId(item.ProductId).Any() ? _pictureService.GetPictureUrl(_pictureService.GetPicturesByProductId(item.ProductId).FirstOrDefault(), 75, true) : null,
                        Description = item.Product.FullDescription,
                        Status = item.Product.Deleted ? 0 : 1,
                        IsFeatured = item.Product.ProductCategories.Any() ? (item.Product.ProductCategories.FirstOrDefault().IsFeaturedProduct ? 1 : 0) : 0,
                        CreatedAt = item.Product.CreatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                        UpdatedAt = item.Product.UpdatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
                        Colors = new List<Common>(),
                        Size = new List<Common>(),
                        Weight = new List<Common>(),
                        AvailableProductQuantity = item.Product.StockQuantity
                    },
                    AvgRating = new List<WishlistProductRatingModel>()
                { new WishlistProductRatingModel()
                    {
                        ProductId = item.ProductId,
                        Aggregate = Convert.ToInt32(item.Product.ProductReviews.Any() ? item.Product.ProductReviews.Average(review => review.Rating) : 0)
                    }}
                }).ToList();

                response = new ResponseModel<WishlistProductResponseModel>()
                {
                    Message = "All data",
                    Data = new WishlistProductResponseModel()
                    {
                        AllWishlists = cartModels,
                        CartCount = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(x => x.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).ToList().Count
                    }
                };
            }
            var json = JsonConvert.SerializeObject(response);//JsonFieldsSerializer.Serialize(shoppingCartsRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/v1/clearWishlist")]
        public IActionResult ClearWishlist()
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Split("Bearer", 2)[1]).EntityId;
            var customer = _customerApiService.GetCustomerEntityById(customerId);

            var response = new ResponseModel<ShoppingCartResponseModel>()
            {
                Message = "Wishlist Cleared Successfully",
                Data = new ShoppingCartResponseModel()
            };

            var cartItems = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Any() ? _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(cart => cart.ShoppingCartTypeId == (int)ShoppingCartType.Wishlist).Select(item => item.Id).ToList() : new List<int>();
            foreach (var id in cartItems)
            {
                var shoppingCartItemForDelete = _shoppingCartItemApiService.GetShoppingCartItem(id);

                if (shoppingCartItemForDelete == null)
                {
                    response = new ResponseModel<ShoppingCartResponseModel>()
                    {
                        Message = "Error in clearing wishlist",
                        Data = new ShoppingCartResponseModel()
                    };
                }
                else
                {
                    _shoppingCartService.DeleteShoppingCartItem(shoppingCartItemForDelete);

                //activity log
                CustomerActivityService.InsertActivity("DeleteShoppingCartItem", LocalizationService.GetResource("ActivityLog.DeleteShoppingCartItem"), shoppingCartItemForDelete);
                }
            }

            response = new ResponseModel<ShoppingCartResponseModel>()
            {
                Message = "Wishlist Cleared Successfully",
                Data = new ShoppingCartResponseModel()
            };
            var json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            return new RawJsonActionResult(json);
        }

        [HttpGet]
        [Route("/api/v1/checkout")]
        public IActionResult Cart()
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Split("Bearer", 2)[1]).EntityId;
            var customer = _customerApiService.GetCustomerEntityById(customerId);

            var cart = _shoppingCartItemApiService.GetShoppingCartItemsByCustomer(customerId).Where(sci => sci.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart).ToList();
            var model = new CheckoutResponseModel();
            model = _shoppingCartItemApiService.PrepareShoppingCartModel(model, cart,customer);
            var response = new ResponseModel<CheckoutResponseModel>()
            {
                Message = "Get Data Successfully",
                Data = model
            };
            var json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            return new RawJsonActionResult(json);
        }
    }
}