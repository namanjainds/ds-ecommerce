﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Nop.Core.Domain.Customers;
using Nop.Core.Infrastructure;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs;
using Nop.Plugin.Api.DTOs.Customers;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.MappingExtensions;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.CustomersParameters;
using Nop.Plugin.Api.Services;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Stores;

namespace Nop.Plugin.Api.Controllers
{
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Mvc;
    using DTOs.Errors;
    using JSON.Serializers;
    using Newtonsoft.Json;
    using Microsoft.Extensions.Primitives;
    using Nop.Core;
    using Nop.Services.Orders;
    using Nop.Services.Events;
    using Nop.Services.Authentication;
    using Nop.Core.Domain.Common;
    using Nop.Core.Http;
	using Nop.Core.Domain.Security;
    using Nop.Core.Domain;
    using Microsoft.AspNetCore.Http;
    using System.IO;
    using Microsoft.AspNetCore.Http.Internal;
    using Nop.Core.Domain.Media;
    using Nop.Plugin.Api.Models;
    using System.Security.Claims;
    using System.IdentityModel.Tokens.Jwt;
    using System.Text;
    using Microsoft.IdentityModel.Tokens;

    public class CustomersController : BaseApiController
    {
        private readonly ICustomerApiService _customerApiService;
        private readonly ICustomerRolesHelper _customerRolesHelper;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IEncryptionService _encryptionService;
        private readonly ICountryService _countryService;
        private readonly IMappingHelper _mappingHelper;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly ILanguageService _languageService;
        private readonly IFactory<Customer> _factory;
        private readonly IWorkContext _workContext;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IEventPublisher _eventPublisher;
        private readonly IAuthenticationService _authenticationService;
        private readonly ILocalizationService _localizationService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IStoreContext _storeContext;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly IDownloadService _downloadService;
        private readonly IWorkflowMessageApiService _workflowMessageApiService;
        private readonly IAddressService _addressService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IPictureService _pictureService;
        private readonly MediaSettings _mediaSettings;
        // We resolve the customer settings this way because of the tests.
        // The auto mocking does not support concreate types as dependencies. It supports only interfaces.
        private CustomerSettings _customerSettings;

        private CustomerSettings CustomerSettings
        {
            get
            {
                if (_customerSettings == null)
                {
                    _customerSettings = EngineContext.Current.Resolve<CustomerSettings>();
                }

                return _customerSettings;
            }
        }

        public CustomersController(
            ICustomerApiService customerApiService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            ICustomerRolesHelper customerRolesHelper,
            IGenericAttributeService genericAttributeService,
            IEncryptionService encryptionService,
            IFactory<Customer> factory,
            ICountryService countryService,
            IMappingHelper mappingHelper,
            INewsLetterSubscriptionService newsLetterSubscriptionService,
            IPictureService pictureService,
            ILanguageService languageService,
            IWorkContext workContext,
            IShoppingCartService shoppingCartService,
            IEventPublisher eventPublisher,
            IAuthenticationService authenticationService,
            IStoreContext storeContext,
            IWorkflowMessageService workflowMessageService,
            StoreInformationSettings storeInformationSettings,
            IWorkflowMessageApiService workflowMessageApiService,
            IDownloadService downloadService,
            IAddressService addressService,
            IStateProvinceService stateProvinceService,
            MediaSettings mediaSettings) :
            base(jsonFieldsSerializer, aclService, customerService, storeMappingService, storeService, discountService, customerActivityService, localizationService, pictureService)
        {
            _customerApiService = customerApiService;
            _factory = factory;
            _countryService = countryService;
            _mappingHelper = mappingHelper;
            _newsLetterSubscriptionService = newsLetterSubscriptionService;
            _languageService = languageService;
            _encryptionService = encryptionService;
            _genericAttributeService = genericAttributeService;
            _customerRolesHelper = customerRolesHelper;
            _workContext = workContext;
            _shoppingCartService = shoppingCartService;
            _eventPublisher = eventPublisher;
            _authenticationService = authenticationService;
            _localizationService = localizationService;
            _customerActivityService = customerActivityService;
            _workflowMessageApiService = workflowMessageApiService;
            _storeContext = storeContext;
            _workflowMessageService = workflowMessageService;
            _storeInformationSettings = storeInformationSettings;
            _downloadService = downloadService;
            _addressService = addressService;
            _stateProvinceService = stateProvinceService;
            _pictureService = pictureService;
            _mediaSettings = mediaSettings;
        }

        /// <summary>
        /// Retrieve all customers of a shop
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/customers")]
        [ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetCustomers(CustomersParametersModel parameters)
        {
            if (parameters.Limit < Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "limit", "Invalid limit parameter");
            }

            if (parameters.Page < Configurations.DefaultPageValue)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid request parameters");
            }

            var allCustomers = _customerApiService.GetCustomersDtos(parameters.CreatedAtMin, parameters.CreatedAtMax, parameters.Limit, parameters.Page, parameters.SinceId);

            var customersRootObject = new CustomersRootObject()
            {
                Customers = allCustomers
            };

            var json = JsonFieldsSerializer.Serialize(customersRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// Retrieve customer by spcified id
        /// </summary>
        /// <param name="id">Id of the customer</param>
        /// <param name="fields">Fields from the customer you want your json to contain</param>
        /// <response code="200">OK</response>
        /// <response code="404">Not Found</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/customers/{id}")]
        [ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetCustomerById(int id, string fields = "")
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            var customer = _customerApiService.GetCustomerById(id);

            if (customer == null)
            {
                return Error(HttpStatusCode.NotFound, "customer", "not found");
            }

            var customersRootObject = new CustomersRootObject();
            customersRootObject.Customers.Add(customer);

            var json = JsonFieldsSerializer.Serialize(customersRootObject, fields);

            return new RawJsonActionResult(json);
        }


        /// <summary>
        /// Get a count of all customers
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/customers/count")]
        [ProducesResponseType(typeof(CustomersCountRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        public IActionResult GetCustomersCount()
        {
            var allCustomersCount = _customerApiService.GetCustomersCount();

            var customersCountRootObject = new CustomersCountRootObject()
            {
                Count = allCustomersCount
            };

            return Ok(customersCountRootObject);
        }

        /// <summary>
        /// Search for customers matching supplied query
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/customers/search")]
        [ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult Search(CustomersSearchParametersModel parameters)
        {
            if (parameters.Limit <= Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "limit", "Invalid limit parameter");
            }

            if (parameters.Page <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid page parameter");
            }

            var customersDto = _customerApiService.Search(parameters.Query, parameters.Order, parameters.Page, parameters.Limit);

            var customersRootObject = new CustomersRootObject()
            {
                Customers = customersDto
            };

            var json = JsonFieldsSerializer.Serialize(customersRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/customers")]
        [ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult CreateCustomer([ModelBinder(typeof(JsonModelBinder<CustomerDto>))] Delta<CustomerDto> customerDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            //If the validation has passed the customerDelta object won't be null for sure so we don't need to check for this.

            // Inserting the new customer
            var newCustomer = _factory.Initialize();
            customerDelta.Merge(newCustomer);

            foreach (var address in customerDelta.Dto.Addresses)
            {
                // we need to explicitly set the date as if it is not specified
                // it will default to 01/01/0001 which is not supported by SQL Server and throws and exception
                if (address.CreatedOnUtc == null)
                {
                    address.CreatedOnUtc = DateTime.UtcNow;
                }
                newCustomer.Addresses.Add(address.ToEntity());
            }

            _customerApiService.InsertCustomer(newCustomer);

            InsertFirstAndLastNameGenericAttributes(customerDelta.Dto.FirstName, customerDelta.Dto.LastName, newCustomer);

            if (!string.IsNullOrEmpty(customerDelta.Dto.LanguageId) && int.TryParse(customerDelta.Dto.LanguageId, out var languageId)
                && _languageService.GetLanguageById(languageId) != null)
            {
                _genericAttributeService.SaveAttribute(newCustomer, NopCustomerDefaults.LanguageIdAttribute, languageId);
            }

            //password
            if (!string.IsNullOrWhiteSpace(customerDelta.Dto.Password))
            {
                AddPassword(customerDelta.Dto.Password, newCustomer);
            }

            // We need to insert the entity first so we can have its id in order to map it to anything.
            // TODO: Localization
            // TODO: move this before inserting the customer.
            if (customerDelta.Dto.RoleIds.Count > 0)
            {
                AddValidRoles(customerDelta, newCustomer);
                _customerApiService.UpdateCustomer(newCustomer);
            }

            // Preparing the result dto of the new customer
            // We do not prepare the shopping cart items because we have a separate endpoint for them.
            var newCustomerDto = newCustomer.ToDto();

            // This is needed because the entity framework won't populate the navigation properties automatically
            // and the country will be left null. So we do it by hand here.
            PopulateAddressCountryNames(newCustomerDto);

            // Set the fist and last name separately because they are not part of the customer entity, but are saved in the generic attributes.
            newCustomerDto.FirstName = customerDelta.Dto.FirstName;
            newCustomerDto.LastName = customerDelta.Dto.LastName;

            newCustomerDto.LanguageId = customerDelta.Dto.LanguageId;

            //activity log
            CustomerActivityService.InsertActivity("AddNewCustomer", LocalizationService.GetResource("ActivityLog.AddNewCustomer"), newCustomer);

            var customersRootObject = new CustomersRootObject();

            customersRootObject.Customers.Add(newCustomerDto);

            var json = JsonFieldsSerializer.Serialize(customersRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpPut]
        [Route("/api/customers/{id}")]
        [ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult UpdateCustomer([ModelBinder(typeof(JsonModelBinder<CustomerDto>))] Delta<CustomerDto> customerDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            // Updateting the customer
            var currentCustomer = _customerApiService.GetCustomerEntityById(customerDelta.Dto.Id);

            if (currentCustomer == null)
            {
                return Error(HttpStatusCode.NotFound, "customer", "not found");
            }

            customerDelta.Merge(currentCustomer);

            if (customerDelta.Dto.RoleIds.Count > 0)
            {
                AddValidRoles(customerDelta, currentCustomer);
            }

            if (customerDelta.Dto.Addresses.Count > 0)
            {
                var currentCustomerAddresses = currentCustomer.Addresses.ToDictionary(address => address.Id, address => address);

                foreach (var passedAddress in customerDelta.Dto.Addresses)
                {
                    var addressEntity = passedAddress.ToEntity();

                    if (currentCustomerAddresses.ContainsKey(passedAddress.Id))
                    {
                        _mappingHelper.Merge(passedAddress, currentCustomerAddresses[passedAddress.Id]);
                    }
                    else
                    {
                        currentCustomer.Addresses.Add(addressEntity);
                    }
                }
            }

            _customerApiService.UpdateCustomer(currentCustomer);

            InsertFirstAndLastNameGenericAttributes(customerDelta.Dto.FirstName, customerDelta.Dto.LastName, currentCustomer);


            if (!string.IsNullOrEmpty(customerDelta.Dto.LanguageId) && int.TryParse(customerDelta.Dto.LanguageId, out var languageId)
                && _languageService.GetLanguageById(languageId) != null)
            {
                _genericAttributeService.SaveAttribute(currentCustomer, NopCustomerDefaults.LanguageIdAttribute, languageId);
            }

            //password
            if (!string.IsNullOrWhiteSpace(customerDelta.Dto.Password))
            {
                AddPassword(customerDelta.Dto.Password, currentCustomer);
            }

            // TODO: Localization

            // Preparing the result dto of the new customer
            // We do not prepare the shopping cart items because we have a separate endpoint for them.
            var updatedCustomer = currentCustomer.ToDto();

            // This is needed because the entity framework won't populate the navigation properties automatically
            // and the country name will be left empty because the mapping depends on the navigation property
            // so we do it by hand here.
            PopulateAddressCountryNames(updatedCustomer);

            // Set the fist and last name separately because they are not part of the customer entity, but are saved in the generic attributes.
            var firstNameGenericAttribute = _genericAttributeService.GetAttributesForEntity(currentCustomer.Id, typeof(Customer).Name)
                .FirstOrDefault(x => x.Key == "FirstName");

            if (firstNameGenericAttribute != null)
            {
                updatedCustomer.FirstName = firstNameGenericAttribute.Value;
            }

            var lastNameGenericAttribute = _genericAttributeService.GetAttributesForEntity(currentCustomer.Id, typeof(Customer).Name)
                .FirstOrDefault(x => x.Key == "LastName");

            if (lastNameGenericAttribute != null)
            {
                updatedCustomer.LastName = lastNameGenericAttribute.Value;
            }

            var languageIdGenericAttribute = _genericAttributeService.GetAttributesForEntity(currentCustomer.Id, typeof(Customer).Name)
                .FirstOrDefault(x => x.Key == "LanguageId");

            if (languageIdGenericAttribute != null)
            {
                updatedCustomer.LanguageId = languageIdGenericAttribute.Value;
            }

            //activity log
            CustomerActivityService.InsertActivity("UpdateCustomer", LocalizationService.GetResource("ActivityLog.UpdateCustomer"), currentCustomer);

            var customersRootObject = new CustomersRootObject();

            customersRootObject.Customers.Add(updatedCustomer);

            var json = JsonFieldsSerializer.Serialize(customersRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpDelete]
        [Route("/api/customers/{id}")]
        [GetRequestsErrorInterceptorActionFilter]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult DeleteCustomer(int id)
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            var customer = _customerApiService.GetCustomerEntityById(id);

            if (customer == null)
            {
                return Error(HttpStatusCode.NotFound, "customer", "not found");
            }

            _customerApiService.DeleteCustomer(customer);

            //remove newsletter subscription (if exists)
            foreach (var store in StoreService.GetAllStores())
            {
                var subscription = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, store.Id);
                if (subscription != null)
                    _newsLetterSubscriptionService.DeleteNewsLetterSubscription(subscription);
            }

            //activity log
            CustomerActivityService.InsertActivity("DeleteCustomer", LocalizationService.GetResource("ActivityLog.DeleteCustomer"), customer);

            return new RawJsonActionResult("{}");
        }

        [HttpPost]
        [Route("/api/v1/auth/login")]
        public IActionResult Login(LoginRequestModel login)
        {
            var json = JsonConvert.SerializeObject(login);
            var response = new ResponseModel<LoginResponseModel>();
            try
            {
                if (ModelState.IsValid && CommonHelper.IsValidEmail(login.Email))
                {

                    var loginResult = _customerApiService.ValidateCustomer(login.Email, login.Password);
                    switch (loginResult)
                    {
                        case CustomerLoginResults.Successful:
                            {
                                var customer = _customerApiService.GetCustomerByEmail(login.Email);

                                //migrate shopping cart
                                _shoppingCartService.MigrateShoppingCart(_workContext.CurrentCustomer, customer, true);

                                //sign in new customer
                                _authenticationService.SignIn(customer, false);

                                //raise event       
                                _eventPublisher.Publish(new CustomerLoggedinEvent(customer));

                                //activity log
                                _customerActivityService.InsertActivity(customer, "PublicStore.Login",
                                    _localizationService.GetResource("ActivityLog.PublicStore.Login"), customer);
                                var user = new LoginModel();

                                var token = _genericAttributeService.GetAttributesByKeyGroupandKey("Customer", NopCustomerDefaults.AuthorizationToken).Where(x => x.EntityId == customer.Id).Any() ? _genericAttributeService.GetAttributesByKeyGroupandKey("Customer", NopCustomerDefaults.AuthorizationToken).Where(x => x.EntityId == customer.Id)?.FirstOrDefault().Value : null;
                                if (string.IsNullOrEmpty(token))
                                {
                                    token = GenerateToken();
                                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AuthorizationToken, token);
                                }
                                response = new ResponseModel<LoginResponseModel>
                                {
                                    Message = _localizationService.GetLocaleStringResourceByName("Account.Login.Successful").ResourceValue,
                                    Data = new LoginResponseModel()
                                    {
                                        Token = token,
                                        User = _customerApiService.PrepareLoginUserModel(user, customer),
                                        IsFollowingSomeone = 0
                                    }
                                };
                                json = JsonConvert.SerializeObject(response);
                                break;
                            }
                        case CustomerLoginResults.NotRegistered:
                        case CustomerLoginResults.NotActive:
                            response = new ResponseModel<LoginResponseModel>
                            {
                                Message = "Please confirm your account before login into system.",
                                Data = new LoginResponseModel(),
                                Status = false,
                            };
                            json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
                            break;
                        case CustomerLoginResults.LockedOut:
                            response = new ResponseModel<LoginResponseModel>
                            {
                                Message = _localizationService.GetLocaleStringResourceByName("Account.Login.WrongCredentials.LockedOut").ResourceValue,
                                Data = new LoginResponseModel(),
                                Status = false,
                            };
                            json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
                            break;
                        case CustomerLoginResults.Deleted:
                        case CustomerLoginResults.CustomerNotExist:
                        case CustomerLoginResults.WrongPassword:
                        default:
                            response = new ResponseModel<LoginResponseModel>
                            {
                                Message = _localizationService.GetLocaleStringResourceByName("Account.Login.WrongCredentials.InvalidCredentials").ResourceValue,
                                Data = new LoginResponseModel(),
                                Status = false,
                            };
                            json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
                            break;
                    }
                }
                else
                {
                    response = new ResponseModel<LoginResponseModel>()
                    {
                        Message = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).Any() ? ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).FirstOrDefault().Select(e => e.ErrorMessage).FirstOrDefault() :
                        _localizationService.GetLocaleStringResourceByName("Account.Login.WrongCredentials.InvalidEmail").ResourceValue,
                        Data = new LoginResponseModel(),
                        Status = false
                    };
                    json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
                }
            }
            catch (Exception ex)
            {
                response = new ResponseModel<LoginResponseModel>()
                {
                    Message = "Some error has occured. Please try again " + ex.InnerException !=null ? ex.InnerException.Message : ex.Message,
                    Data = new LoginResponseModel(),
                    Status = false,
                };
                json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            }

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/v1/auth/register")]
        public IActionResult Register(RegisterModel model)
        {
            var response = new ResponseModel<LoginResponseModel>()
            {
                Message = "You have registered successfully.Please check your email for activation.",
                Data = new LoginResponseModel()
            };
            var json = JsonConvert.SerializeObject(model);
            try
            {
                if (_workContext.CurrentCustomer.IsRegistered())
                {
                    //Already registered customer. 
                    _authenticationService.SignOut();

                    //raise logged out event       
                    _eventPublisher.Publish(new CustomerLoggedOutEvent(_workContext.CurrentCustomer));

                    //Save a new record
                    _workContext.CurrentCustomer = _customerApiService.InsertGuestCustomer();
                }
                var customer = _workContext.CurrentCustomer;
                customer.RegisteredInStoreId = _storeContext.CurrentStore.Id;

                if (ModelState.IsValid)
                {
                    if (model.Username != null)
                    {
                        model.Username = model.Username.Trim();
                    }

                    var isApproved = CustomerSettings.UserRegistrationType == UserRegistrationType.Standard;
                    var registrationRequest = new CustomerRegistrationRequest(customer,
                        model.Email,
                        model.Username,
                        model.Password,
                        CustomerSettings.DefaultPasswordFormat,
                        _storeContext.CurrentStore.Id,
                        isApproved);
                    var registrationResult = _customerApiService.RegisterCustomer(registrationRequest);
                    if (registrationResult.Success)
                    {
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.FirstNameAttribute, model.First_Name);
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PhoneAttribute, model.Telephone_Number);

                        //login customer now
                        if (isApproved)
                            _authenticationService.SignIn(customer, true);

                        //insert default address (if possible)
                        var defaultAddress = new Address
                        {
                            FirstName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FirstNameAttribute),
                            LastName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.LastNameAttribute),
                            Email = customer.Email,
                            Company = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CompanyAttribute),
                            CountryId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.CountryIdAttribute) > 0
                                ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.CountryIdAttribute)
                                : null,
                            StateProvinceId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.StateProvinceIdAttribute) > 0
                                ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.StateProvinceIdAttribute)
                                : null,
                            County = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CountyAttribute),
                            City = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CityAttribute),
                            Address1 = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.StreetAddressAttribute),
                            Address2 = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.StreetAddress2Attribute),
                            ZipPostalCode = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.ZipPostalCodeAttribute),
                            PhoneNumber = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.PhoneAttribute),
                            FaxNumber = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FaxAttribute),
                            CreatedOnUtc = customer.CreatedOnUtc
                        };


                        //raise event       
                        _eventPublisher.Publish(new CustomerRegisteredEvent(customer));

                        //email validation message                    
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AccountActivationTokenAttribute, Guid.NewGuid().ToString());
                        _workflowMessageApiService.SendCustomerEmailValidationMessage(customer, _workContext.WorkingLanguage.Id);
                        var token = GenerateToken();
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AuthorizationToken, token);
                        response = new ResponseModel<LoginResponseModel>()
                        {
                            Message = "You have registered successfully.Please check your email for activation.",
                            Data = new LoginResponseModel() { Token = token }
                        };
                        
                    }
                    else
                    {
                        response = new ResponseModel<LoginResponseModel>()
                        {
                            Message = registrationResult.Errors.FirstOrDefault(),
                            Status = false,
                            Data = new LoginResponseModel()
                        };                        
                    }
                }
                else
                {
                    response = new ResponseModel<LoginResponseModel>()
                    {
                        Message = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).FirstOrDefault().Select(e => e.ErrorMessage).FirstOrDefault(),
                        Status = false,
                        Data = new LoginResponseModel()
                    };
                    
                }
            }
            catch(Exception ex)
            {
                response = new ResponseModel<LoginResponseModel>()
                {
                    Message = "There is some error in registration. " + ex.InnerException != null ? ex.InnerException.Message : ex.Message,
                    Data = new LoginResponseModel(),
                    Status = false
                };
            }
            json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            return new RawJsonActionResult(json);
        }

        [HttpGet]
        [Route("/api/customer/activation")]
        public virtual IActionResult AccountActivation(string token, string email)
        {
            try
            {
                var customer = _customerApiService.GetCustomerByEmail(email);
                if (customer == null)
                    return
                        View(new AccountConfirmation
                        {
                            Result = "Unable to activate your account. User does not exist."
                        });

                var cToken = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.AccountActivationTokenAttribute);
                if (string.IsNullOrEmpty(cToken))
                    return
                        View(new AccountConfirmation
                        {
                            Result = _localizationService.GetResource("Account.AccountActivation.AlreadyActivated")
                        });

                if (!cToken.Equals(token, StringComparison.InvariantCultureIgnoreCase))
                    return
                        View(new AccountConfirmation
                        {
                            Result = "Unable to activate your account. Token is either invalid or expired."
                        });
                //activate user account
                customer.Active = true;
                _customerApiService.UpdateCustomer(customer);
                _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AccountActivationTokenAttribute, "");
                //send welcome message
                _workflowMessageService.SendCustomerWelcomeMessage(customer, _workContext.WorkingLanguage.Id);

                var model = new AccountConfirmation
                {
                    Result = _localizationService.GetResource("Account.AccountActivation.Activated")
                };
                return View(ViewNames.AccountConfirmation, model);
            }
            catch(Exception ex)
            {
                return
                        View(new AccountConfirmation
                        {
                            Result = "Unable to activate your account. There is an error while processing the request. Please try again. " + 
                            ex.InnerException !=null ? ex.InnerException.Message : ex.Message
                        });
            }
            
            //return View("~Plugins/Nop.Plugin.Api/Views/Customers/AccountActivation.cshtml", model);
        }

        [HttpPost]
        [Route("/api/v1/auth/logout")]
        public IActionResult Logout(TokenResult model)
        {

            var customerId = _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, model.Token)?.EntityId;
            var customer = _customerApiService.GetCustomerEntityById(customerId ?? 0);
            if (customer == null)
                customer = _workContext.CurrentCustomer;
            //activity log
            _customerActivityService.InsertActivity(_workContext.CurrentCustomer, "PublicStore.Logout",
                _localizationService.GetResource("ActivityLog.PublicStore.Logout"), customer);

            //standard logout 
            _authenticationService.SignOut();

            //raise logged out event       
            _eventPublisher.Publish(new CustomerLoggedOutEvent(customer));

            //EU Cookie
            if (_storeInformationSettings.DisplayEuCookieLawWarning)
            {
                //the cookie law message should not pop up immediately after logout.
                //otherwise, the user will have to click it again...
                //and thus next visitor will not click it... so violation for that cookie law..
                //the only good solution in this case is to store a temporary variable
                //indicating that the EU cookie popup window should not be displayed on the next page open (after logout redirection to homepage)
                //but it'll be displayed for further page loads
                TempData[$"{NopCookieDefaults.Prefix}{NopCookieDefaults.IgnoreEuCookieLawWarning}"] = true;
            }

            var response = new ResponseModel<LoginResponseModel>()
            {
                Message = "Successfully Logged Out",
                Data = new LoginResponseModel()
            };
            var json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            return new RawJsonActionResult(json);

        }

        [HttpPost]
        [Route("/api/v1/auth/password/email")]
        public IActionResult PasswordRecoverySend(PasswordRecoveryModel model)
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var json = JsonConvert.SerializeObject(model);
            var response = new ResponseModel<PasswordRecoveryModel>();
            try
            {
                if (ModelState.IsValid)
                {
                    var customer = _customerApiService.GetCustomerByEmail(model.Email);
                    if (customer != null && customer.Active && !customer.Deleted)
                    {
                        //save token and current date
                        var passwordRecoveryToken = GenerateRandomNo();
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PasswordRecoveryTokenAttribute,
                            passwordRecoveryToken.ToString());
                        DateTime? generatedDateTime = DateTime.UtcNow;
                        _genericAttributeService.SaveAttribute(customer,
                            NopCustomerDefaults.PasswordRecoveryTokenDateGeneratedAttribute, generatedDateTime);

                        //send email
                        _workflowMessageApiService.SendCustomerPasswordRecoveryMessage(customer,
                            _workContext.WorkingLanguage.Id);

                        response = new ResponseModel<PasswordRecoveryModel>
                        {
                            Message = "We have sent email with reset password otp. Please check your inbox!",
                            Data = new PasswordRecoveryModel(),
                            Status = true,
                        };

                    }
                    else
                    {
                        response = new ResponseModel<PasswordRecoveryModel>
                        {
                            Message = _localizationService.GetLocaleStringResourceByName("Account.PasswordRecovery.EmailNotFound").ResourceValue,
                            Status = false,
                            Data = new PasswordRecoveryModel(),
                        };
                    }
                }
                else
                {
                    response = new ResponseModel<PasswordRecoveryModel>
                    {
                        Message = ModelState.Values.Select(x => x.Errors).Where(y => y.Count > 0).FirstOrDefault().Select(e => e.ErrorMessage).FirstOrDefault(),
                        Status = false,
                        Data = new PasswordRecoveryModel(),
                    };
                }
            }
            catch(Exception ex)
            {
                response = new ResponseModel<PasswordRecoveryModel>
                {
                    Message ="There is some error in sending OTP for resetting password " + ex.InnerException != null ? ex.InnerException.Message : ex.Message,
                    Status = false,
                    Data = new PasswordRecoveryModel()
                };
            }
            json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/v1/auth/password/reset")]
        public IActionResult PasswordRecoveryConfirmPOST(PasswordRecoveryConfirmModel model)
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var json = JsonConvert.SerializeObject(model);
            var response = new ResponseModel<PasswordRecoveryConfirmModel>();
            var customer = _customerApiService.GetCustomerByEmail(model.Email);
            if (customer == null)
                return new RawJsonActionResult(JsonConvert.SerializeObject(new ResponseModel<PasswordRecoveryConfirmModel>()
                {
                    Status = false,
                    Message = "User does not exist. Please relogin and try again.",
                    Data = new PasswordRecoveryConfirmModel()
                }));

            //validate token
            if (!_customerApiService.IsPasswordRecoveryTokenValid(customer, model.OTP))
            {
                response = new ResponseModel<PasswordRecoveryConfirmModel>
                {
                    Message = _localizationService.GetLocaleStringResourceByName("Account.PasswordRecovery.WrongToken").ResourceValue,
                    Status = false,
                    Data = new PasswordRecoveryConfirmModel(),
                };
                json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            }

            //validate token expiration date
            if (_customerApiService.IsPasswordRecoveryLinkExpired(customer))
            {
                response = new ResponseModel<PasswordRecoveryConfirmModel>
                {
                    Message = _localizationService.GetLocaleStringResourceByName("Account.PasswordRecovery.LinkExpired").ResourceValue,
                    Status = false,
                    Data = new PasswordRecoveryConfirmModel(),
                };
                json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            }

            if (ModelState.IsValid)
            {
                var responseResult = _customerApiService.ChangePassword(new ChangePasswordRequest(model.Email,
                    false, CustomerSettings.DefaultPasswordFormat, model.Password));
                if (responseResult.Success)
                {
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PasswordRecoveryTokenAttribute, "");

                    response = new ResponseModel<PasswordRecoveryConfirmModel>
                    {
                        Message = "Password has been resetted successfully",
                        Status = true,
                        Data = new PasswordRecoveryConfirmModel(),
                    };
                }
                else
                {
                    response = new ResponseModel<PasswordRecoveryConfirmModel>
                    {
                        Message = responseResult.Errors.FirstOrDefault(),
                        Status = false,
                        Data = new PasswordRecoveryConfirmModel(),
                    };
                }

                json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            }

            //If we got this far, something failed, redisplay form
            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/v1/user/profile/update")]
        public IActionResult UpdateCustomer(CustomerInfoModel model)
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var json = JsonConvert.SerializeObject(model);
            var response = new ResponseModel<CustomerInfoResponseModel>();
            try
            {
                if (ModelState.IsValid)
                {
                    var currentCustomer = _customerApiService.GetCustomerEntityById(model.User_Id);
                    if (currentCustomer != null)
                    {
                        var newCustomer = _customerApiService.GetCustomerByUsername(model.UserName);
                        //Check if Username exists
                        if (currentCustomer.Username != model.UserName && newCustomer != null)
                        {
                            response = new ResponseModel<CustomerInfoResponseModel>
                            {
                                Message = "Your username already exists. Please choose another username",
                                Status = false,
                                Data = new CustomerInfoResponseModel(),
                            };
                        }
                        else
                        {
                            InsertFirstAndLastNameGenericAttributes(model.First_Name, null, currentCustomer);
                            if (model.Telephone_Number != null)
                                _genericAttributeService.SaveAttribute(currentCustomer, NopCustomerDefaults.PhoneAttribute, model.Telephone_Number);

                            if (model.About_Me != null)
                                _genericAttributeService.SaveAttribute(currentCustomer, NopCustomerDefaults.AboutMeAttribute, model.About_Me);

                            if (model.Background_Image != null)
                                _genericAttributeService.SaveAttribute(currentCustomer, NopCustomerDefaults.BackgroundImageAttribute, model.Background_Image);

                            if (model.Profile_Picture != null)
                                _genericAttributeService.SaveAttribute(currentCustomer, NopCustomerDefaults.ProfilePictureAttribute, model.Profile_Picture.FileName);

                            _customerApiService.UpdateCustomer(currentCustomer);

                            #region Update Profile Picture

                            IFormFile imageFile = model.Profile_Picture;
                            if (model.Profile_Picture !=null)
                            {                                
                                if (imageFile != null && !string.IsNullOrEmpty(imageFile.FileName))
                                {
                                    //Remove old image
                                    RemoveAvtarImage(currentCustomer);

                                    var customerAvatar = PictureService.GetPictureById(_genericAttributeService.GetAttribute<int>(currentCustomer, NopCustomerDefaults.AvatarPictureIdAttribute));

                                    //var avatarMaxSize = _customerSettings.AvatarMaximumSizeBytes;
                                    //if (model.Profile_Picture.Length > avatarMaxSize)
                                    //    throw new NopException(string.Format(_localizationService.GetResource("Account.Avatar.MaximumUploadedFileSize"), avatarMaxSize));

                                    var customerPictureBinary = _downloadService.GetDownloadBits(imageFile);
                                    if (customerAvatar != null)
                                        customerAvatar = PictureService.UpdatePicture(customerAvatar.Id, customerPictureBinary, imageFile.ContentType, null);
                                    else
                                        customerAvatar = PictureService.InsertPicture(customerPictureBinary, imageFile.ContentType, null);


                                    var customerAvatarId = 0;
                                    if (customerAvatar != null)
                                        customerAvatarId = customerAvatar.Id;

                                    _genericAttributeService.SaveAttribute(currentCustomer, NopCustomerDefaults.AvatarPictureIdAttribute, customerAvatarId);
                                }
                            }
                            #endregion
                            var user = new LoginModel();
                            var custInfo = new CustomerInfoResponseModel()
                            {
                                User = _customerApiService.UpdateLoginCustomer(user, currentCustomer)
                            };
                            response = new ResponseModel<CustomerInfoResponseModel>
                            {
                                Message = "Your profile has been successfully updated.",
                                Status = true,
                                Data = custInfo,
                            };
                        }
                    }
                    else
                    {
                        response = new ResponseModel<CustomerInfoResponseModel>
                        {
                            Message = "User is not found. Please check again",
                            Status = false,
                            Data = new CustomerInfoResponseModel(),
                        };
                    }
                }
                else
                {
                    response = new ResponseModel<CustomerInfoResponseModel>
                    {
                        Message = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).FirstOrDefault().Select(e => e.ErrorMessage).FirstOrDefault(),
                        Status = false,
                        Data = new CustomerInfoResponseModel(),
                    };
                }
            }
            catch
            {
                response = new ResponseModel<CustomerInfoResponseModel>
                {
                    Message = "There is some error while updating profile. Please try again",
                    Status = false,
                    Data = new CustomerInfoResponseModel(),
                };
            }
            json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/v1/getUserProfile")]
        public IActionResult GetUserProfile(CustomerProfileRequestModel profile)
        {
            var response = new ResponseModel<CustomerProfileResponseModel>();
            if(ModelState.IsValid)
            {
                var customer = _customerApiService.GetCustomerEntityById(profile.User_Id);
                if (customer == null)
                {
                  return Error(HttpStatusCode.NotFound, "customer", "not found");
                }
                var model = new CustomerProfileModel
                {
                    Name = !string.IsNullOrEmpty(_genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.LastNameAttribute)) ? $"{_genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FirstNameAttribute)} {_genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.LastNameAttribute)}" : $"{_genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FirstNameAttribute)}",
                    TelephoneNumber = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.PhoneAttribute),
                    Id = customer.Id,

                    Email = customer.Email,
                    Username = customer.Username,
                    Status = customer.Active ? 1 : 0,

                    ProfilePicture = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AvatarPictureIdAttribute) != 0 ?
                    _pictureService.GetPictureUrl(
            _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AvatarPictureIdAttribute),
            _mediaSettings.AvatarPictureSize, false) : "",
                    BackgroundImage = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.BackgroundImageAttribute),
                    AboutMe = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.AboutMeAttribute),
                    Confirmed = customer.Active ? 1 : 0,
                    IsBlocked = 0
                };
                response = new ResponseModel<CustomerProfileResponseModel>()
                {
                    Message = "Data Found",
                    Data = new CustomerProfileResponseModel()
                    {
                        User = model,
                        Likeslist = new List<Common>(),
                        Is_liked = 0,
                        Is_Friend = 0,
                        By_Self_User = 0,
                        IsFriendRequestSent = 0,
                        ChatInfo = new List<Common>()
                    }
                };
            }
            var json = JsonConvert.SerializeObject(response);
            return new RawJsonActionResult(json);
        }

        [HttpGet]
        [Route("/api/v1/getAddresses")]
        public IActionResult GetCustomerAddress()
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = headers ? _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Contains("Bearer") ? authorizationtoken.ToString().Split("Bearer", 2)[1].Trim() : authorizationtoken.ToString())?.EntityId : 0;
            var customerDto = _customerApiService.GetCustomerById(customerId??0);
            var customer = _customerApiService.GetCustomerEntityById(customerId??0);

            var response = new ResponseModel<List<CustomerAddressModel>>()
            {
                Message = "Data Fetched",
                Data = new List<CustomerAddressModel>()
            };

            if (customer == null || customerDto == null)
            {
                response = new ResponseModel<List<CustomerAddressModel>>()
                {
                    Message = "Customer not found",
                    Data = new List<CustomerAddressModel>(),
                    Status = false
                };
            }
            else
            {
                var data = customer.Addresses.Select(address => new CustomerAddressModel()
                {
                    Id = address.Id,
                    CustomerId = customer.Id,
                    Name = !string.IsNullOrEmpty(address.LastName) ? $"{address.FirstName} {address.LastName}" : address.FirstName,
                    Email = address.Email,
                    Phone = address.PhoneNumber,
                    Address1 = !string.IsNullOrEmpty(address.Address1) ? address.Address1 : null,
                    Address2 = address.Address2,
                    FlatNumber = _genericAttributeService.GetAttribute<string>(address, NopCustomerDefaults.FlatNumberAttribute),
                    Suburb = address.City,
                    Country = _genericAttributeService.GetAttribute<string>(address,NopCustomerDefaults.CountryNameAttribute),//address.Country.Name,
                    State = string.IsNullOrEmpty(address.StateProvince?.Name) ? address.County : address.StateProvince?.Name,
                    PostCode = address.ZipPostalCode,
                    IsDefault = customer.BillingAddressId != null && customer.BillingAddress.Id == address.Id ? 1 : 0,
                    Status = 1,
                    CreatedAt = address.CreatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss")
                }).ToList();
                //var customersRootObject = new CustomersRootObject();
                //customersRootObject.Customers.Add(customer);
                response = new ResponseModel<List<CustomerAddressModel>>()
                {
                    Message = "Data Fetched",
                    Data = data
                };
            }
            var json = JsonConvert.SerializeObject(response);//JsonFieldsSerializer.Serialize(customersRootObject, fields);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/v1/deleteAddress")]
        public IActionResult DeleteCustomerAddress(CustomerAddressRequestModel customerAddress) 
        {

            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = headers ? _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Split("Bearer", 2)[1])?.EntityId : 0;
            var customerDto = _customerApiService.GetCustomerById(customerId??0);
            var customer = _customerApiService.GetCustomerEntityById(customerId??0);
            var response = new ResponseModel<CustomerAddressModel>();
            
            //find address (ensure that it belongs to the current customer)
            var address = customer.Addresses.FirstOrDefault(a => a.Id == customerAddress.Address_Id);
            if (address != null)
            {
                _customerApiService.RemoveCustomerAddress(customer, address);
                _customerApiService.UpdateCustomer(customer);
                //now delete the address record
                _addressService.DeleteAddress(address);
                response = new ResponseModel<CustomerAddressModel>()
                {
                    Message = "Address deleted successfully",
                    Data = new CustomerAddressModel()
                };

            }
            else
            {
                response = new ResponseModel<CustomerAddressModel>()
                {
                    Message = "Address not found",
                    Data = new CustomerAddressModel()
                };
            }

            var json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/v1/editAddress")]
        public IActionResult EditCustomerAddress(CustomerAddressRequestModel customerAddress)
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Split("Bearer", 2)[1]).EntityId;
            var customerDto = _customerApiService.GetCustomerById(customerId);
            var customer = _customerApiService.GetCustomerEntityById(customerId);

            if (customerDto == null)
            {
                return Error(HttpStatusCode.NotFound, "customer", "not found");
            }

            var data = customer.Addresses.Where(add => add.Id == customerAddress.Address_Id).Select(address => new CustomerAddressModel()
            {
                Id = address.Id,
                CustomerId = customerDto.Id,
                Name = !string.IsNullOrEmpty(address.LastName) ? $"{address.FirstName} {address.LastName}" : address.FirstName,
                Email = address.Email,
                Phone = address.PhoneNumber,
                Address1 = !string.IsNullOrEmpty(address.Address1) ? address.Address1 : null,
                Address2 = address.Address2,
                FlatNumber = _genericAttributeService.GetAttribute<string>(address, NopCustomerDefaults.FlatNumberAttribute),
                Suburb = address.City,
                Country = _genericAttributeService.GetAttribute<string>(address, NopCustomerDefaults.CountryNameAttribute),
                State = string.IsNullOrEmpty(address.StateProvince?.Name) ? address.County : address.StateProvince?.Name,
                PostCode = address.ZipPostalCode,
                IsDefault = customer.BillingAddressId != null && customerDto.BillingAddress.Id == address.Id ? 1 : 0,
                Status = 1,
                CreatedAt = address.CreatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss")
            }).FirstOrDefault();
            //var customersRootObject = new CustomersRootObject();
            //customersRootObject.Customers.Add(customer);
            var response = new ResponseModel<CustomerAddressModel>()
            {
                Message = "Address found.",
                Data = data
            };
            var json = JsonConvert.SerializeObject(response);//JsonFieldsSerializer.Serialize(customersRootObject, fields);

            return new RawJsonActionResult(json);
        }        

        [HttpPost]
        [Route("/api/v1/user/password/change")]
        public IActionResult ChangePassword(ChangePasswordModel model)
        {
            var headers = Request.Headers.TryGetValue("Version-Code", out var authorizationtoken);
            var json = JsonConvert.SerializeObject(model);
            var response = new ResponseModel<ChangePasswordResponseModel>();

            var currentCustomer = _customerApiService.GetCustomerEntityById(model.User_Id);

            if (ModelState.IsValid)
            {
                var changePasswordRequest = new ChangePasswordRequest(currentCustomer.Email,
                    true, CustomerSettings.DefaultPasswordFormat, model.Password, model.Old_Password);
                var changePasswordResult = _customerApiService.ChangePassword(changePasswordRequest);

                if (changePasswordResult.Success)
                {
                    response = new ResponseModel<ChangePasswordResponseModel>
                    {
                        Message = "Password has been changed successfully",
                        Status = true,
                        Data = new ChangePasswordResponseModel(),
                    };
                    json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });

                    return new RawJsonActionResult(json);
                }

                string errorMessage = string.Empty;

                //errors
                foreach (var error in changePasswordResult.Errors)
                    errorMessage += error;

                response = new ResponseModel<ChangePasswordResponseModel>
                {
                    Message = errorMessage,
                    Status = false,
                    Data = new ChangePasswordResponseModel(),
                };
                json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            }
            return new RawJsonActionResult(json);
        }
        
        [HttpPost]
        [Route("/api/v1/addOrUpdateAddress")]
        public IActionResult AddOrUpdateAddress(CustomerAddressEditModel model)
        {
            var headers = Request.Headers.TryGetValue("Authorization", out var authorizationtoken);
            var customerId = _genericAttributeService.GetAttributesByKeyGroupandKeyandValue("Customer", NopCustomerDefaults.AuthorizationToken, authorizationtoken.ToString().Split("Bearer", 2)[1]).EntityId;
            
            var response = new ResponseModel<CustomerAddressModel>();
            var customer = _customerApiService.GetCustomerEntityById(customerId);
            //find address (ensure that it belongs to the current customer)
            var customerDto = _customerApiService.GetCustomerById(customerId);
            var countries =_countryService.GetAllCountries(_workContext.WorkingLanguage.Id);
            var countryId = countries.Where(con => con.Name.ToLower()== model.Country.ToLower()).FirstOrDefault()?.Id;

            if (model.Address_Id == null)
            {
                var address = new Address
                {
                    CreatedOnUtc = DateTime.UtcNow,
                    CountryId = countryId ?? countries.Where(x => x.Name == "Australia").FirstOrDefault().Id,
                    StateProvinceId = _stateProvinceService.GetStateProvinceById(countryId ?? 0)?.Id,
                    FirstName = string.IsNullOrEmpty(model.Name) ? customerDto.FirstName : model.Name,
                    LastName = string.IsNullOrEmpty(model.Name) ? customerDto.LastName : "",
                    Email = customer.Email,
                    County = model.State,
                    City = model.Suburb,
                    Address1 = model.Address_1,
                    Address2 = model.Address_2,
                    ZipPostalCode = model.PostCode,
                    PhoneNumber = model.Contact_Number,
                };

                //customer.Addresses.Add(address);
                customer.CustomerAddressMappings.Add(new CustomerAddressMapping { Address = address });
                
                _customerApiService.UpdateCustomer(customer);
                var newaddress = customer.Addresses.OrderByDescending(x => x.Id).FirstOrDefault();
                customer.BillingAddressId = model.Is_Default == 1 ? newaddress.Id : customer.BillingAddressId;
                customer.ShippingAddressId = model.Is_Default == 1 ? newaddress.Id : customer.ShippingAddressId;

                _genericAttributeService.SaveAttribute(newaddress, NopCustomerDefaults.CountryNameAttribute, model.Country);
                _genericAttributeService.SaveAttribute(newaddress, NopCustomerDefaults.FlatNumberAttribute, model.Flat_Number);
                
                _customerApiService.UpdateCustomer(customer);


                response = new ResponseModel<CustomerAddressModel>()
                {
                    Message = "Address created successfully",
                    Data = new CustomerAddressModel()
                };
            }
            else
            {
                var address = customer.Addresses.FirstOrDefault(a => a.Id == model.Address_Id);
                if (address != null)
                {
                    address.CountryId = countryId ?? countries.Where(x => x.Name == "Australia").FirstOrDefault().Id;
                    address.StateProvinceId = _stateProvinceService.GetStateProvinceById(address.CountryId ?? 0)?.Id;
                    address.FirstName = string.IsNullOrEmpty(model.Name) ? customerDto.FirstName : model.Name;
                    address.LastName = string.IsNullOrEmpty(model.Name) ? customerDto.LastName : "";
                    address.Email = customer.Email;
                    address.County = model.State;
                    address.City = model.Suburb;
                    address.Address1 = model.Address_1;
                    address.Address2 = model.Address_2;
                    address.ZipPostalCode = model.PostCode;
                    address.PhoneNumber = model.Contact_Number;
                    _addressService.UpdateAddress(address);

                    customer.BillingAddressId = model.Is_Default == 1 ? address.Id : customer.BillingAddressId;
                    customer.ShippingAddressId = model.Is_Default == 1 ? address.Id : customer.ShippingAddressId;

                    _genericAttributeService.SaveAttribute(address, NopCustomerDefaults.CountryNameAttribute, model.Country);
                    _genericAttributeService.SaveAttribute(address, NopCustomerDefaults.FlatNumberAttribute, model.Flat_Number);

                    _customerApiService.UpdateCustomer(customer);
                    response = new ResponseModel<CustomerAddressModel>()
                    {
                        Message = "Address updated successfully",
                        Data = new CustomerAddressModel()
                    };
                }
                //address is not found
                else
                {
                    response = new ResponseModel<CustomerAddressModel>()
                    {
                        Message = "Address not found",
                        Data = new CustomerAddressModel(),
                        Status = false
                    };
                }
            }
            var json = JsonConvert.SerializeObject(response, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore});
            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/v1/auth/socialLogin")]
        public IActionResult SocialLogin(CustomerSocialLoginRequestModel customerSocial)
        {
            var response = new ResponseModel<LoginResponseModel>();
            try
            {
                if (ModelState.IsValid && CommonHelper.IsValidEmail(customerSocial.Email))
                {
                
                    var customer = _customerApiService.GetCustomerByEmail(customerSocial.Email);
                    if (customer == null)
                    {
                        var isApproved = true;
                        var registrationRequest = new CustomerRegistrationRequest(customer,
                    customerSocial.Email,
                    customerSocial.Email,
                    "Ds@123456",
                    CustomerSettings.DefaultPasswordFormat,
                    _storeContext.CurrentStore.Id,
                    isApproved);
                        var registrationResult = _customerApiService.RegisterCustomer(registrationRequest);
                        if (registrationResult.Success)
                        {
                            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.FirstNameAttribute, customerSocial.First_Name);
                            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PhoneAttribute, "");

                            //login customer now
                            if (isApproved)
                                _authenticationService.SignIn(customer, true);

                            //insert default address (if possible)
                            var defaultAddress = new Address
                            {
                                FirstName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FirstNameAttribute),
                                LastName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.LastNameAttribute),
                                Email = customer.Email,
                                Company = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CompanyAttribute),
                                CountryId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.CountryIdAttribute) > 0
                                    ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.CountryIdAttribute)
                                    : null,
                                StateProvinceId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.StateProvinceIdAttribute) > 0
                                    ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.StateProvinceIdAttribute)
                                    : null,
                                County = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CountyAttribute),
                                City = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CityAttribute),
                                Address1 = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.StreetAddressAttribute),
                                Address2 = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.StreetAddress2Attribute),
                                ZipPostalCode = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.ZipPostalCodeAttribute),
                                PhoneNumber = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.PhoneAttribute),
                                FaxNumber = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FaxAttribute),
                                CreatedOnUtc = customer.CreatedOnUtc
                            };


                            //raise event       
                            _eventPublisher.Publish(new CustomerRegisteredEvent(customer));
                            var user = new LoginModel();
                            //email validation message                    
                            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AccountActivationTokenAttribute, Guid.NewGuid().ToString());
                            _workflowMessageApiService.SendCustomerEmailValidationMessage(customer, _workContext.WorkingLanguage.Id);
                            var token = GenerateToken();
                            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AuthorizationToken, token);
                            
                            if(customerSocial.Provider != null)
                                _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.ExternalLoginProvider, customerSocial.Provider);
                            
                            if(customerSocial.Id != null)
                                _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.ExternalLoginProviderId, customerSocial.Id);

                            if (customerSocial.Token != null)
                               _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.ExternalLoginProviderToken, customerSocial.Token);

                            response = new ResponseModel<LoginResponseModel>
                            {
                                Message = _localizationService.GetLocaleStringResourceByName("Account.Login.Successful").ResourceValue,
                                Data = new LoginResponseModel()
                                {
                                    Token = token,
                                    User = _customerApiService.PrepareLoginUserModel(user, customer),
                                    IsFollowingSomeone = 0
                                }
                            };
                        }
                    }
                    else
                    {
                        //migrate shopping cart
                        _shoppingCartService.MigrateShoppingCart(_workContext.CurrentCustomer, customer, true);

                        //sign in new customer
                        _authenticationService.SignIn(customer, false);

                        //raise event       
                        _eventPublisher.Publish(new CustomerLoggedinEvent(customer));

                        //activity log
                        _customerActivityService.InsertActivity(customer, "PublicStore.Login",
                            _localizationService.GetResource("ActivityLog.PublicStore.Login"), customer);
                        var user = new LoginModel();

                        var token = _genericAttributeService.GetAttributesByKeyGroupandKey("Customer", NopCustomerDefaults.AuthorizationToken).Where(x => x.EntityId == customer.Id).Any() ? _genericAttributeService.GetAttributesByKeyGroupandKey("Customer", NopCustomerDefaults.AuthorizationToken).Where(x => x.EntityId == customer.Id)?.FirstOrDefault().Value : null;
                        if (string.IsNullOrEmpty(token))
                        {
                            token = GenerateToken();
                            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AuthorizationToken, token);
                        }
                        if(customerSocial.Provider !=null)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.ExternalLoginProvider, customerSocial.Provider);
                        
                        if(customerSocial.Id !=null)
                        _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.ExternalLoginProviderId, customerSocial.Id);

                        if (customerSocial.Token != null)
                            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.ExternalLoginProviderToken, customerSocial.Token);

                        response = new ResponseModel<LoginResponseModel>
                        {
                            Message = _localizationService.GetLocaleStringResourceByName("Account.Login.Successful").ResourceValue,
                            Data = new LoginResponseModel()
                            {
                                Token = token,
                                User = _customerApiService.PrepareLoginUserModel(user, customer),
                                IsFollowingSomeone = 0
                            }
                        };
                    }
                
                }
                else
                {
                    response = new ResponseModel<LoginResponseModel>()
                    {
                        Message = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).FirstOrDefault().Select(e => e.ErrorMessage).FirstOrDefault(),
                        Status = false,
                        Data = new LoginResponseModel()
                    };
                }
            }
            catch (Exception ex)
            {
                response = new ResponseModel<LoginResponseModel>()
                {
                    Message = "There is some error while social Login " + ex.InnerException != null ? ex.InnerException.Message : ex.Message,
                    Status = false,
                    Data = new LoginResponseModel()
                };
            }
            var json = JsonConvert.SerializeObject(response);
            return new RawJsonActionResult(json);
        }

        private void InsertFirstAndLastNameGenericAttributes(string firstName, string lastName, Customer newCustomer)
        {
            // we assume that if the first name is not sent then it will be null and in this case we don't want to update it
            if (firstName != null)
            {
                _genericAttributeService.SaveAttribute(newCustomer, NopCustomerDefaults.FirstNameAttribute, firstName);
            }

            if (lastName != null)
            {
                _genericAttributeService.SaveAttribute(newCustomer, NopCustomerDefaults.LastNameAttribute, lastName);
            }
        }

        private void AddValidRoles(Delta<CustomerDto> customerDelta, Customer currentCustomer)
        {
            var allCustomerRoles = _customerApiService.GetAllCustomerRoles(true);
            foreach (var customerRole in allCustomerRoles)
            {
                if (customerDelta.Dto.RoleIds.Contains(customerRole.Id))
                {
                    //new role
                    if (currentCustomer.CustomerCustomerRoleMappings.Count(mapping => mapping.CustomerRoleId == customerRole.Id) == 0)
                        currentCustomer.CustomerCustomerRoleMappings.Add(new CustomerCustomerRoleMapping { CustomerRole = customerRole });
                }
                else
                {
                    if (currentCustomer.CustomerCustomerRoleMappings.Count(mapping => mapping.CustomerRoleId == customerRole.Id) > 0)
                    {
                        currentCustomer.CustomerCustomerRoleMappings
                            .Remove(currentCustomer.CustomerCustomerRoleMappings.FirstOrDefault(mapping => mapping.CustomerRoleId == customerRole.Id));
                    }
                }
            }
        }

        private void PopulateAddressCountryNames(CustomerDto newCustomerDto)
        {
            foreach (var address in newCustomerDto.Addresses)
            {
                SetCountryName(address);
            }

            if (newCustomerDto.BillingAddress != null)
            {
                SetCountryName(newCustomerDto.BillingAddress);
            }

            if (newCustomerDto.ShippingAddress != null)
            {
                SetCountryName(newCustomerDto.ShippingAddress);
            }
        }

        private void SetCountryName(AddressDto address)
        {
            if (string.IsNullOrEmpty(address.CountryName) && address.CountryId.HasValue)
            {
                var country = _countryService.GetCountryById(address.CountryId.Value);
                address.CountryName = country.Name;
            }
        }

        private void AddPassword(string newPassword, Customer customer)
        {
            // TODO: call this method before inserting the customer.
            var customerPassword = new CustomerPassword
            {
                Customer = customer,
                PasswordFormat = CustomerSettings.DefaultPasswordFormat,
                CreatedOnUtc = DateTime.UtcNow
            };

            switch (CustomerSettings.DefaultPasswordFormat)
            {
                case PasswordFormat.Clear:
                    {
                        customerPassword.Password = newPassword;
                    }
                    break;
                case PasswordFormat.Encrypted:
                    {
                        customerPassword.Password = _encryptionService.EncryptText(newPassword);
                    }
                    break;
                case PasswordFormat.Hashed:
                    {
                        var saltKey = _encryptionService.CreateSaltKey(5);
                        customerPassword.PasswordSalt = saltKey;
                        customerPassword.Password = _encryptionService.CreatePasswordHash(newPassword, saltKey, CustomerSettings.HashedPasswordFormat);
                    }
                    break;
            }

            _customerApiService.InsertCustomerPassword(customerPassword);

            // TODO: remove this.
            _customerApiService.UpdateCustomer(customer);
        }

        public void RemoveAvtarImage(Customer customer)
        {
            var customerAvatar = PictureService.GetPictureById(_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AvatarPictureIdAttribute));
            if (customerAvatar != null)
                PictureService.DeletePicture(customerAvatar);
            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AvatarPictureIdAttribute, 0);
        }

        public int GenerateRandomNo()
        {
            int min = 1000;
            int max = 9999;
            var rdm = new Random();
            return rdm.Next(min, max);
        }

        #region Generate JWT Token    
        private string GenerateToken(Claim[] claim = null)
        {
            try
            {
                var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };
                if (claim != null && claim.Length > 0)
                {
                    claims.AddRange(claim);
                }
                var accessKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("IYamWebApiServices"));
                var credentials = new SigningCredentials(accessKey, SecurityAlgorithms.HmacSha256);

                var accessToken = new JwtSecurityToken("http://apiiyam.projectstatus.co.uk/", "http://apiiyam.projectstatus.co.uk/", claims, expires: DateTime.Now.AddYears(1), signingCredentials: credentials);

                return new JwtSecurityTokenHandler().WriteToken(accessToken);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return string.Empty;


        }
        #endregion
    }
}