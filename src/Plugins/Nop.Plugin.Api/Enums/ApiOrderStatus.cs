﻿namespace Nop.Plugin.Api.Enums
{
    /// <summary>
    /// Represents an order status enumeration
    /// </summary>
    public enum ApiOrderStatus
    {
        /// <summary>
        /// Pending
        /// </summary>
        Pending = 10,

        /// <summary>
        /// Processing
        /// </summary>
        Processing = 20,

        /// <summary>
        /// Complete
        /// </summary>
        Complete = 30,

        /// <summary>
        /// Cancelled
        /// </summary>
        Cancelled = 40,

        /// <summary>
        /// Delivered
        /// </summary>
        Delivered = 50,
    }
}
