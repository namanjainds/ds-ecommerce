﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.Models.CustomersParameters
{
    public class TokenResult
    {
        [JsonProperty("token")]
        public string Token { get; set; }       
    
    }
}
