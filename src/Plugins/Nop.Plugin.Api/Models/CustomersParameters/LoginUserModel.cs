﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.DTOs.ShoppingCarts;
using Nop.Plugin.Api.Validators;

namespace Nop.Plugin.Api.Models.CustomersParameters
{    
    public class LoginModel
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("telephone_number")]
        public string TelephoneNumber { get; set; }

        [JsonProperty("confirmed")]
        public int? Confirmed { get; set; }

        [JsonProperty("status")]
        public int? Status { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }        

        [JsonProperty("profile_picture")]
        public string ProfilePicture { get; set; }

        [JsonProperty("background_image")]
        public string BackgroundImage { get; set; }
        
        [JsonProperty("about_me")]
        public string AboutMe { get; set; }                
        
    }
}
