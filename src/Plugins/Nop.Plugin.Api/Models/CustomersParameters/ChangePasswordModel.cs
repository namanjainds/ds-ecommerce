﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.Models.CustomersParameters
{
    public class ChangePasswordModel
    {
        public string Old_Password { get; set; }
        public string Password { get; set; }
        public string Password_Confirmation { get; set; }
        public int User_Id { get; set; }
    }
    public class ChangePasswordResponseModel
    {
        [JsonProperty("result")]
        public string Result { get; set; }
    }
}
