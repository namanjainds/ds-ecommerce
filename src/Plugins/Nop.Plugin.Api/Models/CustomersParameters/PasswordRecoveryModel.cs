﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.Models.CustomersParameters
{
    public class PasswordRecoveryModel
    {
        [Required(ErrorMessage = "Please enter email address")]
        [DataType(DataType.EmailAddress,ErrorMessage = "Please enter valid email address")]
        [JsonProperty("email")]
        public string Email { get; set; }
    }
    public partial class PasswordRecoveryConfirmModel
    {
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("otp")]
        public string OTP { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }
        [JsonProperty("password_confirmation")]
        public string Password_Confirmation { get; set; }
    }
}
