﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.Models.CustomersParameters
{
    public class CustomerInfoModel
    {
        public int User_Id { get; set; }
        public string UserName { get; set; }
        public string First_Name { get; set; }
        public string Telephone_Number { get; set; }
        public IFormFile Profile_Picture { get; set; }
        public string Background_Image { get; set; }
        public string About_Me { get; set; }
    }
    public class CustomerInfoResponseModel
    {
        [JsonProperty("user")]
        public LoginModel User { get; set; }
    }

    public class CustomerAddressModel
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("user_id")]
        public int? CustomerId { get; set; }
        
        [JsonProperty("name")]
        public string Name { get; set; }
                
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("contact_number")]
        public string Phone { get; set; }
        
        [JsonProperty("address_1")]
        public string Address1 { get; set; }

        [JsonProperty("address_2")]
        public string Address2 { get; set; }

        [JsonProperty("flat_number")]
        public string FlatNumber { get; set; }

        [JsonProperty("suburb")]
        public string Suburb { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }
        
        [JsonProperty("postcode")]
        public string PostCode { get; set; }

        [JsonProperty("is_default")]
        public int? IsDefault { get; set; }
        
        [JsonProperty("status")]
        public int? Status { get; set; }
        
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }
        
        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }
        
        [JsonProperty("deleted_at")]
        public string DeletedAt { get; set; }
    }

    public class CustomerAddressRequestModel 
    {
        public int Address_Id { get; set; }
    }

    public class CustomerAddressEditModel
    {
        public int? Address_Id { get; set; }
        public string Name { get; set; }
        public string Contact_Number { get; set; }
        public string Flat_Number { get; set; }
        public string Address_1 { get; set; }
        public string Address_2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostCode { get; set; }
        public int Is_Default { get; set; }
    }

    public class CustomerProfileRequestModel
    {
        public int User_Id { get; set; }
    }

    public class CustomerProfileResponseModel
    {
        [JsonProperty("user")]
        public CustomerProfileModel User { get; set; }
        [JsonProperty("likesList")]
        public List<Common> Likeslist { get; set; }
        [JsonProperty("is_liked")]
        public int? Is_liked { get; set; }
        [JsonProperty("is_friend")]
        public int? Is_Friend { get; set; }
        [JsonProperty("by_self_user")]
        public int? By_Self_User { get; set; }
        [JsonProperty("isFriendRequestSent")]
        public int? IsFriendRequestSent { get; set; }
        [JsonProperty("chatInfo")]
        public List<Common> ChatInfo { get; set; }
    }

    public class CustomerProfileModel : LoginModel
    {
        [JsonProperty("isBlocked")]
        public int? IsBlocked { get; set; }
    }

    public class CustomerSocialLoginRequestModel
    {
        public string Email { get; set; }
        public string Id { get; set; }
        public string Token { get; set; }
        public string Provider { get; set; }
        public string First_Name { get; set; }
        public string Device_Type { get; set; }
        public string Android_Device_Id { get; set; }
        public string Ios_Device_Id { get; set; }
    }

    public class CustomerSocialLoginResponseModel
    {

    }

    public class Common
    {
        [JsonProperty("id")]
        public int? Id { get; set; }
        
        [JsonProperty("name")]
        public string Name { get; set; }
    }

}
