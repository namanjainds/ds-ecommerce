﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Nop.Plugin.Api.Models.CustomersParameters
{
    [Serializable]
    public class LoginRequestModel
    {
        public string DeviceType { get; set; }
        public string Ios_Device_Id { get; set; }
        public string Android_Device_Id { get; set; }
        
        [Required(ErrorMessage ="Password field is required")]
        public string Password { get; set; }
        
        [Required(ErrorMessage ="Email address is required")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }

}
