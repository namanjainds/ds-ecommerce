﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Api.Models.CustomersParameters
{
    [Serializable]
    public class RegisterModel
    {
        [JsonProperty("username")]
        [Required(ErrorMessage ="Please enter username.")]
        [MinLength(3,ErrorMessage = "Username should be atleast 3 characters long.")]
        public string Username { get; set; }

        [DataType(DataType.EmailAddress,ErrorMessage = "Please enter valid email address.")]        
        [JsonProperty("email")]
        [Required(ErrorMessage = "Please enter email address.")]
        public string Email { get; set; }                        
        
        [DataType(DataType.Password)]
        [MinLength(8, ErrorMessage = "The password must be at least 8 characters.")]
        [NoTrim]
        [JsonProperty("password")]
        [Required(ErrorMessage ="Please enter Password")]
        public string Password { get; set; }

        [JsonProperty("first_name")]
        [Required(ErrorMessage ="Please enter full name")]
        public string First_Name { get; set; }

        [DataType(DataType.PhoneNumber,ErrorMessage = "Please enter valid telephone number.")]
        [JsonProperty("telephone_number")]
        [MinLength(7,ErrorMessage = "The telephone number must be at least 7 characters.")]
        [Required(ErrorMessage ="Please enter telephone number.")]
        public string Telephone_Number { get; set; }

        [DataType(DataType.Password)]
        [NoTrim]
        [Compare("Password",ErrorMessage = "The password confirmation and password must match.")]
        [Required(ErrorMessage="Please enter Password confirmation field.")]
        [JsonProperty("password_confirmation")]
        public string Password_Confirmation { get; set; }

    }
}