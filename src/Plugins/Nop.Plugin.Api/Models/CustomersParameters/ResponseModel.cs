﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.Models.CustomersParameters
{
    public class ResponseModel<T>
    {
        public ResponseModel()
        {
            Status = true;
            Code = HttpStatusCode.OK;
        }

        [JsonProperty("message")]
        public string Message { get; set; }
        
        [JsonProperty("data")]
        public T Data { get; set; }

        [JsonProperty("status")]
        public bool Status { get; set; }

        [JsonProperty("code")]
        public HttpStatusCode Code { get; set; }        
    }
}
