﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.ProductsParameters
{
    using System;
    using Microsoft.AspNetCore.Mvc;

    public class CategoryListingModel : SubCategoryListingModel
    {
      public List<SubCategoryListingModel> SubCategory { get; set; }
    }

    public class SubCategoryListingModel
    {                
        [JsonProperty("id")]
        public int Id { get; set; }
        
        [JsonProperty("category")]
        public string Category { get; set; }

        [JsonProperty("parent_id")]
        public int? ParentId { get; set; }               
        
        [JsonProperty("status")]
        public int Status { get; set; }
        
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }
        
        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }
        
        [JsonProperty("deleted_at")]
        public string DeletedAt { get; set; }        
    }
}