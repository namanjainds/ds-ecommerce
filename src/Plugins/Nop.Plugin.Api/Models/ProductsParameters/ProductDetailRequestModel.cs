﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.ProductsParameters
{
    using Microsoft.AspNetCore.Mvc;

    public class ProductDetailRequestModel
    {        

        public int Product_Id { get; set; }                
        public string Detail_Type { get; set; }
    }
}