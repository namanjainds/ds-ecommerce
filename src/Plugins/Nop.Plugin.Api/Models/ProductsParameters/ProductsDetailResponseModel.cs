﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Nop.Plugin.Api.Models.CustomersParameters;

namespace Nop.Plugin.Api.Models.ProductsParameters
{
    public class ProductsDetailResponseModel
    {        

        [JsonProperty("productDetail")]
        public ProductDetailModel ProductDetail { get; set; }
        
        [JsonProperty("similarProducts")]
        public IList<SimilarProductDetailModel> SimilarProducts { get; set; }

        [JsonProperty("cartCount")]
        public int? CartCount { get; set; }

        [JsonProperty("allreviews")]
        public IList<ProductReviewModel> AllReviews { get; set; }

        [JsonProperty("avg_rating")]
        public int? AvgRating { get; set; }

        [JsonProperty("wishlist_added")]
        public int? WishlistAdded { get; set; }

        [JsonProperty("colors")]
        public List<Common> Colors { get; set; }

        [JsonProperty("size")]
        public List<Common> Size { get; set; }
        [JsonProperty("weight")]
        public List<Common> Weight { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "productDetail";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof (ProductListingModel);
        }
    }

    public class ProductReviewRequestModel
    {
        public int Product_Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal Star { get; set; }
    }

    public class ProductReviewResponseModel
    {
        [JsonProperty("result")]
        public string Result { get; set; }
    }
}