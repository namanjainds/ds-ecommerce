﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.Models.ProductsParameters
{
    public class ProductsListingResponseModel
    {        
        [JsonProperty("productsListing")]
        public IList<ProductListingModel> Products { get; set; }

        [JsonProperty("categories")]
        public IList<CategoryListingModel> Categories { get; set; }
        
        [JsonProperty("subCategories")]
        public IList<CategoryListingModel> SubCategories { get; set; }

        [JsonProperty("totalRecords")]
        public int? TotalRecords { get; set; }

        [JsonProperty("record_per_page")]
        public int? RecordsPerPage { get; set; }

        [JsonProperty("maxPrice")]
        public decimal? MaxPrice { get; set; }

        [JsonProperty("minPrice")]
        public decimal? MinPrice { get; set; }

        [JsonProperty("cartcount")]
        public int CartCount { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "products";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof (ProductListingModel);
        }
    }
}