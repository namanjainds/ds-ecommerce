﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.ProductsParameters
{
    using Microsoft.AspNetCore.Mvc;

    public class ProductListingModel
    {                

        [JsonProperty("id")]
        public int Id { get; set; }
        
        [JsonProperty("product_category_id")]
        public int? ProductCategoryId { get; set; }

        [JsonProperty("product_subcategory_id")]
        public int? ProductSubCategoryId { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("category_name")]
        public string CategoryName { get; set; }

        [JsonProperty("sub_category_name")]
        public string SubCategoryName { get; set; }
        
        [JsonProperty("quantity")]
        public int? Quantity { get; set; }

        [JsonProperty("avg_rating")]
        public int? AvgRating { get; set; }

        [JsonProperty("price")]
        public decimal? Price { get; set; }
        
        [JsonProperty("wishlist_added")]
        public int? WishlistAdded { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
        
        [JsonProperty("primary_image")]
        public string PrimaryImage { get; set; }        
    }
}