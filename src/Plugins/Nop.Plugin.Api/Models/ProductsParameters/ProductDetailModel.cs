﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.ProductsParameters
{
    using Microsoft.AspNetCore.Mvc;
    using Nop.Plugin.Api.Models.CustomersParameters;

    public class ProductDetailModel
    {                

        [JsonProperty("id")]
        public int? Id { get; set; }
        
        [JsonProperty("product_category_id")]
        public int? ProductCategoryId { get; set; }

        [JsonProperty("product_category")]
        public string ProductCategory { get; set; }

        [JsonProperty("product_subcategory_id")]
        public int? ProductSubCategoryId { get; set; }

        [JsonProperty("product_subcategory_name")]
        public string ProductSubCategory { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }
        
        [JsonProperty("available_product_quantity")]
        public int? StockQuantity { get; set; }
        
        [JsonProperty("added_quantity")]
        public int? Quantity { get; set; }
        
        [JsonProperty("price")]
        public decimal? Price { get; set; }

        [JsonProperty("product_attrubte")]
        public List<ProductAttributeModel> ProductAttribute { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
        
        [JsonProperty("primary_image")]
        public string PrimaryImage { get; set; }
        
        [JsonProperty("animal_product_images")]
        public List<ProductImages> ProductImages { get; set; }

    }

    public class ProductAttributeModel
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("product_id")]
        public int? ProductId { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("product_attribute_id")]
        public int? ProductAttributeId { get; set; }

        [JsonProperty("attribute_type_2_value")]
        public string AttributeTypeValue { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        [JsonProperty("deleted_at")]
        public string DeletedAt { get; set; }
    }

    public class SimilarProductDetailModel
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("product_category_id")]
        public int? ProductCategoryId { get; set; }

        [JsonProperty("product_category")]
        public string ProductCategory { get; set; }

        [JsonProperty("product_subcategory_id")]
        public int? ProductSubCategoryId { get; set; }

        [JsonProperty("product_subcategory_name")]
        public string ProductSubCategory { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }
        
        [JsonProperty("quantity")]
        public int? Quantity { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("primary_image")]
        public string PrimaryImage { get; set; }

        [JsonProperty("avg_rating")]
        public int? AvgRating { get; set; }

        [JsonProperty("wishlist_added")]
        public int? WishlistAdded { get; set; }
}

    public class ProductImages 
    {
        [JsonProperty("image")]
        public string Image { get; set; }
    }

    public class ProductReviewModel 
    {
        [JsonProperty("id")]
        public int? Id { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("star")]
        public int? Star { get; set; }
        
        [JsonProperty("user_id")]
        public int? CustomerId { get; set; }
        
        [JsonProperty("product_id")]
        public int? ProductId { get; set; }
        
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }
        
        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }
    }
}

