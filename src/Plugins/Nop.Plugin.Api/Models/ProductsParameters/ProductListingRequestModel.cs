﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.ProductsParameters
{
    using Microsoft.AspNetCore.Mvc;

    public class ProductListingRequestModel
    {
        public ProductListingRequestModel()
        {
            Page = Configurations.DefaultPageValue;
            Sort = Configurations.DefaultSort;
        }
        
        /// <summary>
        /// Page to show (default: 1)
        /// </summary>
        [JsonProperty("page")]
        public int Page { get; set; }                
        public string Listing_Type { get; set; }
        public string Title { get; set; }
        public int? MinPrice { get; set; }
        public int? MaxPrice { get; set; }
        public int? Category { get; set; }
        public int? SubCategory { get; set; }
        public string Sort { get; set; }
        public int? Is_Featured { get; set; }
        public int? Is_BestSeller { get; set; }
    }
}