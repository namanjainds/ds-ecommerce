﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.OrdersParameters
{
    using Microsoft.AspNetCore.Mvc;
    using Nop.Core.Domain.Common;
    using Nop.Core.Domain.Customers;
    using Nop.Core.Domain.Localization;
    using Nop.Core.Domain.Orders;
    using Nop.Core.Domain.Shipping;
    using Nop.Core.Domain.Tax;
    using Nop.Plugin.Api.Models.CustomersParameters;
    using Nop.Services.Discounts;
    using Nop.Services.Orders;

    // JsonProperty is used only for swagger
    [ModelBinder(typeof(ParametersModelBinder<OrdersParametersModel>))]
    public class OrdersParametersModel : BaseOrdersParametersModel
    {
        public OrdersParametersModel()
        {
            Ids = null;
            Limit = Configurations.DefaultLimit;
            Page = Configurations.DefaultPageValue;
            SinceId = Configurations.DefaultSinceId;
            Fields = string.Empty;
        }

        /// <summary>
        /// A comma-separated list of order ids
        /// </summary>
        [JsonProperty("ids")]
        public List<int> Ids { get; set; }

        /// <summary>
        /// Amount of results (default: 50) (maximum: 250)
        /// </summary>
        [JsonProperty("limit")]
        public int Limit { get; set; }

        /// <summary>
        /// Page to show (default: 1)
        /// </summary>
        [JsonProperty("page")]
        public int Page { get; set; }

        /// <summary>
        /// Restrict results to after the specified ID
        /// </summary>
        [JsonProperty("since_id")]
        public int SinceId { get; set; }

        /// <summary>
        /// comma-separated list of fields to include in the response
        /// </summary>
        [JsonProperty("fields")]
        public string Fields { get; set; }
    }

    public class OrderHistoryRequestModel
    {
        public int Page { get; set; }
    }

    public class OrderDetailRequestModel
    {
        public int Order_Id { get; set; }
    }

    public class PlaceOrderRequestModel
    {
        public int? Billing_Address_Id { get; set; }
        
        public int? Shipping_Address_Id { get; set; }
        
        public string Order_Comment { get; set; }
        
        public string StripeToken { get; set; }

        public int? Voucher_code_applied { get; set; }

        public string Voucher_code_value { get; set; }
    }

    public class PlaceOrderResponseModel
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("order_id")]
        public string Order_Id { get; set; }
    }

    public class OrderHistoryResponseModel
    {        
        [JsonProperty("orderHistoryData")]
        public List<OrderHistoryModel> OrderHistoryData { get; set; }
        
        [JsonProperty("totalRecords")]
        public int TotalRecords { get; set; }
        
        [JsonProperty("record_per_page")]
        public int RecordPerPage { get; set; }
    }

    public class OrderHistoryModel
    {
        [JsonProperty("products")]
        public List<OrderHistoryProductModel> Products { get; set; }

        [JsonProperty("id")]
        public int? Id { get; set; }
        
        [JsonProperty("order_id")]
        public string OrderId { get; set; }
        
        [JsonProperty("total_amount")]
        public decimal TotalAmount { get; set; }

        [JsonProperty("order_status")]
        public string OrderStatus { get; set; }
        
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }
    }

    public class OrderHistoryProductModel
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("order_id")]
        public int? OrderId { get; set; }

        [JsonProperty("product_id")]
        public int? ProductId { get; set; }

        [JsonProperty("product_name")]
        public string ProductName { get; set; }

        [JsonProperty("product_category_name")]
        public string ProductCategoryName { get; set; }

        [JsonProperty("product_subcategory_name")]
        public string ProductSubCategoryName { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("unit_price")]
        public decimal? UnitPrice { get; set; }

        [JsonProperty("quantity")]
        public int? Quantity { get; set; }

        [JsonProperty("total_price")]
        public decimal? TotalPrice { get; set; }

        [JsonProperty("order_status")]
        public string OrderStatus { get; set; }

        [JsonProperty("color_id")]
        public int? ColorId { get; set; }

        [JsonProperty("size_id")]
        public int? SizeId { get; set; }

        [JsonProperty("weight_id")]
        public int? WeightId { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }
    }

    public class OrderDetailResponseModel
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("order_id")]
        public string OrderId { get; set; }

        [JsonProperty("transaction")]
        public List<Transaction> Transaction { get; set; }

        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("total_amount")]
        public decimal TotalAmount { get; set; }

        [JsonProperty("discount")]
        public decimal Discount { get; set; }

        [JsonProperty("discount_type")]
        public string DiscountType { get; set; }

        [JsonProperty("voucher_code")]
        public string VoucherCode { get; set; }

        [JsonProperty("order_status")]
        public string OrderStatus { get; set; }

        [JsonProperty("cancel_reason")]
        public string CancelReason { get; set; }

        [JsonProperty("order_paid")]
        public int OrderPaid { get; set; }

        [JsonProperty("payment_status")]
        public int PaymentStatus { get; set; }

        [JsonProperty("payment_failure_reason")]
        public string PaymentFailureReason { get; set; }

        [JsonProperty("order_comment")] 
        public string OrderComment { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("billing_addr_contact_name")]
        public string BillingAddressName { get; set; }

        [JsonProperty("billing_addr_contact_number")]
        public string BillingAddressNumber { get; set; }

        [JsonProperty ("billing_flat_number")]
        public string BillingFlatNumber { get; set; }

        [JsonProperty("billing_address_1")]
        public string BillingAddress1 { get; set; }

        [JsonProperty("billing_address_2")]
        public string BillingAddress2 { get; set; }

        [JsonProperty("billing_addr_suburb")]
        public string BillingAddressSuburb { get; set; }

        [JsonProperty("billing_addr_state")]
        public string BillingAddressState { get; set; }

        [JsonProperty("billing_addr_country")]
        public string BillingAddressCountry { get; set; }

        [JsonProperty("billing_addr_postcode")]
        public string BillingAddressPostCode { get; set; }

        [JsonProperty("shipping_addr_contact_name")]
        public string ShippingAddressName { get; set; }

        [JsonProperty("shipping_addr_contact_number")]
        public string ShippingAddressNumber { get; set; }

        [JsonProperty("shipping_addr_flat_number")]
        public string ShippingAddressFlatNumber { get; set; }

        [JsonProperty("shipping_address_1")]
        public string ShippingAddress1 { get; set; }

        [JsonProperty("shipping_address_2")]
        public string ShippingAddress2 { get; set; }

        [JsonProperty("shipping_suburb")]
        public string ShippingAddressSuburb { get; set; }

        [JsonProperty("shipping_state")]
        public string ShippingAddressState { get; set; }

        [JsonProperty("shipping_country")]
        public string ShippingAddressCountry { get; set; }

        [JsonProperty("shipping_postcode")]
        public string ShippingAddressPostCode { get; set; }

        [JsonProperty("products")]
        public List<OrderDetailProductModel> Products  { get; set; }
    }

    public class OrderDetailProductModel : OrderHistoryProductModel
    {
        [JsonProperty("colors")]
        public Common Colors { get; set; }
        
        [JsonProperty("size")]
        public Common Size { get; set; }
        
        [JsonProperty("weight")]
        public Common Weight { get; set; }

        [JsonProperty("animalproduct")]
        public OrderDetailProductDetailModel AnimalProduct { get; set; }

        [JsonProperty("order_variation")]
        public List<OrderDetailProductDetailModel> OrderVariation { get; set; }
    }

    public class Transaction
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("transaction_id")]
        public string TransactionId { get; set; }

        [JsonProperty("refund_id")]
        public string RefundId { get; set; }

        [JsonProperty("refund_response")]
        public string RefundResponse { get; set; }

        [JsonProperty("user_id")]
        public int? UserId { get; set; }

        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("type")]
        public int? Type { get; set; }

        [JsonProperty("payment_token")]
        public string PaymentToken { get; set; }

        [JsonProperty("payment_method")]
        public string PaymentMethod { get; set; }

        [JsonProperty("payment_response")]
        public string PaymentReponse { get; set; }

        [JsonProperty("payment_status")]
        public string PaymentStatus { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        [JsonProperty("pivot")]
        public Pivot Pivot { get; set; }
    }

    public class OrderDetailProductDetailModel 
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("product_category_id")]
        public int? ProductCategoryId { get; set; }

        [JsonProperty("product_subcategory_id")]
        public int? ProductSubCategoryId { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("quantity")]
        public int? Quantity { get; set; }

        [JsonProperty("price")]
        public decimal? Price { get; set; }

        [JsonProperty("primary_image")]
        public string PrimaryImage { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("is_featured")]
        public int? IsFeatured { get; set; }

        [JsonProperty("status")]
        public int? Status { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        [JsonProperty("deleted_at")]
        public string DeletedAt { get; set; }

    }

    public class Pivot
    {
        [JsonProperty("order_id")]
        public int? OrderId { get; set; }
        
        [JsonProperty("transaction_id")]
        public int? TransactionId { get; set; }
    }

    public class PaymentResponse
    {

    }

    /// <summary>
    /// PlaceOrder container
    /// </summary>
    public class PlaceOrderContainer
    {
        public PlaceOrderContainer()
        {
            Cart = new List<ShoppingCartItem>();
            AppliedDiscounts = new List<DiscountForCaching>();
            AppliedGiftCards = new List<AppliedGiftCard>();
        }

        /// <summary>
        /// Customer
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// Customer language
        /// </summary>
        public Language CustomerLanguage { get; set; }

        /// <summary>
        /// Affiliate identifier
        /// </summary>
        public int AffiliateId { get; set; }

        /// <summary>
        /// TAx display type
        /// </summary>
        public TaxDisplayType CustomerTaxDisplayType { get; set; }

        /// <summary>
        /// Selected currency
        /// </summary>
        public string CustomerCurrencyCode { get; set; }

        /// <summary>
        /// Customer currency rate
        /// </summary>
        public decimal CustomerCurrencyRate { get; set; }

        /// <summary>
        /// Billing address
        /// </summary>
        public Address BillingAddress { get; set; }

        /// <summary>
        /// Shipping address
        /// </summary>
        public Address ShippingAddress { get; set; }

        /// <summary>
        /// Shipping status
        /// </summary>
        public ShippingStatus ShippingStatus { get; set; }

        /// <summary>
        /// Selected shipping method
        /// </summary>
        public string ShippingMethodName { get; set; }

        /// <summary>
        /// Shipping rate computation method system name
        /// </summary>
        public string ShippingRateComputationMethodSystemName { get; set; }

        /// <summary>
        /// Is pickup in store selected?
        /// </summary>
        public bool PickupInStore { get; set; }

        /// <summary>
        /// Selected pickup address
        /// </summary>
        public Address PickupAddress { get; set; }

        /// <summary>
        /// Is recurring shopping cart
        /// </summary>
        public bool IsRecurringShoppingCart { get; set; }

        /// <summary>
        /// Initial order (used with recurring payments)
        /// </summary>
        public Order InitialOrder { get; set; }

        /// <summary>
        /// Checkout attributes
        /// </summary>
        public string CheckoutAttributeDescription { get; set; }

        /// <summary>
        /// Shopping cart
        /// </summary>
        public string CheckoutAttributesXml { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IList<ShoppingCartItem> Cart { get; set; }

        /// <summary>
        /// Applied discounts
        /// </summary>
        public List<DiscountForCaching> AppliedDiscounts { get; set; }

        /// <summary>
        /// Applied gift cards
        /// </summary>
        public List<AppliedGiftCard> AppliedGiftCards { get; set; }

        /// <summary>
        /// Order subtotal (incl tax)
        /// </summary>
        public decimal OrderSubTotalInclTax { get; set; }

        /// <summary>
        /// Order subtotal (excl tax)
        /// </summary>
        public decimal OrderSubTotalExclTax { get; set; }

        /// <summary>
        /// Subtotal discount (incl tax)
        /// </summary>
        public decimal OrderSubTotalDiscountInclTax { get; set; }

        /// <summary>
        /// Subtotal discount (excl tax)
        /// </summary>
        public decimal OrderSubTotalDiscountExclTax { get; set; }

        /// <summary>
        /// Shipping (incl tax)
        /// </summary>
        public decimal OrderShippingTotalInclTax { get; set; }

        /// <summary>
        /// Shipping (excl tax)
        /// </summary>
        public decimal OrderShippingTotalExclTax { get; set; }

        /// <summary>
        /// Payment additional fee (incl tax)
        /// </summary>
        public decimal PaymentAdditionalFeeInclTax { get; set; }

        /// <summary>
        /// Payment additional fee (excl tax)
        /// </summary>
        public decimal PaymentAdditionalFeeExclTax { get; set; }

        /// <summary>
        /// Tax
        /// </summary>
        public decimal OrderTaxTotal { get; set; }

        /// <summary>
        /// VAT number
        /// </summary>
        public string VatNumber { get; set; }

        /// <summary>
        /// Tax rates
        /// </summary>
        public string TaxRates { get; set; }

        /// <summary>
        /// Order total discount amount
        /// </summary>
        public decimal OrderDiscountAmount { get; set; }

        /// <summary>
        /// Redeemed reward points
        /// </summary>
        public int RedeemedRewardPoints { get; set; }

        /// <summary>
        /// Redeemed reward points amount
        /// </summary>
        public decimal RedeemedRewardPointsAmount { get; set; }

        /// <summary>
        /// Order total
        /// </summary>
        public decimal OrderTotal { get; set; }
    }

}