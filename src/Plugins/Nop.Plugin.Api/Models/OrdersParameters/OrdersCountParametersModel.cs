﻿using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.OrdersParameters
{
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json;

    [ModelBinder(typeof(ParametersModelBinder<OrdersCountParametersModel>))]
    public class OrdersCountParametersModel : BaseOrdersParametersModel
    {
        // Nothing special here, created just for clarity.
    }

    public class StoreDataResponseModel
    {
        [JsonProperty("categories")]
        public List<StoreCategoryListingModel> Categories { get; set; }

        [JsonProperty("best_seller_product")]
        public List<StoreProductListingModel> BestSellerProduct { get; set; }

        [JsonProperty("featured_product")]
        public List<StoreProductListingModel> FeaturedProduct { get; set; }

        [JsonProperty("slider")]
        public List<StoreImageSlider> Slider { get; set; }

        [JsonProperty("cartcount")]
        public string CartCount { get; set; }

    }

    public class StoreSubCategoryListingModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("category")]
        public string Category { get; set; }
        [JsonProperty("parent_id")]
        public int ParentId { get; set; }

        [JsonProperty("primary_image")]
        public string PrimaryImage { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }
        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }
        [JsonProperty("deleted_at")]
        public string DeletedAt { get; set; }
    }

    public class StoreCategoryListingModel : StoreSubCategoryListingModel
    {
        [JsonProperty("subcategory")]
        public List<StoreSubCategoryListingModel> SubCategory { get; set; }
    }

    public class StoreProductListingModel
    {
        [JsonProperty("id")]
        public int? Id { get; set; }
        [JsonProperty("product_category_id")]
        public int? ProductCategoryId { get; set; }
        [JsonProperty("product_subcategory_id")]
        public int? ProductSubCategoryId { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("category_name")]
        public string CategoryName { get; set; }
        [JsonProperty("sub_category_name")]
        public string SubCategoryName { get; set; }
        [JsonProperty("quantity")]
        public int Quantity { get; set; }
        [JsonProperty("avg_rating")]
        public int? AvgRating { get; set; }
        [JsonProperty("price")]
        public decimal Price { get; set; }
        [JsonProperty("wishlist_added")]
        public int WishlistAdded { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("primary_image")]
        public string PrimaryImage { get; set; }
    }

    public class StoreImageSlider
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("image")]
        public string Image { get; set; }
    }

    public class VoucherRequestModel
    {
        [JsonProperty("total_amount")]
        public decimal TotalAmount { get; set; }

        [JsonProperty("voucher_code")]
        public string Voucher_Code { get; set; }
    }

    public class VoucherResponseModel
    {
        [JsonProperty("voucher_code")]
        public List<VoucherCouponModel> VoucherCode { get; set; }
    }

    public class VoucherCouponModel
    {
        [JsonProperty("id")]
        public int? Id { get; set; }
        
        [JsonProperty("title")]
        public string Title { get; set; }
        
        [JsonProperty("voucher_code")]
        public string VoucherCode { get; set; }
        
        [JsonProperty("type")]
        public string Type { get; set; }
        
        [JsonProperty("value")]
        public int Value { get; set; }
        
        [JsonProperty("min_order")]
        public int? MinOrder { get; set; }
        
        [JsonProperty("from_date")]
        public string FromDate { get; set; }

        [JsonProperty("to_date")]
        public string ToDate { get; set; }
        
        [JsonProperty("max_users")]
        public int? MaxUsers { get; set; }
    }

    public class ContactUsRequestModel
    {        
        public string Name { get; set; }
        
        public string Email { get; set; }
        
        public string Title { get; set; }

        public string Message { get; set; }

        public string Contact_Number { get; set; }
    }

    public class PrivacyResponseModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("status_label")]
        public string StatusLabel { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("created_by")]
        public string CreatedBy { get; set; }
    }
}