﻿using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.ShoppingCartsParameters
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Newtonsoft.Json;
    using Nop.Core.Domain.Catalog;
    using Nop.Plugin.Api.Models.CustomersParameters;
    using Nop.Plugin.Api.Models.ProductsParameters;

    public class ShoppingCartRequestModel
    {
        [JsonProperty("product_id")]
        public string Product_Id { get; set; }
        [JsonProperty("quantity")]
        public string Quantity { get; set; }
        
        [JsonProperty("type")]
        public string Type { get; set; }
        
        [JsonProperty("size_id")]
        public string Size_Id { get; set; }
        
        [JsonProperty("color_id")]
        public string Color_Id { get; set; }

        [JsonProperty("weight_id")]
        public string Weight_Id { get; set; }
    }

    public class ShoppingCartResponseModel 
    {
        [JsonProperty("cartCount")]
        public int? CartCount { get; set; }
    }

    public class ShoppingCartModel 
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("product_id")]
        public int? ProductId { get; set; }

        [JsonProperty("price")]
        public decimal? Price { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("quantity")]
        public int? Quantity { get; set; }

        [JsonProperty("available_product_quantity")]
        public int? AvailableQuantity { get; set; }

        [JsonProperty("primary_image")]
        public string PrimaryImage { get; set; }

        [JsonProperty("category_id")]
        public int? CategoryId { get; set; }

        [JsonProperty("category_title")]
        public string CategoryTitle { get; set; }

        [JsonProperty("sub_category_id")]
        public int? SubCategoryId { get; set; }
        
        [JsonProperty("subCategory_title")]
        public string SubCategoryTitle { get; set; }

        [JsonProperty("colors")]
        public Common Colors { get; set; }

        [JsonProperty("size")]
        public Common Size { get; set; }

        [JsonProperty("weight")]
        public Common Weight { get; set; }
    }

    public class ShoppingCartProductModel
    {
        [JsonProperty("product_id")]
        public int Product_Id { get; set; }
    }

    public class WishlistCartResponseModel
    {
        [JsonProperty("wishlist")]
        public WishlistProductModel Wishlist { get; set; }
    }

    public class WishlistProductModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("user_id")]
        public int CustomerId { get; set; }

        [JsonProperty("product_id")]
        public int ProductId { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }
        
        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        [JsonProperty("animalproduct")]
        public WishlistProductListingModel AnimalProduct { get; set; }

        [JsonProperty("avg_rating")]
        public List<WishlistProductRatingModel> AvgRating { get; set; }
    }

    public class WishlistListingModel
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("product_category_id")]
        public int? ProductCategoryId { get; set; }

        [JsonProperty("product_subcategory_id")]
        public int? ProductSubCategoryId { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("quantity")]
        public int? Quantity { get; set; }
        
        [JsonProperty("price")]
        public decimal? Price { get; set; }

        [JsonProperty("primary_image")]
        public string PrimaryImage { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("is_featured")]
        public int? IsFeatured { get; set; }

    }

    public class WishlistProductListingModel : WishlistListingModel
    {
        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        [JsonProperty("deleted_at")]
        public string DeletedAt { get; set; }

        [JsonProperty("colors")]
        public List<Common> Colors { get; set; }
        
        [JsonProperty("weight")]
        public List<Common> Weight { get; set; }
        
        [JsonProperty("size")]
        public List<Common> Size { get; set; }

        [JsonProperty("available_product_quantity")]
        public int? AvailableProductQuantity { get; set; }
    }

    public class WishlistProductResponseModel
    {
     [JsonProperty("allWishlists")]
     public List<WishlistProductModel> AllWishlists { get; set; }
     
     [JsonProperty("cartcount")]
     public int CartCount { get; set; }
    }

    public class WishlistProductRatingModel
    {
        [JsonProperty("aggregate")]
        public int? Aggregate { get; set; }

        [JsonProperty("product_id")]
        public int? ProductId { get; set; }
    }

    public class CheckoutResponseModel
    {   
        [JsonProperty("addresses")]
        public List<CheckoutAddressModel> CheckoutAddresses { get; set; }

        [JsonProperty("cartProducts")]
        public List<CheckoutProductModel> CartProducts { get; set; }

        [JsonProperty("proceedStatus")]
        public int ProceedStatus { get; set; }
    }

    public class CheckoutAddressModel
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("user_id")]
        public int? UserId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("contact_number")]
        public string ContactNumber { get; set; }

        [JsonProperty("address_1")]
        public string Address1 { get; set; }

        [JsonProperty("address_2")]
        public string Address2 { get; set; }

        [JsonProperty("flat_number")]
        public string FlatNumber { get; set; }
        
        [JsonProperty("suburb")]
        public string Suburb { get; set; }
        
        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }
        
        [JsonProperty("postcode")]
        public string PostCode { get; set; }

        [JsonProperty("is_default")]
        public int? IsDefault { get; set; }

        [JsonProperty("status")]
        public int? Status { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        [JsonProperty("deletedAt")]
        public string DeletedAt { get; set; }
    }

    public class CheckoutProductModel
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("product_id")]
        public int? ProductId { get; set; }

        [JsonProperty("price")]
        public decimal? Price { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("available_product_quantity")]
        public int StockQuantity { get; set; }

        [JsonProperty("primary_image")]
        public string PrimaryImage { get; set; }

        [JsonProperty("category_id")]
        public int? ProductCategoryId { get; set; }

        [JsonProperty("category_title")]
        public string ProductCategory { get; set; }

        [JsonProperty("sub_category_id")]
        public int? ProductSubCategoryId { get; set; }

        [JsonProperty("subCategory_title")]
        public string ProductSubCategory { get; set; }

        [JsonProperty("colors")]
        public Common Colors { get; set; }

        [JsonProperty("size")]
        public Common Size { get; set; }
        
        [JsonProperty("weight")]
        public Common Weight { get; set; }
    }
}