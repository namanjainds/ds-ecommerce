﻿using System;
using System.Collections.Generic;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Models.ShoppingCartsParameters;

namespace Nop.Plugin.Api.Services
{
    public interface IShoppingCartItemApiService
    {
        List<ShoppingCartItem> GetShoppingCartItems(int? customerId = null, DateTime? createdAtMin = null, DateTime? createdAtMax = null,
                                                    DateTime? updatedAtMin = null, DateTime? updatedAtMax = null, int limit = Configurations.DefaultLimit, 
                                                    int page = Configurations.DefaultPageValue);

        ShoppingCartItem GetShoppingCartItem(int id);

        List<ShoppingCartItem> GetShoppingCartItemsByCustomer(int? customerId);

        CheckoutResponseModel PrepareShoppingCartModel(CheckoutResponseModel model, IList<ShoppingCartItem> cart, Customer customer);


    }
}