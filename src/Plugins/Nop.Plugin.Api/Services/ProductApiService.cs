﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Vendors;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.DataStructures;
using Nop.Services.Stores;

namespace Nop.Plugin.Api.Services
{
    public class ProductApiService : IProductApiService
    {
        private readonly IStoreMappingService _storeMappingService;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<ProductCategory> _productCategoryMappingRepository;
        private readonly IRepository<Vendor> _vendorRepository;
        private readonly IRepository<OrderItem> _orderItemRepository;
        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<Category> _categoryRepository;

        public ProductApiService(IRepository<Product> productRepository,
            IRepository<ProductCategory> productCategoryMappingRepository,
            IRepository<Vendor> vendorRepository, IRepository<Category> categoryRepository,
            IStoreMappingService storeMappingService, IRepository<OrderItem> orderItemRepository, IRepository<Order> orderRepository)
        {
            _productRepository = productRepository;
            _productCategoryMappingRepository = productCategoryMappingRepository;
            _vendorRepository = vendorRepository;
            _storeMappingService = storeMappingService;
            _orderItemRepository = orderItemRepository;
            _orderRepository = orderRepository;
            _categoryRepository = categoryRepository;
        }

        public IList<Product> GetProducts(IList<int> ids = null,
            DateTime? createdAtMin = null, DateTime? createdAtMax = null, DateTime? updatedAtMin = null, DateTime? updatedAtMax = null,
           int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue, int sinceId = Configurations.DefaultSinceId,
           int? categoryId = null, string vendorName = null, bool? publishedStatus = null)
        {
            var query = GetProductsQuery(createdAtMin, createdAtMax, updatedAtMin, updatedAtMax, vendorName, publishedStatus, ids, categoryId);

            if (sinceId > 0)
            {
                query = query.Where(c => c.Id > sinceId);
            }

            return new ApiList<Product>(query, page - 1, limit);
        }

        public IQueryable<Product> GetProducts(string title = null, decimal? minPrice = null, decimal? maxPrice = null, 
           int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue,
           int? categoryId = null, int? subCategoryId = null, string sort = null, int? isFeatured = null, int? isBestSeller = null)
        {
            return GetProductsQueryByFilter(title, minPrice, maxPrice, categoryId, subCategoryId, sort, isFeatured, isBestSeller).OrderBy(x=>x.Id);            

            //return new ApiList<Product>(query, page - 1, limit);
        }

        public int GetProductsCount(DateTime? createdAtMin = null, DateTime? createdAtMax = null, 
            DateTime? updatedAtMin = null, DateTime? updatedAtMax = null, bool? publishedStatus = null, string vendorName = null, 
            int? categoryId = null)
        {
            var query = GetProductsQuery(createdAtMin, createdAtMax, updatedAtMin, updatedAtMax, vendorName,
                                         publishedStatus, categoryId: categoryId);

            return query.ToList().Count(p => _storeMappingService.Authorize(p));
        }

        public Product GetProductById(int productId)
        {
            if (productId == 0)
                return null;

            return _productRepository.Table.FirstOrDefault(product => product.Id == productId && !product.Deleted);
        }

        public Product GetProductByIdNoTracking(int productId)
        {
            if (productId == 0)
                return null;

            return _productRepository.Table.FirstOrDefault(product => product.Id == productId && !product.Deleted);
        }

        private IQueryable<Product> GetProductsQuery(DateTime? createdAtMin = null, DateTime? createdAtMax = null, 
            DateTime? updatedAtMin = null, DateTime? updatedAtMax = null, string vendorName = null, 
            bool? publishedStatus = null, IList<int> ids = null, int? categoryId = null)
            
        {
            var query = _productRepository.Table;

            if (ids != null && ids.Count > 0)
            {
                query = query.Where(c => ids.Contains(c.Id));
            }

            if (publishedStatus != null)
            {
                query = query.Where(c => c.Published == publishedStatus.Value);
            }

            // always return products that are not deleted!!!
            query = query.Where(c => !c.Deleted);

            if (createdAtMin != null)
            {
                query = query.Where(c => c.CreatedOnUtc > createdAtMin.Value);
            }

            if (createdAtMax != null)
            {
                query = query.Where(c => c.CreatedOnUtc < createdAtMax.Value);
            }

            if (updatedAtMin != null)
            {
               query = query.Where(c => c.UpdatedOnUtc > updatedAtMin.Value);
            }

            if (updatedAtMax != null)
            {
                query = query.Where(c => c.UpdatedOnUtc < updatedAtMax.Value);
            }

            if (!string.IsNullOrEmpty(vendorName))
            {
                query = from vendor in _vendorRepository.Table
                        join product in _productRepository.Table on vendor.Id equals product.VendorId
                        where vendor.Name == vendorName && !vendor.Deleted && vendor.Active
                        select product;
            }

            if (categoryId != null)
            {
                var categoryMappingsForProduct = from productCategoryMapping in _productCategoryMappingRepository.Table
                                                 where productCategoryMapping.CategoryId == categoryId
                                                 select productCategoryMapping;

                query = from product in query
                        join productCategoryMapping in categoryMappingsForProduct on product.Id equals productCategoryMapping.ProductId
                        select product;
            }

            query = query.OrderBy(product => product.Id);

            return query;
        }

        private IQueryable<Product> GetProductsQueryByFilter(string title = null, decimal? minPrice = null,
            decimal? maxPrice = null, int? categoryId = null, int? subCategoryId = null, string sort = null, int? isFeatured = null, int? isBestSeller = null)

        {
            var query = _productRepository.Table;            

            // always return products that are not deleted!!!
            query = query.Where(c => !c.Deleted);

            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(c => c.Name.ToLower().Contains(title.ToLower()));
            }

            if (minPrice != null)
            {
                query = query.Where(c => c.Price >= minPrice);
            }

            if (maxPrice != null)
            {
                query = query.Where(c => c.Price <= maxPrice);
            }

            if (categoryId != null)
            {
                var category = from categoryMapping in _categoryRepository.Table
                               where categoryMapping.ParentCategoryId == 0 && categoryMapping.Id == categoryId
                               select categoryMapping;
                if (!category.ToList().Any())
                {
                    var categoryMappingsForProduct = from productCategoryMapping in _productCategoryMappingRepository.Table
                                                     where productCategoryMapping.Category.ParentCategoryId == categoryId
                                                     select productCategoryMapping;

                    query = from product in query
                        join productCategoryMapping in categoryMappingsForProduct on product.Id equals productCategoryMapping.ProductId
                        select product; 
                }
                else
                {
                    var categoryMappingsForProduct = from productCategoryMapping in _productCategoryMappingRepository.Table
                                                     where productCategoryMapping.CategoryId == categoryId
                                                     select productCategoryMapping;

                    query = from product in query
                            join productCategoryMapping in categoryMappingsForProduct on product.Id equals productCategoryMapping.ProductId
                            select product;
                }
            }

            if (subCategoryId != null)
            {
                var categoryMappingsForProduct = from productCategoryMapping in _productCategoryMappingRepository.Table
                                                 where productCategoryMapping.CategoryId == subCategoryId
                                                 select productCategoryMapping;

                query = from product in query
                        join productCategoryMapping in categoryMappingsForProduct on product.Id equals productCategoryMapping.ProductId
                        select product;
            }

            if (isFeatured == 1)
            {
                var categoryMappingsForProduct = from productCategoryMapping in _productCategoryMappingRepository.Table
                                                 where productCategoryMapping.IsFeaturedProduct == true
                                                 select productCategoryMapping;
                query = from product in query
                        join productCategoryMapping in categoryMappingsForProduct on product.Id equals productCategoryMapping.ProductId
                        select product;
            }

            if(isBestSeller == 1)
            {
                var query1 = from orderItem in _orderItemRepository.Table
                             join o in _orderRepository.Table on orderItem.OrderId equals o.Id
                             join p in _productRepository.Table on orderItem.ProductId equals p.Id
                             where !o.Deleted && !p.Deleted                                   
                             select orderItem;

                var query2 =
                    //group by products
                    from orderItem in query1
                    group orderItem by orderItem.ProductId into g
                    select new
                    {
                        ProductId = g.Key,
                        TotalAmount = g.Sum(x => x.PriceExclTax),
                        TotalQuantity = g.Sum(x => x.Quantity),                        
                    };

                query = from product in query
                        join orderproduct in query2 on product.Id equals orderproduct.ProductId
                        select product;
            }

            if (!string.IsNullOrEmpty(sort))
                 query = sort == "max" ? query.OrderByDescending(product => product.Price) : query.OrderBy(product => product.Price);
            
            return query;
        }
    }
}