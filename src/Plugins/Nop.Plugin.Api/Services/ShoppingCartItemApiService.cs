﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Data;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.DataStructures;
using Nop.Core;
using Nop.Plugin.Api.Models.ShoppingCartsParameters;
using Nop.Services.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Services.Payments;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Vendors;
using Nop.Services.Common;
using Nop.Services.Orders;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Core.Domain.Common;
using Nop.Services.Discounts;
using Nop.Services.Vendors;
using Nop.Services.Security;
using System.Globalization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Shipping;
using Nop.Services.Media;
using Microsoft.AspNetCore.Http;
using Nop.Services.Tax;
using Nop.Services.Seo;
using Nop.Core.Domain.Media;
using Nop.Core.Http.Extensions;
using Nop.Core.Domain.Directory;
using Nop.Core.Infrastructure;
using Nop.Core.Caching;
using Nop.Plugin.Api.Models.CustomersParameters;

namespace Nop.Plugin.Api.Services
{
    public class ShoppingCartItemApiService : IShoppingCartItemApiService
    {
        private readonly IRepository<ShoppingCartItem> _shoppingCartItemsRepository;
        private readonly IStoreContext _storeContext;
        private readonly OrderSettings _orderSettings;
        private readonly IProductService _productService;
        private readonly ShoppingCartSettings _shoppingCartSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly VendorSettings _vendorSettings;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IWorkContext _workContext;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly ICurrencyService _currencyService;
        private readonly ILocalizationService _localizationService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly CommonSettings _commonSettings;
        private readonly ICustomerApiService _customerApiService;
        private readonly IDiscountService _discountService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IVendorService _vendorService;
        private readonly IPaymentPluginManager _paymentPluginManager;
        private readonly ICheckoutAttributeParser _checkoutAttributeParser;
        private readonly ICheckoutAttributeService _checkoutAttributeService;
        private readonly ICountryService _countryService;
        private readonly IDownloadService _downloadService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ITaxService _taxService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly MediaSettings _mediaSettings;
        private readonly IPermissionService _permissionService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IProductAttributeFormatter _productAttributeFormatter;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly AddressSettings _addressSettings;
        private readonly ShippingSettings _shippingSettings;
        private readonly IStaticCacheManager _cacheManager;
        private readonly IWebHelper _webHelper;
        private readonly IPictureService _pictureService;
        private readonly IAddressAttributeService _addressAttributeService;
        private readonly IAddressAttributeParser _addressAttributeParser;
        private readonly IAddressAttributeFormatter _addressAttributeFormatter;
        private readonly ICategoryService _categoryService;

        public ShoppingCartItemApiService(IRepository<ShoppingCartItem> shoppingCartItemsRepository, IStoreContext storeContext, OrderSettings orderSettings, IProductService productService, ShoppingCartSettings shoppingCartSettings, CatalogSettings catalogSettings, VendorSettings vendorSettings, IGenericAttributeService genericAttributeService, IWorkContext workContext, IOrderProcessingService orderProcessingService, ICurrencyService currencyService,
            ILocalizationService localizationService, IPriceFormatter priceFormatter, CommonSettings commonSettings, ICustomerApiService customerApiService,
            IDiscountService discountService, IShoppingCartService shoppingCartService, IVendorService vendorService, IPaymentPluginManager paymentPluginManager,
            ICheckoutAttributeService checkoutAttributeService, ICheckoutAttributeParser checkoutAttributeParser, ICountryService countryService,
            IDownloadService downloadService, IHttpContextAccessor httpContextAccessor, ITaxService taxService, IUrlRecordService urlRecordService,
            MediaSettings mediaSettings, IPermissionService permissionService, IStateProvinceService stateProvinceService, IProductAttributeFormatter productAttributeFormatter, IPriceCalculationService priceCalculationService, AddressSettings addressSettings, ShippingSettings shippingSettings,
            IStaticCacheManager cacheManager, IWebHelper webHelper, IPictureService pictureService, IAddressAttributeService addressAttributeService,
            IAddressAttributeParser addressAttributeParser, IAddressAttributeFormatter addressAttributeFormatter, ICategoryService categoryService)
        {
            _shoppingCartItemsRepository = shoppingCartItemsRepository;
            _storeContext = storeContext;
            _orderSettings = orderSettings;
            _productService = productService;
            _shoppingCartSettings = shoppingCartSettings;
            _catalogSettings = catalogSettings;
            _vendorSettings = vendorSettings;
            _genericAttributeService = genericAttributeService;
            _workContext = workContext;
            _orderProcessingService = orderProcessingService;
            _currencyService = currencyService;
            _localizationService = localizationService;
            _priceFormatter = priceFormatter;
            _commonSettings = commonSettings;
            _customerApiService = customerApiService;
            _discountService = discountService;
            _shoppingCartService = shoppingCartService;
            _vendorService = vendorService;
            _paymentPluginManager = paymentPluginManager;
            _checkoutAttributeService = checkoutAttributeService;
            _checkoutAttributeParser = checkoutAttributeParser;
            _countryService = countryService;
            _downloadService = downloadService;
            _httpContextAccessor = httpContextAccessor;
            _taxService = taxService;
            _urlRecordService = urlRecordService;
            _mediaSettings = mediaSettings;
            _permissionService = permissionService;
            _stateProvinceService = stateProvinceService;
            _productAttributeFormatter = productAttributeFormatter;
            _priceCalculationService = priceCalculationService;
            _addressSettings = addressSettings;
            _shippingSettings = shippingSettings;
            _cacheManager = cacheManager;
            _webHelper = webHelper;
            _pictureService = pictureService;
            _addressAttributeService = addressAttributeService;
            _addressAttributeParser = addressAttributeParser;
            _addressAttributeFormatter = addressAttributeFormatter;
            _categoryService = categoryService;
    }

        public List<ShoppingCartItem> GetShoppingCartItems(int? customerId = null, DateTime? createdAtMin = null, DateTime? createdAtMax = null,
                                                           DateTime? updatedAtMin = null, DateTime? updatedAtMax = null, int limit = Configurations.DefaultLimit,
                                                           int page = Configurations.DefaultPageValue)
        {
            var query = GetShoppingCartItemsQuery(customerId, createdAtMin, createdAtMax,
                                                                           updatedAtMin, updatedAtMax);

            return new ApiList<ShoppingCartItem>(query, page - 1, limit);
        }

        public ShoppingCartItem GetShoppingCartItem(int id)
        {
            return _shoppingCartItemsRepository.GetById(id);
        }

        private IQueryable<ShoppingCartItem> GetShoppingCartItemsQuery(int? customerId = null, DateTime? createdAtMin = null, DateTime? createdAtMax = null,
                                                                       DateTime? updatedAtMin = null, DateTime? updatedAtMax = null)
        {
            var query = _shoppingCartItemsRepository.Table;

            if (customerId != null)
            {
                query = query.Where(shoppingCartItem => shoppingCartItem.CustomerId == customerId);
            }

            if (createdAtMin != null)
            {
                query = query.Where(c => c.CreatedOnUtc > createdAtMin.Value);
            }

            if (createdAtMax != null)
            {
                query = query.Where(c => c.CreatedOnUtc < createdAtMax.Value);
            }

            if (updatedAtMin != null)
            {
                query = query.Where(c => c.UpdatedOnUtc > updatedAtMin.Value);
            }

            if (updatedAtMax != null)
            {
                query = query.Where(c => c.UpdatedOnUtc < updatedAtMax.Value);
            }

            // items for the current store only
            var currentStoreId = _storeContext.CurrentStore.Id;
            query = query.Where(c => c.StoreId == currentStoreId);

            query = query.OrderBy(shoppingCartItem => shoppingCartItem.Id);

            return query;
        }

        public List<ShoppingCartItem> GetShoppingCartItemsByCustomer(int? customerId)
        {
            var query = _shoppingCartItemsRepository.Table;

            query = query.Where(shoppingCartItem => shoppingCartItem.CustomerId == customerId);
            
            query = query.OrderBy(shoppingCartItem => shoppingCartItem.Id);

            return query.ToList();
        }

        public virtual CheckoutResponseModel PrepareShoppingCartModel(CheckoutResponseModel model, IList<ShoppingCartItem> cart, Customer customer)
        {
            if (cart == null)
                throw new ArgumentNullException(nameof(cart));

            if (model == null)
                throw new ArgumentNullException(nameof(model));
            model.CartProducts = new List<CheckoutProductModel>();
            try
            {
                //cart items
                foreach (var sci in cart)
                {
                    var cartItemModel = PrepareShoppingCartItemModel(cart, sci);
                    model.CartProducts.Add(cartItemModel);
                }
                model.CheckoutAddresses = PrepareAddressModel(customer);
                model.ProceedStatus = 1;
            }
            catch
            {
                model.ProceedStatus = 0;
            }
            return model;
        }

        /// <summary>
        /// Prepare the shopping cart item model
        /// </summary>
        /// <param name="cart">List of the shopping cart item</param>
        /// <param name="sci">Shopping cart item</param>
        /// <returns>Shopping cart item model</returns>
        protected virtual CheckoutProductModel PrepareShoppingCartItemModel(IList<ShoppingCartItem> cart, ShoppingCartItem sci)
        {
            if (cart == null)
                throw new ArgumentNullException(nameof(cart));

            if (sci == null)
                throw new ArgumentNullException(nameof(sci));

            var cartItemModel = new CheckoutProductModel
            {
                Id = sci.Id,
                ProductId = sci.Product.Id,
                Price = sci.Product.Price,
                Title = sci.Product.Name,
                Quantity = sci.Quantity,
                StockQuantity = sci.Product.StockQuantity,
                PrimaryImage = _pictureService.GetPicturesByProductId(sci.Product.Id).Any() ? _pictureService.GetPictureUrl(_pictureService.GetPicturesByProductId(sci.Product.Id).FirstOrDefault(), 0, true) : null,
                ProductCategoryId = sci.Product.ProductCategories.Any() ? sci.Product.ProductCategories.FirstOrDefault()?.Category?.ParentCategoryId == 0 ? sci.Product.ProductCategories.FirstOrDefault()?.CategoryId : sci.Product.ProductCategories.FirstOrDefault()?.Category?.ParentCategoryId : null,
                ProductCategory = sci.Product.ProductCategories.Any() ? sci.Product.ProductCategories.FirstOrDefault()?.Category?.ParentCategoryId == 0 ? sci.Product.ProductCategories.FirstOrDefault()?.Category.Name : _categoryService.GetCategoryById(sci.Product.ProductCategories.FirstOrDefault().Category?.ParentCategoryId ?? 0)?.Name : null,
                ProductSubCategoryId = sci.Product.ProductCategories.Any() ? sci.Product.ProductCategories.FirstOrDefault()?.CategoryId : null,
                ProductSubCategory = sci.Product.ProductCategories.Any() ? sci.Product.ProductCategories.FirstOrDefault()?.Category.Name : null,
                Colors = new Common(),
                Size = new Common(),
                Weight = new Common()
            };                       

            return cartItemModel;
        }

        /// <summary>
        /// Prepare address model
        /// </summary>
        /// <param name="model">Address model</param>
        /// <param name="address">Address entity</param>
        /// <param name="excludeProperties">Whether to exclude populating of model properties from the entity</param>
        /// <param name="addressSettings">Address settings</param>
        /// <param name="loadCountries">Countries loading function; pass null if countries do not need to load</param>
        /// <param name="prePopulateWithCustomerFields">Whether to populate model properties with the customer fields (used with the customer entity)</param>
        /// <param name="customer">Customer entity; required if prePopulateWithCustomerFields is true</param>
        /// <param name="overrideAttributesXml">Overridden address attributes in XML format; pass null to use CustomAttributes of the address entity</param>
        public virtual List<CheckoutAddressModel> PrepareAddressModel(Customer customer)
        {
           return customer.Addresses.Select(add => new CheckoutAddressModel()
            {
                Id = add.Id,
                UserId = customer.Id,
                Name = add.FirstName,
                Email = add.Email,
                ContactNumber = add.PhoneNumber,
                FlatNumber = _genericAttributeService.GetAttribute<string>(add, NopCustomerDefaults.FlatNumberAttribute),
                Address1 = add.Address1,
                Address2 = add.Address2,
                Country = _genericAttributeService.GetAttribute<string>(add, NopCustomerDefaults.CountryNameAttribute),
                State = add.StateProvince != null ? add.StateProvince.Name : string.IsNullOrEmpty(add.County) ? null : add.County,
                Suburb = add.City,
                PostCode = add.ZipPostalCode,
                IsDefault = customer.BillingAddressId == add.Id ? 1 : 0,
                Status = 1,
                CreatedAt = add.CreatedOnUtc.ToString("yyyy/MM/dd HH:mm:ss"),
           }).ToList();
                
        }
    }
}