﻿using System;
using System.Collections.Generic;
using Nop.Core.Domain.Customers;

namespace Nop.Plugin.Api.Services
{
    public interface IWorkflowMessageApiService
    {
        IList<int> SendCustomerPasswordRecoveryMessage(Customer customer, int languageId);
        IList<int> SendCustomerEmailValidationMessage(Customer customer, int languageId);
    }
}
