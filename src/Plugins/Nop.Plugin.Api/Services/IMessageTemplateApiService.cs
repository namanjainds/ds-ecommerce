﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Core.Domain.Messages;

namespace Nop.Plugin.Api.Services
{
    public interface IMessageTemplateApiService
    {
        IList<MessageTemplate> GetMessageTemplatesByName(string messageTemplateName, int? storeId = null);
    }
}
