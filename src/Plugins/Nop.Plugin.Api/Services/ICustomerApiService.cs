﻿using System;
using System.Collections.Generic;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.DTOs.Customers;
using Nop.Plugin.Api.Models.CustomersParameters;
using Nop.Services.Customers;

namespace Nop.Plugin.Api.Services
{
    public interface ICustomerApiService
    {
        int GetCustomersCount();

        CustomerDto GetCustomerById(int id, bool showDeleted = false);

        Customer GetCustomerEntityById(int id);

        IList<CustomerDto> GetCustomersDtos(DateTime? createdAtMin = null, DateTime? createdAtMax = null,
            int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue, int sinceId = Configurations.DefaultSinceId);
        
        IList<CustomerDto> Search(string query = "", string order = Configurations.DefaultOrder, 
            int page = Configurations.DefaultPageValue, int limit = Configurations.DefaultLimit);

        Dictionary<string, string> GetFirstAndLastNameByCustomerId(int customerId);
        CustomerLoginResults ValidateCustomer(string usernameOrEmail, string password);
        Customer GetCustomerByEmail(string email);
        string GetCustomerFullName(Customer customer);
        LoginModel PrepareLoginUserModel(LoginModel model, Customer customer);
        bool IsPasswordRecoveryTokenValid(Customer customer, string token);
        Customer InsertGuestCustomer();
        CustomerRegistrationResult RegisterCustomer(CustomerRegistrationRequest request);
        Customer GetCustomerByUsername(string username);
        void InsertCustomerPassword(CustomerPassword customerPassword);
        bool IsPasswordRecoveryLinkExpired(Customer customer);
		void UpdateCustomer(Customer customer);
        LoginModel UpdateLoginCustomer(LoginModel model, Customer customer);
        void RemoveCustomerAddress(Customer customer, Address address);
        void InsertCustomer(Customer customer);
        ChangePasswordResult ChangePassword(ChangePasswordRequest request);

        void DeleteCustomer(Customer customer);
        IList<CustomerRole> GetAllCustomerRoles(bool showHidden = false);

        string[] ParseAppliedDiscountCouponCodes(Customer customer);
        void ResetCheckoutData(Customer customer, int storeId,
            bool clearCouponCodes = false, bool clearCheckoutAttributes = false,
            bool clearRewardPoints = true, bool clearShippingMethod = true,
            bool clearPaymentMethod = true);

        void ApplyDiscountCouponCode(Customer customer, string couponCode);
    }
}