﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Messages;
using Nop.Services.Messages;
using Nop.Services.Stores;

namespace Nop.Plugin.Api.Services
{
    public class MessageTemplateApiService : IMessageTemplateApiService
    {
        private readonly ICacheManager _cacheManager;
        private readonly IRepository<MessageTemplate> _messageTemplateRepository;
        private readonly IStoreMappingService _storeMappingService;
        public MessageTemplateApiService(ICacheManager cacheManager, IRepository<MessageTemplate> messageTemplateRepository,
            IStoreMappingService storeMappingService)
        {
            _cacheManager = cacheManager;
            _messageTemplateRepository = messageTemplateRepository;
            _storeMappingService = storeMappingService;
        }
        public virtual IList<MessageTemplate> GetMessageTemplatesByName(string messageTemplateName, int? storeId = null)
        {
            if (string.IsNullOrWhiteSpace(messageTemplateName))
                throw new ArgumentException(nameof(messageTemplateName));

            var key = string.Format(NopMessageDefaults.MessageTemplatesByNameCacheKey, messageTemplateName, storeId ?? 0);
            return _cacheManager.Get(key, () =>
            {
                //get message templates with the passed name
                var templates = _messageTemplateRepository.Table
                    .Where(messageTemplate => messageTemplate.Name.Equals(messageTemplateName))
                    .OrderBy(messageTemplate => messageTemplate.Id).ToList();

                //filter by the store
                if (storeId.HasValue && storeId.Value > 0)
                    templates = templates.Where(messageTemplate => _storeMappingService.Authorize(messageTemplate, storeId.Value)).ToList();

                return templates;
            });
        }
    }
}
