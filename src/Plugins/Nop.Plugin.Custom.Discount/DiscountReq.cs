﻿namespace Nop.Plugin.Custom.Discount
{
    public static class DiscountReq
    {
        public const string SystemName = "Custom.Discount";
        public const string SettingsKey = "Custom.Discount-{0}";
        public const string HtmlFieldPrefix = "DiscountRulesIsFirstOrder{0}";
    }
}